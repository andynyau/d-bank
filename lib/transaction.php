<?php
//to check whether the balance in the account is sufficient
function check_balance_enough($aid,$amount)
{
	global $s,$db,$tb;

	$q=new sql($db);
	$sql = "SELECT * FROM {$tb['balance']} WHERE aid = '$aid' AND balance >= '$amount'";
	$q->query($sql);
	if ($q->numrows() == 0)
	{
		errormsg("Amount exceed the balance in Account.");
	}
}

//to check the amount is exceed daily limit
function check_daily_limit($aid,$amount)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT SUM(amount) as total FROM {$tb['transaction_out']} WHERE aid = '$aid' AND LEFT(datetime, 10) = CURDATE()";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$total = $rows['total'];
		}
	}
	$amount = $amount + $total;
	if ($amount > $s['limit_daily_transaction'])
	{
		errormsg("Amount has exceed daily limit.");
	}
}

//to check the amount is exceed per transaction limit
function check_per_limit($amount)
{
	global $s,$db,$tb;
	if ($amount > $s['limit_per_transaction'])
	{
		return false;
	}
	else
	{
		return true;
	}
}

//insert record into transaction in and out
function do_transaction($from,$to,$amount,$remark,$date='')
{
	global $s,$db,$tb;

	if ($date == '')
	{
		$date = "NOW()";
	}
	else
	{
		$date = "'".$date."'";
	}

	$to_id = get_account_id($to);
	$from_account_no = get_account_no($from);
	$q=new sql($db);

	//begin transaction
	$sql = "BEGIN;";
	$q->query($sql);

	//do transaction out
	$sql = "INSERT INTO {$tb['transaction_out']} VALUES ('',$from,$date,'$to','$amount','$remark');";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	//do transaction in
	$sql = "INSERT INTO {$tb['transaction_in']} VALUES ('',$to_id,$date,'$from_account_no','$amount','$remark');";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	//update from balance amount
	$sql = "UPDATE {$tb['balance']} SET balance = balance - '$amount' WHERE aid = '$from';";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	//update to balance amount
	$sql = "UPDATE {$tb['balance']} SET balance = balance + '$amount' WHERE aid = '$to_id';";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	if ($error)
	{
		//rollback transacion
		$sql = "ROLLBACK;";
		$q->query($sql);
	}
	else
	{
		//commit transaction
		$sql = "COMMIT;";
		$q->query($sql);
	}
}

//insert record into transaction in and out
function pending_transaction($from,$to,$amount,$remark)
{
	global $s,$db,$tb;

	$from_account_no = get_account_no($from);
	$q=new sql($db);

	//insert transaction pending
	$sql = "BEGIN;";
	$q->query($sql);

	//insert pending transaction record
	$sql = "INSERT INTO {$tb['transaction_pending']} VALUES ('',$from_account_no,'$to','$amount',NOW(),'$remark');";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	if ($error)
	{
		//rollback transacion
		$sql = "ROLLBACK;";
		$q->query($sql);
	}
	else
	{
		//commit transaction
		$sql = "COMMIT;";
		$q->query($sql);
	}
}

//get the transaction pending info
function get_transaction_pending_info($id)
{
	global $s,$db,$tb;

	$q=new sql($db);
	$sql = "SELECT * FROM {$tb['transaction_pending']} WHERE id = '$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$pending_info = $rows;
		}
	}
	return $pending_info;
}
?>