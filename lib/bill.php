<?php
//place payment for bill
function do_bill_payment($id,$pid,$aid,$amount,$remark)
{
	global $s,$db,$tb;

	$q = new sql($db);

	$date = date("Y-m-d H:i:s");

	//begin transaction
	$sql = "BEGIN;";
	$q->query($sql);

	//insert bill payment record
	$sql = "INSERT INTO {$tb['bill']} VALUES ('','$id','$pid','$amount','$amount','$date','$remark');";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	$to = get_participant_account_no($pid);
	$to_id = get_account_id($to);
	$from_account_no = get_account_no($aid);
	$remark = "Bill Payment ($remark : $from_account_no : $date)";	

	//do transaction out
	$sql = "INSERT INTO {$tb['transaction_out']} VALUES ('','$aid','$date','$to','$amount','$remark');";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	//do transaction in
	$sql = "INSERT INTO {$tb['transaction_in']} VALUES ('','$to_id','$date','$from_account_no','$amount','$remark');";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	//update from balance amount
	$sql = "UPDATE {$tb['balance']} SET balance = balance - '$amount' WHERE aid = '$aid';";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	//update to balance amount
	$sql = "UPDATE {$tb['balance']} SET balance = balance + '$amount' WHERE aid = '$to_id';";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	if ($error)
	{
		//rollback transacion
		$sql = "ROLLBACK;";
		$q->query($sql);
	}
	else
	{
		//commit transaction
		$sql = "COMMIT;";
		$q->query($sql);
	}
}
?>