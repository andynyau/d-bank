<?php

//this is a class to establish mysql connection
class sql
{
	var $sql_query;
	var $link;
	var $result;
	var $mysql_host;
	var $mysql_user;
	var $mysql_pass;
	var $mysql_db;
	
	//the object that established the mysql connection
	function sql($db)
	{
		$this->mysql_host = $db['mysql_host'];
		$this->mysql_user = $db['mysql_user'];
		$this->mysql_pass = $db['mysql_pass'];
		$this->mysql_db = $db['mysql_db'];

		$this->link = mysql_connect($this->mysql_host, $this->mysql_user, $this->mysql_pass)
			or die("Could not connect : " .mysql_error());
		mysql_select_db($this->mysql_db,$this->link) or die("Could not select database");
	}
	
	//this function is to execute the sql query
	function query($sql)
	{
		$this->sql_query = $sql;
		$this->result = mysql_query($this->sql_query, $this->link);
	}

	//this function is to return the number of rows that get from the sql query
	function numrows()
	{
		return mysql_num_rows($this->result);
	}

	//this function is to return the data that get from the sql query
	function getrows()
	{
		return mysql_fetch_array($this->result);
	}
}

//this function is to generate a pagination link to divide large numbers of records into pages, it will return the links of the pages
function pagination($page_count,$cut_off,$start,$PHP_SELF,$sql,$opt,$act,$extra)
{
	global $db,$tb;

	$q= new sql($db);
	$q->query($sql);
	$num = $q->numrows();

	$newnum = $num / $page_count;
	$newnum = ceil($newnum);
	if(!isset($page))$page = 1;

	if($newnum >= 2)
	{
		if(isset($start) && $start != 0)
		{
			$pglink = "<a href=\"$PHP_SELF?opt=".$opt."&act=".$act."&start=0".$extra."\"><<</a>&nbsp;";
			$pglink .= "<a href=\"$PHP_SELF?opt=".$opt."&act=".$act."&start=".($start - $page_count).$extra."\"><</a>&nbsp;";
		}
		else
		{
			$pglink = "<< ";
			$pglink .= "< ";
		}

		$total_pages = $newnum;
		if($newnum > $cut_off)$newnum = $cut_off;

		$cur_page = ($start + $page_count) / $page_count;

		if($cur_page > $cut_off)$page = $cur_page - $cut_off + 1;

		if($cur_page > $cut_off)
		{
			$start_page = $page * $page_count - $page_count;
		}
		else
		{
			$start_page = 0;
		}

		for($i=0; $i<$newnum;$i++)
		{
			if($start == ($page * $page_count) - $page_count)
			{
				$pglink .= "<font color=\"#ff0000\">".$page."</font>&nbsp;";
			}
			else
			{
				$pglink .= "<a href=\"$PHP_SELF?opt=".$opt."&act=".$act."&start=$start_page".$extra."\">$page</a>&nbsp;";
			}
			$page++;
			$start_page = $start_page + $page_count;
		}

		if($newnum >= 2 && $cur_page < $newnum && $cur_page <= $total_pages)
		{
			$pglink .= "<a href=\"$PHP_SELF?opt=".$opt."&act=".$act."&start=".($start + $page_count).$extra."\">></a>&nbsp;";
		}
		elseif($cur_page >= $total_pages)
		{
			$pglink .= "> ";
		}
		else
		{
			$pglink .= "<a href=\"$PHP_SELF?opt=".$opt."&act=".$act."&start=".($start + $page_count).$extra."\">></a>&nbsp;";
		}
		
		$final_page = ($total_pages - 1) * $page_count;

		if($start != $final_page)
		{
			$pglink .= "<a href=\"$PHP_SELF?opt=".$opt."&act=".$act."&start=".(($total_pages - 1) * $page_count).$extra."\">>></a>&nbsp;";
		}
		else
		{
			$pglink .= ">>&nbsp;";
		}
		$pglink .= "Page $cur_page of $newnum";
	}
	return $pglink;
}

?>