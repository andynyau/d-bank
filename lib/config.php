<?php
require_once('../lib/mysql.php');

//configuration setting of Database
$db['mysql_host'] = "localhost";
$db['mysql_user'] = "root";
$db['mysql_pass'] = "iseedeadpeople";
$db['mysql_db'] = "dbank";

//configuration setting of Table names
$tb['account'] = "account";
$tb['account_reserve'] = "account_reserve";
$tb['account_type'] = "account_type";
$tb['address'] = "address";
$tb['admin_login'] = "admin_login";
$tb['balance'] = "balance";
$tb['bill'] = "bill";
$tb['countries'] = "countries";
$tb['credit_card'] = "credit_card";
$tb['credit_card_type'] = "credit_card_type";
$tb['credit_card_payment'] = "credit_card_payment";
$tb['customer'] = "customer";
$tb['customer_image'] = "customer_image";
$tb['email'] = "email";
$tb['login'] = "login";
$tb['participant'] = "participant";
$tb['phone'] = "phone";
$tb['phone_type'] = "phone_type";
$tb['setting'] = "setting";
$tb['transaction_in'] = "transaction_in";
$tb['transaction_out'] = "transaction_out";
$tb['transaction_pending'] = "transaction_pending";
$tb['web_log'] = "web_log";

//establish database connection to get system setting from database
$q = new sql($db);
$sql = "SELECT * FROM setting ORDER BY id";
$q -> query($sql);
if ($q -> numrows())
{
		while ($rows=$q->getrows())
		{
			if ($rows['key'] == 'site_name') {$s['site_name']=$rows['value'];}
			if ($rows['key'] == 'url') {$s['url']=$rows['value'];}
			if ($rows['key'] == 'customer_url') {$s['customer_url']=$rows['value'];}
			if ($rows['key'] == 'admin_url') {$s['admin_url']=$rows['value'];}
			if ($rows['key'] == 'lib_path') {$s['lib_path']=$rows['value'];}
			if ($rows['key'] == 'img_path') {$s['img_path']=$rows['value'];}
			if ($rows['key'] == 'inc_path') {$s['inc_path']=$rows['value'];}
			if ($rows['key'] == 'meta_desc') {$s['meta_desc']=$rows['value'];}
			if ($rows['key'] == 'meta_keywds') {$s['meta_keywds']=$rows['value'];}
			if ($rows['key'] == 'meta_copy') {$s['meta_copy']=$rows['value'];}
			if ($rows['key'] == 'cookies_name') {$s['cookies_name']=$rows['value'];}
			if ($rows['key'] == 'cookies_name_admin') {$s['cookies_name_admin']=$rows['value'];}
			if ($rows['key'] == 'cookies_timeout') {$s['cookies_timeout']=$rows['value'];}
			if ($rows['key'] == 'site_email') {$s['site_email']=$rows['value'];}
			if ($rows['key'] == 'limit_per_transaction') {$s['limit_per_transaction']=$rows['value'];}
			if ($rows['key'] == 'limit_daily_transaction') {$s['limit_daily_transaction']=$rows['value'];}
			if ($rows['key'] == 'company_account') {$s['company_account']=$rows['value'];}
		}
}

//setting path for library to include
require_once($s['lib_path']."smarty/Smarty.class.php");
require_once($s['lib_path']."smarty_conf.php");
require_once($s['lib_path']."account.php");
require_once($s['lib_path']."bill.php");
require_once($s['lib_path']."cookies.php");
require_once($s['lib_path']."creditcard.php");
require_once($s['lib_path']."countries.php");
require_once($s['lib_path']."date.php");
require_once($s['lib_path']."email.php");
require_once($s['lib_path']."function.php");
require_once($s['lib_path']."participant.php");
require_once($s['lib_path']."phone_type.php");
require_once($s['lib_path']."setting.php");
require_once($s['lib_path']."transaction.php");
require_once($s['lib_path']."validate.php");
require_once($s['lib_path']."weblog.php");

//benchmark
require_once($s['lib_path']."Benchmark/PHP_Benchmark/Iterate.php");
?>