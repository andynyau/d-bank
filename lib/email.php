<?php
// this function will send a verify link to the new added email address
function sendmail_new($email,$verify_str)
{
	global $s;
	$subject = "D-Bank Email Verification";
	$message = "Thank You for using D-Bank online banking system. This is the link to verify your new added email address.\n\n";
	$message .= "{$s['customer_url']}?opt=verify_email&verify_str=$verify_str";
	mail($email, $subject, $message, "From: {$s['site_email']}\r\n"."X-Mailer: PHP/".phpversion());
}

//return the string that contains all customers' primary email
function get_email_list()
{
	global $s,$db,$tb;
	$q = new sql($db);
	$sql = "SELECT email FROM {$tb['email']} WHERE `primary` = 'YES'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			if (empty($email))
			{
				$email = $rows['email'];
			}
			else
			{
				$email .= ",".$rows['email'];
			}
		}
	}
	return $email;
}

//send the mail
function send_mail($to,$subject,$message)
{
	global $s;
	mail($to, $subject, $message, "From: {$s['site_email']}\r\n"."X-Mailer: PHP/".phpversion());
}
?>