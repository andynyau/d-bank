<?php
//this is to let user to logout from the system and reset the cookies on client's machine
function admin_logout()
{
	global $s,$db,$tb;

	$session_fr_cookies = $_COOKIE[$s['cookies_name_admin']];
	$session_decode = base64_decode($_COOKIE[$s['cookies_name_admin']]);
	$cookies_var = explode(":",$session_decode);
	$username = $cookies_var[0];
	$q=new sql($db);
	$sql="SELECT id FROM {$tb['admin_login']} WHERE username='$username'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$id = $rows['id'];
		}
		// now reset cookie
		setcookie($s['cookies_name_admin'],'',time()-86400*365,"/");

		//remove session id
		$sql="UPDATE {$tb['admin_login']} SET session='' WHERE id='$id'";
		$q->query($sql);
	}
}

//this is to validate the cookies that set on the client's machine, to check if it is valid
function validate_admin_login()
{
	global $s, $db, $tb;
	if (($_POST['admin_login_user']) && ($_POST['admin_login_password']))
	{
		$username = $_POST['admin_login_user'];
		$password = md5($_POST['admin_login_password']);
		$q=new sql($db);
    	$sql="SELECT {$tb['admin_login']}.id, {$tb['admin_login']}.level FROM {$tb['admin_login']} WHERE {$tb['admin_login']}.username='$username' AND {$tb['admin_login']}.password='$password'";
    	$q->query($sql);
		if ($q->numrows())
		{
			while ($rows=$q->getrows())
			{
				docookie($username,$rows['id'],'admin');
				return $rows['level'];
			}
		}
		else
		{
//			admin_logout();
			return false;
		}
	}
	elseif (isset($_COOKIE[$s['cookies_name_admin']]))
	{
		$session_fr_cookies = $_COOKIE[$s['cookies_name_admin']];
		$session_decode = base64_decode($_COOKIE[$s['cookies_name_admin']]);
		$cookies_var = explode(":",$session_decode);
		$username_fr_cookies = $cookies_var[0];
		$sql="SELECT {$tb['admin_login']}.level, {$tb['admin_login']}.session FROM {$tb['admin_login']} WHERE {$tb['admin_login']}.username='$username_fr_cookies'";
		$q=new sql($db);
		$q->query($sql);
		if ($q->numrows())
		{
			while ($rows=$q->getrows())
			{
				$level = $rows['level'];
				$session_fr_db = $rows['session'];
			}
			if ($session_fr_db == $session_fr_cookies)
			{
				return $level;
			}
			else
			{
//				admin_logout();
				return false;
			}
		}
		else
		{
//			admin_logout();
			return false;
		}
	}
	else
	{
//		admin_logout();
		return false;
	}
}

//this is to let user to logout from the system and reset the cookies on client's machine
function logout()
{
	global $s,$db,$tb;

	$session_fr_cookies = $_COOKIE[$s['cookies_name']];
	$session_decode = base64_decode($_COOKIE[$s['cookies_name']]);
	$cookies_var = explode(":",$session_decode);
	$username = $cookies_var[0];
	$q=new sql($db);
	$sql="SELECT {$tb['login']}.id FROM {$tb['customer']} INNER JOIN {$tb['login']} ON {$tb['customer']}.id = {$tb['login']}.cid WHERE {$tb['customer']}.username='$username'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$lid = $rows['id'];
		}
		// now reset cookie
		setcookie($s['cookies_name'],'',time()-86400*365,"/");

		//remove session id
		$sql="UPDATE {$tb['login']} SET session='' WHERE id='$lid'";
		$q->query($sql);
	}
}

//this is to validate the cookies that set on the client's machine, to check if it is valid
function validate_login()
{
	global $s, $db, $tb;
	if (($_POST['login_user']) && ($_POST['login_password']))
	{
		$username = $_POST['login_user'];
		$password = md5($_POST['login_password']);
		$q=new sql($db);
    	$sql="SELECT {$tb['login']}.id as lid FROM {$tb['customer']} INNER JOIN {$tb['login']} ON {$tb['customer']}.id = {$tb['login']}.cid WHERE {$tb['customer']}.username='$username' AND {$tb['login']}.password='$password'";
    	$q->query($sql);
		if ($q->numrows())
		{
			while ($rows=$q->getrows())
			{
				docookie($username,$rows['lid'],'customer');
				return true;
			}
		}
		else
		{
//			logout();
			return false;
		}
	}
	elseif (isset($_COOKIE[$s['cookies_name']]))
	{
		$session_fr_cookies = $_COOKIE[$s['cookies_name']];
		$session_decode = base64_decode($_COOKIE[$s['cookies_name']]);
		$cookies_var = 	$cvar=explode(":",$session_decode);
		$username_fr_cookies = $cookies_var[0];
		$sql="SELECT {$tb['login']}.session FROM {$tb['customer']} INNER JOIN {$tb['login']} ON {$tb['customer']}.id = {$tb['login']}.cid WHERE {$tb['customer']}.username='$username_fr_cookies'";
		$q=new sql($db);
		$q->query($sql);
		if ($q->numrows())
		{
			while ($rows=$q->getrows())
			{
				$session_fr_db = $rows['session'];
			}
			if ($session_fr_db == $session_fr_cookies)
			{
				return true;
			}
			else
			{
//				logout();
				return false;
			}
		}
		else
		{
//			logout();
			return false;
		}
	}
	else
	{
//		logout();
		return false;
	}
}

//this is the function to set cookies on client machine
function docookie($username,$id,$type)
{
	global $s,$db,$tb;

	$sessionid=base64_encode("$username:".time());
	$timeout=time()+(3600*$s['cookies_timeout']);
	if ($type == 'customer')
	{
	    setcookie($s['cookies_name'],$sessionid,$timeout,"/");
	}
	elseif ($type == 'admin')
	{
	    setcookie($s['cookies_name_admin'],$sessionid,$timeout,"/");
	}

	$q=new sql($db);
	if ($type == 'customer')
	{
		$sql="UPDATE {$tb['login']} SET session='$sessionid' WHERE id='$id'";
	}
	elseif ($type == 'admin')
	{
		$sql="UPDATE {$tb['admin_login']} SET session='$sessionid' WHERE id='$id'";
	}
	$q->query($sql);
}
?>