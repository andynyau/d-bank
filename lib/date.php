<?php
//return list for year
function getyearlist($start,$end)
{
	$total = $end - $start;
	for ($x=0;$x<=$total;$x++)
	{
		$year[$x]=$x + $start;
	}
	return $year;
}

//return list for month
function getmonthlist()
{
	$month = array(
		'01'=>"January",
		'02'=>"February",
		'03'=>"March",
		'04'=>'April',
		'05'=>'May',
		'06'=>'June',
		'07'=>'July',
		'08'=>'August',
		'09'=>'September',
		'10'=>'October',
		'11'=>'November',
		'12'=>'December');
	return $month;
}

//return list of day
function getdaylist()
{
	for ($x=0;$x<=30;$x++)
	{
		$day[$x]=$x + 1;
	}
	return $day;
}
?>