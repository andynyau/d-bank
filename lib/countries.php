<?php
function get_country_list()
{
	global $s,$db,$tb;
	$q = new sql($db);
	$sql = "SELECT * FROM {$tb['countries']} ORDER BY id";
	$q->query($sql);
	if ($q->numrows())
	{
		$i=0;
		while($rows=$q->getrows())
		{
			$countries[$i]['id'] = $rows['id'];
			$countries[$i]['countries_name'] = $rows['countries_name'];
			$i++;
		}
	}
	return $countries;
}
?>