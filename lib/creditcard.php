<?php
//delete cc for customer
function delete_customer_cc($cc_id)
{
	global $smarty,$s,$db,$tb;

	$q=new sql($db);
	$sql = "DELETE FROM {$tb['credit_card']} WHERE id = '$cc_id'";
	$q->query($sql);
}

//insert cc for customer
function insert_customer_cc($cid,$type,$cc_no)
{
	global $smarty,$s,$db,$tb;

	$q=new sql($db);
	$sql = "INSERT INTO {$tb['credit_card']} VALUES ('','$cid','$cc_no','$type',NOW(),DATE_ADD(NOW(),INTERVAL 1 YEAR),'active',NOW())";
	$q->query($sql);
}

//get cc type list
function get_cc_type_list()
{
	global $s,$db,$tb;
	$q = new sql($db);
	$sql = "SELECT * FROM {$tb['credit_card_type']} ORDER BY id";
	$q->query($sql);
	if ($q->numrows())
	{
		$i=0;
		while($rows=$q->getrows())
		{
			$cc_type[$i]['id'] = $rows['id'];
			$cc_type[$i]['type_name'] = $rows['type_name'];
			$i++;
		}
	}
	return $cc_type;
}

//get the cc no
function get_cc_no($cc_id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT cc_no FROM {$tb['credit_card']} WHERE id = '$cc_id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while($rows=$q->getrows())
		{
			$cc_no = $rows['cc_no'];
		}
	}
	return $cc_no;
}

// to check whether the credit card no is valid
function CCVal($Num, $Name='')
{
	$GoodCard = true;
	$Num = ereg_replace("[^[:digit:]]", "", $Num);
	switch ($Name)
	{
		case "1" :
	    $GoodCard = ereg("^5[1-5].{14}$", $Num);
		break;

		case "2" :
	    $GoodCard = ereg("^4.{15}$|^4.{12}$", $Num);
		break;
/*
		case "3" :
	    $GoodCard = ereg("^3[47].{13}$", $Num);
		break;

		case "4" :
	    $GoodCard = ereg("^6011.{12}$", $Num);
		break;

		case "5" :
	    $GoodCard = ereg("^30[0-5].{11}$|^3[68].{12}$", $Num);
		break;

		case "6" :
	    $GoodCard = ereg("^3.{15}$|^2131|1800.{11}$", $Num);
		break;
*/
	}

	$Num = strrev($Num);
	$Total = 0;
	for ($x=0; $x<strlen($Num); $x++)
	{
		$digit = substr($Num,$x,1);
	    if ($x/2 != floor($x/2))
		{
			$digit *= 2;
	        if (strlen($digit) == 2)
			{
				$digit = substr($digit,0,1) + substr($digit,1,1);
			}
        }
        $Total += $digit;
    }
    if ($GoodCard && $Total % 10 == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//generate the credit card no
function makeCardNo($type)
{
	global $s, $db, $tb;
	$q=new sql($db);
	$quit = 0;
	$j = "";
	do
	{
		if ($type==1)
		{
			$start = '5140';
		}
		elseif($type==2)
		{
			$start = '4556';
		}
		
		$i[0] = $start;
		$i[1] = '0101';
		$i[2] = '2020';
		$i[3] = mt_rand(0000,9999);

		while(strlen($i[3]) < 4)
		{
			$i[3] = "0".$i[3];
		}

		$j = implode("",$i);

		if (CCVal($j,$type) != 0)
		{
			$sql = "SELECT * FROM {$tb['credit_card']} WHERE cc_no = '$j'";
			$q->query($sql);
			if ($q->numrows())
			{
				$quit = 0;
			}
			else
			{
				$quit = 1;
			}
		}
		else
		{
			$quit = 0;
		}
	}while ($quit == 0) ;
	return $j;
}

//place payment for credit card
function do_cc_payment($cc_id,$aid,$amount)
{
	global $s,$db,$tb;

	$q = new sql($db);

	$date = date("Y-m-d H:i:s");

	//begin transaction
	$sql = "BEGIN;";
	$q->query($sql);

	//insert credit card payment record
	$sql = "INSERT INTO {$tb['credit_card_payment']} VALUES ('','$cc_id','$aid','$amount','$date');";
	$q->query($sql);

	$cc_no = get_cc_no($cc_id);
	$to = $s['company_account'];	
	$to_id = get_account_id($to);
	$from_account_no = get_account_no($aid);
	$remark = "Credit Card Payment ($cc_no : $from_account_no : $date)";

	//do transaction out
	$sql = "INSERT INTO {$tb['transaction_out']} VALUES ('','$aid','$date','$to','$amount','$remark');";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	//do transaction in
	$sql = "INSERT INTO {$tb['transaction_in']} VALUES ('','$to_id','$date','$from_account_no','$amount','$remark');";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	//update from balance amount
	$sql = "UPDATE {$tb['balance']} SET balance = balance - '$amount' WHERE aid = '$aid';";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	//update to balance amount
	$sql = "UPDATE {$tb['balance']} SET balance = balance + '$amount' WHERE aid = '$to_id';";
	$q->query($sql);
	if (mysql_error())
	{
		$error = true;
	}

	if ($error)
	{
		//rollback transacion
		$sql = "ROLLBACK;";
		$q->query($sql);
	}
	else
	{
		//commit transaction
		$sql = "COMMIT;";
		$q->query($sql);
	}
}
?>