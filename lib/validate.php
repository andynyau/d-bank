<?php
function errormsg($msg)
{
	global $errormsg;
	$errormsg[] = $msg;
}

function usernameCheck($username)
{
	global $s,$db,$tb;
	if ((!$username) || ($username==""))
	{ 
		errormsg("Missing <b>Username</b> Field (required)");
	}
	elseif ((strspn($username,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_ ")!=strlen($username)))
	{
		errormsg("Invalid <b>Username</b>");
	}
	elseif (strrpos($username,' ') > 0)
	{
		errormsg("There cannot be any spaces in the <b>Username</b>");
	}
	elseif (strlen($username) < 4)
	{
		errormsg("<b>Username</b> must be at least 4 characters");
	}
	else
	{
		$q=new sql($db);
		$sql = "SELECT username from {$tb['customer']} where username='$username'";
		$q->query($sql);
		if ($q->numrows())
		{
			errormsg("<b>Username</b> taken");
		}
	}
}

function passwdCheck($password, $passwordconfirm) {
	if ((!$password) || ($password=="") || (!$passwordconfirm) || ($passwordconfirm==""))
	{
		errormsg("Missing <b>Password</b> Field (required)");
	}
	elseif (strlen($password) < 4)
	{
		errormsg("<b>Password</b> must be at least 4 characters");
	}
	elseif ($password != $passwordconfirm)
	{
		errormsg("<b>Password</b> and <b>Confirm Password</b> not matched");
	}
}

function check_field($var,$fieldname,$req,$type,$minlen=0)
{
	if ($type == "text")
	{
		if (!$var)
		{
			if ($req)
			{ 
				errormsg("Missing <b>$fieldname</b> Field (required)");
			}
		}
		elseif (strlen($var)<$minlen)
		{
			errormsg("<b>$fieldname</b> Field must be at least $minlen characters");
		}
    	elseif (strspn($var,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_ ")!=strlen($var))
		{
			errormsg("Illegal character in <b>$fieldname</b> Field");
		}
	}
	elseif ($type == "num")
	{
		if (!$var)
		{
			if ($req)
			{
				errormsg("Missing <b>$fieldname</b> Field (required)");
			}
		}
		elseif (strlen($var)<$minlen)
		{
			errormsg("<b>$fieldname</b> Field must be at least $minlen characters");
		}
		elseif (strspn($var,"1234567890-.")!=strlen($var))
		{
       			errormsg("Illegal character in the <b>$fieldname</b> Field");
		}
    	elseif (strrpos($var," ") > 0)
		{
			errormsg("There cannot be any spaces in the <b>$fieldname</b> Field");
		}
	}
	elseif ($type == "any")
	{
		if (!$var)
		{
			if ($req)
			{
				errormsg("Missing <b>$fieldname</b> Field (required)");
			}
		}
		elseif (strlen($var)<$minlen)
		{
			errormsg("<b>$fieldname</b> Field must be at least $minlen characters");
		}
	}
}

function dateCheck($year, $month, $day, $fieldname)
{
	if (!checkdate($month,$day,$year))
	{
		errormsg("Illegal date in the <b>$fieldname</b> Field");
	}
	else
	{
		return "$year-$month-$day";
	}
}

function currencyCheck($num, $fieldname)
{
	if ((empty($num)) || ($num == 0))
	{
		errormsg("Missing <b>$fieldname</b> Field (required)");
	}
	elseif(!is_numeric($num))
	{
		errormsg("Illegal character in the <b>$fieldname</b> Field");
	}
}

function icCheck($ic)
{
	global $s,$db,$tb;
	
	$q=new sql($db);
	$sql = "SELECT ic_passport from {$tb['customer']} where ic_passport='$ic'";
	$q->query($sql);
	if ($q->numrows())
	{
		errormsg("<b>IC / Passport</b> is registered");
	}
}

function emailCheck($email, $emailconfirm)
{
	global $s,$db,$tb;
	if ((!$email) || ($email=="") || (!$emailconfirm) || ($emailconfirm==""))
	{ 
		errormsg("Missing <b>Email</b> Field (required)");
	}
	elseif ((!eregi("^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3}$",$email)))
	{
		errormsg("Invalid <b>Email addresses</b>");
	}
	elseif (strrpos($email,' ') > 0)
	{
		errormsg("<b>Email addresses<b> do not contain spaces");
	}
	elseif ($email != $emailconfirm)
	{
		errormsg("<b>Email address</b> and <b>Confirm Email address</b> not matched");
	}
	else
	{
		$q=new sql($db);
		$sql = "SELECT email from {$tb['email']} where email='$email'";
		$q->query($sql);
		if ($q->numrows())
		{
			errormsg("<b>Email</b> is registered");
		}
	}
}
?>