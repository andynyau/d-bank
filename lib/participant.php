<?php
//get the account list of the user
function get_participant_list()
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT id, company_name FROM {$tb['participant']} WHERE id <> '1'";
	$q->query($sql);
	if ($q->numrows())
	{
		$i = 0;
		while($rows=$q->getrows())
		{
			$participant[$i]['id'] = $rows['id'];
			$participant[$i]['company_name'] = $rows['company_name'];
			$i++;
		}
	}
	return $participant;
}

//get the company name of the participant
function get_participant_name($pid)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT company_name FROM {$tb['participant']} WHERE id = '$pid'";
	$q->query($sql);
	if ($q->numrows())
	{
		while($rows=$q->getrows())
		{
			$participant_name = $rows['company_name'];
		}
	}
	return $participant_name;
}

//get the participant info
function get_participant_info($pid)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT * FROM {$tb['participant']} WHERE id = '$pid'";
	$q->query($sql);
	if ($q->numrows())
	{
		while($rows=$q->getrows())
		{
			$participant_info = $rows;
		}
	}
	return $participant_info;
}
?>