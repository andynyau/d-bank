<?php
//delete account for customer
function delete_customer_account($aid)
{
	global $smarty,$s,$db,$tb;

	$q=new sql($db);
	$sql = "DELETE FROM {$tb['account']} WHERE id = '$aid'";
	$q->query($sql);
	$sql = "BEGIN;";
	$q->query($sql);
	$sql = "DELETE FROM {$tb['balance']} WHERE aid = '$aid'";
	$q->query($sql);
	$sql = "COMMIT;";
	$q->query($sql);
}

//insert account for customer
function insert_customer_account($cid,$type,$account_no,$amount)
{
	global $smarty,$s,$db,$tb;

	$q=new sql($db);
	$sql = "INSERT INTO {$tb['account']} VALUES ('','$cid','','$account_no','$type','active',NOW(),NOW())";
	$q->query($sql);
	$aid = get_last_insert_id($tb['account']);
	$sql = "BEGIN;";
	$q->query($sql);
	$sql = "INSERT INTO {$tb['balance']} VALUES ('','$aid','$amount')";
	$q->query($sql);
	$sql = "COMMIT;";
	$q->query($sql);
}

//delete acount for participant
function delete_participant_account($pid)
{
	global $smarty,$s,$db,$tb;

	$aid = get_participant_account_id($pid);
	$q=new sql($db);
	$sql = "DELETE FROM {$tb['account']} WHERE pid = '$pid'";
	$q->query($sql);
	$sql = "BEGIN;";
	$q->query($sql);
	$sql = "DELETE FROM {$tb['balance']} WHERE aid = '$aid'";
	$q->query($sql);
	$sql = "COMMIT;";
	$q->query($sql);
}

//insert acount to new add participant
function insert_participant_account($pid)
{
	global $smarty,$s,$db,$tb;

	$account_no = make_accountno('3');
	$q=new sql($db);
	$sql = "INSERT INTO {$tb['account']} VALUES ('','','$pid','$account_no','3','active',NOW(),NOW())";
	$q->query($sql);
	$aid = get_last_insert_id($tb['account']);
	$sql = "BEGIN;";
	$q->query($sql);
	$sql = "INSERT INTO {$tb['balance']} VALUES ('','$aid','')";
	$q->query($sql);
	$sql = "COMMIT;";
	$q->query($sql);
}

//to check whether the account no is exist
function check_account_exist($account_no)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT * FROM {$tb['account']} WHERE account_no = '$account_no' AND status = 'active'";
	$q->query($sql);
	if ($q->numrows() == 0)
	{
		errormsg("To Account not exist.");
	}
}

//get the account list of the user
function get_account_list($id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT id, account_no FROM {$tb['account']} WHERE cid = '$id' AND status = 'active'";
	$q->query($sql);
	if ($q->numrows())
	{
		$i = 0;
		while($rows=$q->getrows())
		{
			$account_no[$i]['id'] = $rows['id'];
			$account_no[$i]['account_no'] = $rows['account_no'];
			$i++;
		}
	}
	return $account_no;
}

//get account type list
function get_account_type_list()
{
	global $s,$db,$tb;
	$q = new sql($db);
	$sql = "SELECT * FROM {$tb['account_type']} ORDER BY id";
	$q->query($sql);
	if ($q->numrows())
	{
		$i=0;
		while($rows=$q->getrows())
		{
			$account_type[$i]['id'] = $rows['id'];
			$account_type[$i]['type_name'] = $rows['type_name'];
			$i++;
		}
	}
	return $account_type;
}

//get the type name of the account type
function get_account_type($id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT type_name FROM {$tb['account_type']} WHERE id = '$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while($rows=$q->getrows())
		{
			$type_name = $rows['type_name'];
		}
	}
	return $type_name;
}

//get the company account no of the participant
function get_participant_account_no($pid)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT account_no FROM {$tb['account']} WHERE pid = '$pid'";
	$q->query($sql);
	if ($q->numrows())
	{
		while($rows=$q->getrows())
		{
			$account_no = $rows['account_no'];
		}
	}
	return $account_no;
}

//get the company account no of the participant
function get_participant_account_id($pid)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT id FROM {$tb['account']} WHERE pid = '$pid'";
	$q->query($sql);
	if ($q->numrows())
	{
		while($rows=$q->getrows())
		{
			$id = $rows['id'];
		}
	}
	return $id;
}

//get the account no of the account
function get_account_no($id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT account_no FROM {$tb['account']} WHERE id = '$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while($rows=$q->getrows())
		{
			$account_no = $rows['account_no'];
		}
	}
	return $account_no;
}

//get the account_id of the account
function get_account_id($account_no)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT id FROM {$tb['account']} WHERE account_no = '$account_no'";
	$q->query($sql);
	if ($q->numrows())
	{
		while($rows=$q->getrows())
		{
			$id = $rows['id'];
		}
	}
	return $id;
}

//random generate the account no for new account
function make_accountno($type)
{
	global $s, $db, $tb;
	$q=new sql($db);
	$quit = 0;
	$i = "";
	do
	{
		$i = mt_rand(1,999999999);
		while(strlen($i) < 9)
		{
			$i = "0".$i;
		}
		$i = $type.$i;
	
		$sql = "SELECT * FROM {$tb['account']} WHERE account_no = '$i'";
		$q->query($sql);
		$c1 = $q->numrows();

		$sql = "SELECT * FROM {$tb['account_reserve']} WHERE account_no = '$i'";
		$q->query($sql);
		$c2 = $q->numrows();

		if ($c1)
		{
			$quit = 0;
		}
		elseif ($c2)
		{
			$quit = 0;
		}
		else
		{
			$quit = 1;
		}
	}while ($quit == 0) ;
	return $i;
}
?>