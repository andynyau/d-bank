<?php
class Smarty_Default extends Smarty {

   function Smarty_Default() {
		$this->Smarty();
		$this->template_dir = './tpl/';
		$this->compile_dir = './tpl_c/';
		$this->config_dir = '../lib/configs/';
   }
}

class Smarty_Admin extends Smarty {

   function Smarty_Admin() {
		$this->Smarty();
		$this->template_dir = './tpl/';
		$this->compile_dir = './tpl_c/';
		$this->config_dir = '../lib/configs/';
   }
}
?>