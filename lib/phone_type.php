<?php
function get_phone_type_list()
{
	global $s,$db,$tb;
	$q = new sql($db);
	$sql = "SELECT * FROM {$tb['phone_type']} ORDER BY id";
	$q->query($sql);
	if ($q->numrows())
	{
		$i=0;
		while($rows=$q->getrows())
		{
			$phone_type[$i]['id'] = $rows['id'];
			$phone_type[$i]['type_name'] = $rows['type_name'];
			$i++;
		}
	}
	return $phone_type;
}
?>