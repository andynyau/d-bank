<?php
function authentication()
{
	global $s,$db,$tb;
	if ((!isset($_SERVER['PHP_AUTH_USER'])) || ($_SERVER['PHP_AUTH_USER'] != $s['admin_user']) || (md5($_SERVER['PHP_AUTH_PW']) != $s['admin_pass']))
	{ 
			header('WWW-Authenticate: Basic realm="Admin Area"'); 
			header("HTTP/1.0 401 Unauthorized"); 
			echo "<center>";
			echo "<font size=\"4\" color=\"#ff0000\">Access Denied.</font><br>";
			echo "Valid <b>username</b> and <b>password</b> is required.<br>";
			echo "click <a href=\"?\">here</a> to login again.";
			echo "</center>";
			exit; 
	}
	else
	{
		$session = md5($s['cookies_name']);
		$q = new sql($db);
		$sql = "SELECT * FROM {$tb['setting']} WHERE `key` = 'admin_session' AND `value` = '$session'";
		$q->query($sql);
		$numrows=$q->numrows();
		$sql = "UPDATE {$tb['setting']} SET `value` = '$session' WHERE `key` = 'admin_session'";
		$q->query($sql);
		if (!$numrows)
		{
			header('WWW-Authenticate: Basic realm="Admin Area"'); 
			header("HTTP/1.0 401 Unauthorized"); 
			echo "<center>";
			echo "<font size=\"4\" color=\"#ff0000\">Access Denied.</font><br>";
			echo "Valid <b>username</b> and <b>password</b> is required.<br>";
			echo "click <a href=\"?\">here</a> to login again.";
			echo "</center>";
			exit; 
		}
	}
}

function logout_auth()
{
	global $s,$db,$tb;

	$q = new sql($db);
	$sql = "UPDATE {$tb['setting']} SET `value` = '' WHERE `key` = 'admin_session'";
	$q->query($sql);
}
?>