<?php
//get the userid of current user
function get_userid()
{
	global $s,$db,$tb;
	$session_fr_cookies = $_COOKIE[$s['cookies_name']];
	$session_decode = base64_decode($_COOKIE[$s['cookies_name']]);
	$cookies_var = explode(":",$session_decode);
	$username = $cookies_var[0];

	$q=new sql($db);
	$sql="SELECT id FROM {$tb['customer']} WHERE username='$username'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$id=$rows['id'];
		}
	}
	return $id;
}

//get address info
function get_address_info($id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql="SELECT * FROM {$tb['address']} WHERE id='$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$address=$rows;
		}
	}
	return $address;
}

//get phone info
function get_phone_info($id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql="SELECT * FROM {$tb['phone']} WHERE id='$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$phone=$rows;
		}
	}
	return $phone;
}

//get email ingo
function get_email_info($id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql="SELECT * FROM {$tb['email']} WHERE id='$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$email=$rows;
		}
	}
	return $email;
}

//get photo status
function get_photo_status($id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql="SELECT status FROM {$tb['customer_image']} WHERE id='$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$status=$rows['status'];
		}
	}
	return $status;
}

//get the record no
function get_record_no($id,$field,$sql)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$q->query($sql);
	if ($q->numrows())
	{
		$i=1;
		while($rows=$q->getrows())
		{
			if ($rows[$field] == $id)
			{
				$no = $i;
			}
			$i++;
		}
	}
	return $no;
}

//get the max id from the table
function get_final_id($table)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT MAX(id) as final FROM $table";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$final = $rows['final'];
		}
	}
	return $final;
}

//get last insert record's id from table
function get_last_insert_id($table)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT LAST_INSERT_ID() as new FROM $table";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$new = $rows['new'];
		}
	}
	return $new;
}

//get customer fullname
function get_customer_fullname($id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT fullname FROM {$tb['customer']} WHERE id = '$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$fullname = $rows['fullname'];
		}
	}
	return $fullname;
}

function print_arr($array)
{
	print "<pre>";
	print_r($array);
	print "</pre>";
}

?>