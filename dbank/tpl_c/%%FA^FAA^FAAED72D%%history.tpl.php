<?php /* Smarty version 2.6.5-dev, created on 2004-09-24 09:01:33
         compiled from history.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'history.tpl', 1, false),array('function', 'cycle', 'history.tpl', 115, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['validate']): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "sidebar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "login.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<td valign="top">
<h3>Transaction History</h3>
<?php if (count($_from = (array)$this->_tpl_vars['errormsg'])):
    foreach ($_from as $this->_tpl_vars['error']):
?>
	<font class="error"><?php echo $this->_tpl_vars['error']; ?>
</font><br>
<?php endforeach; unset($_from); endif; ?>
<?php if ($this->_tpl_vars['empty'] == 'no'): ?>
<table border="0" cellpadding="1" cellspacing="1">
<tr>
	<td valign="top">
		<form method="post" action="?">
		<table border="0" width="230" cellpadding="1" cellspacing="1">
		<tr>
			<td class="blacktb" colspan="3">Select Account</td>
		</tr>
		<tr>
			<td class="whitetb">
				<select name="account_id">
				<?php if (count($_from = (array)$this->_tpl_vars['account_list'])):
    foreach ($_from as $this->_tpl_vars['account_list_item']):
?>
					<option value="<?php echo $this->_tpl_vars['account_list_item']['id']; ?>
"><?php echo $this->_tpl_vars['account_list_item']['account_no']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
				</select>
				<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td class="blacktb">From</td>
		</tr>
		<tr>
			<td>
				<select name="from_year">
				<?php if (count($_from = (array)$this->_tpl_vars['yearlist'])):
    foreach ($_from as $this->_tpl_vars['itemyear']):
?>
					<option value="<?php echo $this->_tpl_vars['itemyear']; ?>
"<?php if ($this->_tpl_vars['itemyear'] == $this->_tpl_vars['year']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['itemyear']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
				</select>
				<select name="from_month">
				<?php if (count($_from = (array)$this->_tpl_vars['monthlist'])):
    foreach ($_from as $this->_tpl_vars['keymonth'] => $this->_tpl_vars['itemmonth']):
?>
					<option value="<?php echo $this->_tpl_vars['keymonth']; ?>
"<?php if ($this->_tpl_vars['keymonth'] == $this->_tpl_vars['month']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['itemmonth']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
				</select>
				<select name="from_day">
				<?php if (count($_from = (array)$this->_tpl_vars['daylist'])):
    foreach ($_from as $this->_tpl_vars['itemday']):
?>
					<?php if ($this->_tpl_vars['itemday'] < 10):  $this->assign('itemday', "0".($this->_tpl_vars['itemday']));  endif; ?>
					<option value="<?php echo $this->_tpl_vars['itemday']; ?>
"<?php if ($this->_tpl_vars['itemday'] == $this->_tpl_vars['day']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['itemday']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
				</select>
				<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td class="blacktb">To</td>
		</tr>
		<tr>
			<td>
			<select name="to_year">
			<?php if (count($_from = (array)$this->_tpl_vars['yearlist'])):
    foreach ($_from as $this->_tpl_vars['itemyear']):
?>
				<option value="<?php echo $this->_tpl_vars['itemyear']; ?>
"<?php if ($this->_tpl_vars['itemyear'] == $this->_tpl_vars['year']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['itemyear']; ?>
</option>
			<?php endforeach; unset($_from); endif; ?>
			</select>
			<select name="to_month">
			<?php if (count($_from = (array)$this->_tpl_vars['monthlist'])):
    foreach ($_from as $this->_tpl_vars['keymonth'] => $this->_tpl_vars['itemmonth']):
?>
				<option value="<?php echo $this->_tpl_vars['keymonth']; ?>
"<?php if ($this->_tpl_vars['keymonth'] == $this->_tpl_vars['month']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['itemmonth']; ?>
</option>
			<?php endforeach; unset($_from); endif; ?>
			</select>
			<select name="to_day">
			<?php if (count($_from = (array)$this->_tpl_vars['daylist'])):
    foreach ($_from as $this->_tpl_vars['itemday']):
?>
				<?php if ($this->_tpl_vars['itemday'] < 10):  $this->assign('itemday', "0".($this->_tpl_vars['itemday']));  endif; ?>
				<option value="<?php echo $this->_tpl_vars['itemday']; ?>
"<?php if ($this->_tpl_vars['itemday'] == $this->_tpl_vars['day']): ?> selected<?php endif; ?>><?php echo $this->_tpl_vars['itemday']; ?>
</option>
			<?php endforeach; unset($_from); endif; ?>
			</select>
			<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td class="blacktb">Type</td>
		</tr>
		<tr>
			<td>
				<input type="radio" name="type" value="in" checked>Transaction in
				<br>
				<input type="radio" name="type" value="out">Transaction out
				<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<input type="hidden" name="opt" value="history">
				<input type="hidden" name="act" value="view">
				<input type="submit" value="View" class="button1">
			</td>
		</tr>
		</table>
		</form>
	</td>
<?php if ($this->_tpl_vars['view'] == 'yes'): ?>
<td width="50">
&nbsp;
</td>
<td valign="top">
<table border="0" width="500" cellpadding="1" cellspacing="1">
	<tr class="blacktb">
	<?php unset($this->_sections['field_rows']);
$this->_sections['field_rows']['name'] = 'field_rows';
$this->_sections['field_rows']['loop'] = is_array($_loop=$this->_tpl_vars['field_values']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['field_rows']['show'] = true;
$this->_sections['field_rows']['max'] = $this->_sections['field_rows']['loop'];
$this->_sections['field_rows']['step'] = 1;
$this->_sections['field_rows']['start'] = $this->_sections['field_rows']['step'] > 0 ? 0 : $this->_sections['field_rows']['loop']-1;
if ($this->_sections['field_rows']['show']) {
    $this->_sections['field_rows']['total'] = $this->_sections['field_rows']['loop'];
    if ($this->_sections['field_rows']['total'] == 0)
        $this->_sections['field_rows']['show'] = false;
} else
    $this->_sections['field_rows']['total'] = 0;
if ($this->_sections['field_rows']['show']):

            for ($this->_sections['field_rows']['index'] = $this->_sections['field_rows']['start'], $this->_sections['field_rows']['iteration'] = 1;
                 $this->_sections['field_rows']['iteration'] <= $this->_sections['field_rows']['total'];
                 $this->_sections['field_rows']['index'] += $this->_sections['field_rows']['step'], $this->_sections['field_rows']['iteration']++):
$this->_sections['field_rows']['rownum'] = $this->_sections['field_rows']['iteration'];
$this->_sections['field_rows']['index_prev'] = $this->_sections['field_rows']['index'] - $this->_sections['field_rows']['step'];
$this->_sections['field_rows']['index_next'] = $this->_sections['field_rows']['index'] + $this->_sections['field_rows']['step'];
$this->_sections['field_rows']['first']      = ($this->_sections['field_rows']['iteration'] == 1);
$this->_sections['field_rows']['last']       = ($this->_sections['field_rows']['iteration'] == $this->_sections['field_rows']['total']);
?>
		<td>
			<a href="?opt=history&act=view&sort_by=<?php echo $this->_tpl_vars['field_values'][$this->_sections['field_rows']['index']];  if ($this->_tpl_vars['field_values'][$this->_sections['field_rows']['index']] == $this->_tpl_vars['sort_by'] && $this->_tpl_vars['sort_order'] == 'desc'): ?>&sort_order=<?php elseif ($this->_tpl_vars['field_values'][$this->_sections['field_rows']['index']] == $this->_tpl_vars['sort_by'] && $this->_tpl_vars['sort_order'] == ''): ?>&sort_order=desc<?php endif; ?>&start=<?php echo $this->_tpl_vars['start'];  echo $this->_tpl_vars['extra']; ?>
"><?php echo $this->_tpl_vars['field_names'][$this->_sections['field_rows']['index']]; ?>
</a>
		</td>
	<?php endfor; endif; ?>
	</tr>
	<?php if (count($_from = (array)$this->_tpl_vars['history_info'])):
    foreach ($_from as $this->_tpl_vars['history']):
?>
		<tr class="<?php echo smarty_function_cycle(array('values' => "orangetb1,greytb"), $this);?>
">
		<td width="20"><?php echo $this->_tpl_vars['history']['id']; ?>
</td>
		<td width="150"><?php echo $this->_tpl_vars['history']['datetime']; ?>
</td>
		<td width="100"><?php if ($this->_tpl_vars['type'] == 'in'):  echo $this->_tpl_vars['history']['from'];  else:  echo $this->_tpl_vars['history']['to'];  endif; ?></td>
		<td width="100"><?php echo $this->_tpl_vars['history']['amount']; ?>
</td>
		<td width="130"><?php echo $this->_tpl_vars['history']['remark']; ?>
</td>
		</tr>
	<?php endforeach; unset($_from); endif; ?>
	<tr>
		<td colspan="8" class="blacktb"><?php echo $this->_tpl_vars['pg_link']; ?>
</td>
	</tr>
</table>
</td>
<?php endif; ?>
</table>
<?php else: ?>
<font class="text">No Account is available for this service.</font>
<?php endif; ?>
</td>
      </tr>
    </table></td>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>