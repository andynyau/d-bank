<?php /* Smarty version 2.6.5-dev, created on 2004-09-26 06:18:30
         compiled from phone_form.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'phone_form.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['validate']): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "sidebar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "login.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<td valign="top">
	<h3><?php if ($this->_tpl_vars['type'] == 'add'): ?>Add Phone<?php else: ?>Edit Phone<?php endif; ?></h3>
	<?php if (count($_from = (array)$this->_tpl_vars['error'])):
    foreach ($_from as $this->_tpl_vars['error_item']):
?>
		<font class="error"><?php echo $this->_tpl_vars['error_item']; ?>
</font><br>
	<?php endforeach; unset($_from); endif; ?>
	<form method="post" action="?">
	<table width="400" height="100" border="0" class="blacktb">
	  <tr>
		<td>Phone Number</td>
		<td>
		  <input type="text" name="phone_no" class="textboxname" maxlength="64"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['phone_info']['phone_no']; ?>
"<?php endif; ?>>
		</td>
	  </tr>
	  <tr>
		<td>Phone Type</td>
		<td>
			<select name="phone_type">
			<?php if (count($_from = (array)$this->_tpl_vars['typelist'])):
    foreach ($_from as $this->_tpl_vars['ptype']):
?>
				<option value="<?php echo $this->_tpl_vars['ptype']['id']; ?>
"<?php if ($this->_tpl_vars['type'] == 'edit'):  if ($this->_tpl_vars['ptype']['id'] == $this->_tpl_vars['phone_info']['type_id']): ?> selected<?php endif;  else:  if ($this->_tpl_vars['ptype']['id'] == '1'): ?> selected<?php endif;  endif; ?>><?php echo $this->_tpl_vars['ptype']['type_name']; ?>
</option>
			<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	  </tr>
	  
	  <tr>
		<td>&nbsp;</td>
	  </tr>
	  <tr>
		<td colspan="2" align="center">
			<?php if ($this->_tpl_vars['type'] == 'edit'): ?><input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
"><?php else: ?><input type="hidden" name="final" value="<?php echo $this->_tpl_vars['final']; ?>
"><?php endif; ?>
			<input type="hidden" name="cid" value="<?php echo $this->_tpl_vars['cid']; ?>
">
			<input type="hidden" name="opt" value="phone">
			<input type="hidden" name="act" value="process">
			<input type="hidden" name="type" value="<?php echo $this->_tpl_vars['type']; ?>
">
			<input type="submit" value=<?php if ($this->_tpl_vars['type'] == 'add'): ?>"Add"<?php else: ?>"Edit"<?php endif; ?> class="button1">
			<input type="reset" value="Clear" class="button1">
		</td>
	  </tr>
	</table>
	<table border="0">
	  <tr>
		<td colspan="2" align="center">
			<a href="?opt=phone"><font class="text_u">Back</font></a><font class="text"> To Phone List.</font>
		</td>
	  </tr>
	</table>
	</form>
</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>