<?php /* Smarty version 2.6.5-dev, created on 2004-09-23 02:40:31
         compiled from signup_form.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'signup_form.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['validate']): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "sidebar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "login.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<td valign="top">
	<h3>Sign Up Form</h3>
	<?php if (count($_from = (array)$this->_tpl_vars['error'])):
    foreach ($_from as $this->_tpl_vars['error_item']):
?>
		<font class="error"><?php echo $this->_tpl_vars['error_item']; ?>
</font><br>
	<?php endforeach; unset($_from); endif; ?>
	<form method="post" action="?">
	<table width="400" height="300" border="0" class="blacktb">
	  <tr>
		<td>Username</td>
		<td>
		  <input type="text" name="username" class="textboxname" maxlength="16">
		</td>
	  </tr>
	  <tr>
		<td>Password</td>
		<td>
		  <input type="password" name="password" class="textboxpassword" maxlength="64">
		</td>
	  </tr>
	  <tr>
		<td>Confirm password </td>
		<td><input type="password" name="password2" class="textboxpassword" maxlength="64"></td>
	  </tr>
	  <tr>
		<td>Fullname</td>
		<td><input type="text" name="fullname" class="textboxname" maxlength="64"></td>
	  </tr>
	  <tr>
		<td>IC / Passport No </td>
		<td><input type="text" name="ic" class="textboxname" maxlength="32"></td>
	  </tr>
	  <tr>
		<td>Date Of Birth </td>
		<td>
			<select name="dob_year">
				<?php if (count($_from = (array)$this->_tpl_vars['year_list'])):
    foreach ($_from as $this->_tpl_vars['itemyear']):
?>
					<option value="<?php echo $this->_tpl_vars['itemyear']; ?>
"><?php echo $this->_tpl_vars['itemyear']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
			<select name="dob_month">
				<?php if (count($_from = (array)$this->_tpl_vars['month_list'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
					<option value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
			<select name="dob_day">
				<?php if (count($_from = (array)$this->_tpl_vars['day_list'])):
    foreach ($_from as $this->_tpl_vars['itemday']):
?>
					<?php if ($this->_tpl_vars['itemday'] < 10): ?>
						<option value="0<?php echo $this->_tpl_vars['itemday']; ?>
"><?php echo $this->_tpl_vars['itemday']; ?>
</option>
					<?php else: ?>
						<option value="<?php echo $this->_tpl_vars['itemday']; ?>
"><?php echo $this->_tpl_vars['itemday']; ?>
</option>
					<?php endif; ?>
				<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
	  </tr>
	  <tr>
		<td colspan="2" align="center">
			<input type="hidden" name="opt" value="register">
			<input type="hidden" name="act" value="process">
			<input type="submit" value="Sign Up" class="button1">
			<input type="reset" value="Clear" class="button1">
		</td>
	  </tr>
	</table>
	</form>
</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>