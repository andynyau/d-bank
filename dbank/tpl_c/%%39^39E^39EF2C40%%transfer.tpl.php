<?php /* Smarty version 2.6.5-dev, created on 2004-09-24 07:20:17
         compiled from transfer.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'transfer.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['validate']): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "sidebar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "login.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<td valign="top">
<h3>Fund Transfer</h3>
<?php if (count($_from = (array)$this->_tpl_vars['errormsg'])):
    foreach ($_from as $this->_tpl_vars['error']):
?>
	<font class="error"><?php echo $this->_tpl_vars['error']; ?>
</font><br>
<?php endforeach; unset($_from); endif; ?>
<?php if ($this->_tpl_vars['empty'] == 'no'): ?>
<form method="post" action="?">
<table border="0" width="420" cellpadding="1" cellspacing="1">
	<tr>
		<td class="blacktb">From Account</td><td class="blacktb">To Account</td><td class="blacktb">Amount</td><td class="blacktb">Remark</td>
	</tr>
	<tr>
		<td class="whitetb">
			<select name="account_id">
			<?php if (count($_from = (array)$this->_tpl_vars['account_list'])):
    foreach ($_from as $this->_tpl_vars['account_list_item']):
?>
				<option value="<?php echo $this->_tpl_vars['account_list_item']['id']; ?>
"><?php echo $this->_tpl_vars['account_list_item']['account_no']; ?>
</option>
			<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
		<td>
			<input type="text" name="to_account" class="textbox1" maxlength="10">
		</td>
		<td>
			<input type="text" name="amount" value="0.00" maxlength="13" class="textbox1">
		</td>
		<td>
			<input type="text" name="remark" maxlength="255" class="textbox1">
		</td>
	</tr>
	<tr>
	<td colspan="4" class="blacktb">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4">
			<input type="hidden" name="opt" value="transfer">
			<input type="hidden" name="act" value="process">
			<input type="submit" value="Transfer" class="button1">
		</td>
	</tr>
</table>
</form>
<?php else: ?>
<font class="text">No Account is available for this service.</font>
<?php endif; ?>
</td>
      </tr>
    </table></td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>