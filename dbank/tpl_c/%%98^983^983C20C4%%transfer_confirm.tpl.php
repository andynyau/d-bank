<?php /* Smarty version 2.6.5-dev, created on 2004-09-24 03:48:44
         compiled from transfer_confirm.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'transfer_confirm.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['validate']): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "sidebar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "login.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<td valign="top">
<h3>Confirm Fund Transfer</h3>
<?php if (count($_from = (array)$this->_tpl_vars['errormsg'])):
    foreach ($_from as $this->_tpl_vars['error']):
?>
	<font class="error"><?php echo $this->_tpl_vars['error']; ?>
</font><br>
<?php endforeach; unset($_from); endif; ?>
<form method="post" action="?">
<table border="0" width="500" cellpadding="1" cellspacing="1">
	<tr>
		<td class="whitetb">
			To : <b><?php echo $this->_tpl_vars['to']; ?>
</b>
		</td>
	</tr>
	<tr>
		<td class="whitetb">
			Amount : <b><?php echo $this->_tpl_vars['amount']; ?>
</b>
		</td>
	</tr>
	<tr>
		<td class="whitetb">
			Remark : <b><?php echo $this->_tpl_vars['remark']; ?>
</b>
		</td>
	</tr>
	<?php if ($this->_tpl_vars['limit'] == ''): ?>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="whitetb">
			* Your amount exceed the limit per transaction, this transaction will be pending for administrator approve.
		</td>
	</tr>
	<?php endif; ?>
	<tr>
		<td class="whitetb">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="whitetb">
			<input type="hidden" name="aid" value="<?php echo $this->_tpl_vars['from']; ?>
">
			<input type="hidden" name="to" value="<?php echo $this->_tpl_vars['to']; ?>
">
			<input type="hidden" name="amount" value="<?php echo $this->_tpl_vars['amount']; ?>
">
			<input type="hidden" name="remark" value="<?php echo $this->_tpl_vars['remark']; ?>
">
			<input type="hidden" name="limit" value="<?php if ($this->_tpl_vars['limit'] == ''): ?>0<?php else: ?>1<?php endif; ?>">
			<input type="hidden" name="final" value="<?php echo $this->_tpl_vars['final']; ?>
">
			<input type="hidden" name="opt" value="transfer">
			<input type="hidden" name="act" value="confirm">
			<input type="submit" value="Confirm" class="button1">
		</td>
	</tr>
</table>
</form>
</td>
      </tr>
    </table></td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>