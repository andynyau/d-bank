<?php /* Smarty version 2.6.5-dev, created on 2004-09-26 06:13:09
         compiled from address_form.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'address_form.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['validate']): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "sidebar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "login.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<td valign="top">
	<h3><?php if ($this->_tpl_vars['type'] == 'add'): ?>Add Address<?php else: ?>Edit Address<?php endif; ?></h3>
	<?php if (count($_from = (array)$this->_tpl_vars['error'])):
    foreach ($_from as $this->_tpl_vars['error_item']):
?>
		<font class="error"><?php echo $this->_tpl_vars['error_item']; ?>
</font><br>
	<?php endforeach; unset($_from); endif; ?>
	<form method="post" action="?">
	<table width="400" height="300" border="0" class="blacktb">
	  <tr>
		<td>Street</td>
		<td>
		  <input type="text" name="street" class="textboxlong" maxlength="64"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['address_info']['street']; ?>
"<?php endif; ?>>
		</td>
	  </tr>
	  <tr>
		<td>City</td>
		<td>
		  <input type="text" name="city" class="textboxname" maxlength="64"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['address_info']['city']; ?>
"<?php endif; ?>>
		</td>
	  </tr>
	  <tr>
		<td>Postcode</td>
		<td><input type="text" name="postcode" class="textboxname" maxlength="8"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['address_info']['postcode']; ?>
"<?php endif; ?>></td>
	  </tr>
	  <tr>
		<td>State</td>
		<td><input type="text" name="state" class="textboxname" maxlength="64"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['address_info']['state']; ?>
"<?php endif; ?>></td>
	  </tr>
	  <tr>
		<td>Country</td>
		<td>
			<select name="country">
			<?php if (count($_from = (array)$this->_tpl_vars['countrylist'])):
    foreach ($_from as $this->_tpl_vars['country']):
?>
				<option value="<?php echo $this->_tpl_vars['country']['id']; ?>
"<?php if ($this->_tpl_vars['type'] == 'edit'):  if ($this->_tpl_vars['country']['id'] == $this->_tpl_vars['address_info']['countries_id']): ?> selected<?php endif;  else:  if ($this->_tpl_vars['country']['id'] == '129'): ?> selected<?php endif;  endif; ?>><?php echo $this->_tpl_vars['country']['countries_name']; ?>
</option>
			<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	  </tr>
	  
	  <tr>
		<td>&nbsp;</td>
	  </tr>
	  <tr>
		<td colspan="2" align="center">
			<?php if ($this->_tpl_vars['type'] == 'edit'): ?><input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
"><?php else: ?><input type="hidden" name="final" value="<?php echo $this->_tpl_vars['final']; ?>
"><?php endif; ?>
			<input type="hidden" name="cid" value="<?php echo $this->_tpl_vars['cid']; ?>
">
			<input type="hidden" name="opt" value="address">
			<input type="hidden" name="act" value="process">
			<input type="hidden" name="type" value="<?php echo $this->_tpl_vars['type']; ?>
">
			<input type="submit" value=<?php if ($this->_tpl_vars['type'] == 'add'): ?>"Add"<?php else: ?>"Edit"<?php endif; ?> class="button1">
			<input type="reset" value="Clear" class="button1">
		</td>
	  </tr>
	</table>
	<table border="0">
	  <tr>
		<td colspan="2" align="center">
			<a href="?opt=address"><font class="text_u">Back</font></a><font class="text"> To Address List.</font>
		</td>
	  </tr>
	</table>
	</form>
</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>