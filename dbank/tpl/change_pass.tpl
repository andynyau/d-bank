{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Change Password</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
<form method="post" action="?">
<table border="0" width="420" cellpadding="1" cellspacing="1">
	<tr>
		<td class="blacktb">Old Password</td>
	</tr>
	<tr>
		<td class="whitetb">
			<input type="password" name="oldpassword" maxlength="64" class="textbox1">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="blacktb">New Password</td>
	</tr>
	<tr>
		<td class="whitetb">
			<input type="password" name="password1" maxlength="64" class="textbox1">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="blacktb">Confirm Password</td>
	</tr>
	<tr>
		<td class="whitetb">
			<input type="password" name="password2" maxlength="64" class="textbox1">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="opt" value="change_pass">
			<input type="hidden" name="act" value="process">
			<input type="submit" value="Change Password" class="button1">
		</td>
	</tr>
</table>
</form>
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}