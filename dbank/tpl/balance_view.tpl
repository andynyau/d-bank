{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
	<h3>Balance Inquiry</h3>
	<p>
	Account No: <b>{$account_no}</b>
	<br><br>
	Balance : <font class="big">{$balance}</font>
	</p>
	<br><br><br>
	<a href="?opt=balance"><font class="text_u">Back</font></a><font class="text"> To Balance Inquiry.</font>
</td>
{include file="footer.tpl"}