{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Bill Payment</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
<p>
	Bill Payment has done successfully!
	<br><br>
	To : <b>{$participant}</b>
	<br><br>
	Account No : <b>{$account_no}</b>
	<br><br>
	Amount : <b>{$amount}</b>
	<br><br>
	Bill No : <b>{$billno}</b>
	<br><br>
</p>
<a href="?opt=bill"><font class="text_u">Back</font></a><font class="text"> To Bill Payment.</font>
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}