{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Register Terms</h3>
<p class="text">
</p>
<p>
<center>
<textarea rows="16" cols="60">
D-Bank Online
Terms and Conditions of Access
 
For Personal Banking Services 

Please take a moment to read these terms and conditions carefully.

THE FOLLOWING TERMS AND CONDITIONS APPLY TO YOUR ACCESS AND THE USE OF THIS WEBSITE AND THE SERVICES PROVIDED HEREIN BY D-Bank. BY ACCESSING ANY PAGE OF THIS WEBSITE AND/OR USING THE SERVICES, YOU AGREE TO BE BOUND BY THESE TERMS AND CONDITIONS WITHOUT LIMITATION OR QUALIFICATION. IF YOU DO NOT ACCEPT THESE TERMS AND CONDITIONS, PLEASE IMMEDIATELY DISCONTINUE YOUR ACCESS TO THIS WEBSITE AND/OR USE OF THE SERVICES. 

General 

The information, products and services described or offered in this website are not intended for distribution to, or use by, any person or entity in any jurisdiction or country where such distribution or use would be contrary to law or regulation or which would subject D-Bank or its subsidiaries or affiliates to any registration requirement within such jurisdiction or country. 
The materials, information (including opinions and recommendations) and functions provided in this website shall not under any circumstances be considered or construed as an offer or solicitation to sell, buy, give, take, issue, allot or transfer, or as the giving of any advice in respect of shares, stocks, bonds, notes, interests, unit trusts, mutual funds or other securities, investments, loans, advances, credits or deposits in any jurisdiction. 

Reliance on your own judgement 

Although this site may contain or provide access or links to information, nor any opinions or recommendations on products and services of D-Bank or other parties, D-Bank does not represent that such products or services, opinions or recommendations are suitable for you. All transactions are done at your sole risk and responsibility and in reliance only upon your own judgement and you will make your own independent evaluation of the suitability of the recommendations, products and services obtained, offered or accessed through this website. Further you should seek professional advice at all times and obtain independent verification of the materials and information provided herein prior to making any investment, business or commercial decision based on any such materials or information. 
D-Bank shall have no responsibility or liability to you in respect of any opinions or recommendations expressed by it or any other parties on this site. None of the information, opinions or recommendations contained in this site constitutes a solicitation or offer by D-Bank to sell/buy any products or services, provide any investment or financial advice or an invitation to enter into any legally binding contract or arrangement with you. 
Your eligibility for any particular products or services is subject to the final determination and acceptance by D-Bank. 

Disclaimer 

The materials and information in this website, including but not limited to services, products, information, data, text, graphics, audio, video, links or other items, are provided by D-Bank on an "as is" and "as available" basis. All such materials and information (including text, graphics, links or other items) are subject to change without notice at our discretion. References to material and information contained in the website include such material and information provided by third parties. D-Bank advises you to visit, telephone, e-mail, fax or write to such third parties for more information or to confirm the information contained herein. 
D-Bank does not make any express or implied warranties, representations or endorsements including but not limited to any warranties of title, non-infringement, merchantability, usefulness, operation, completeness, currentness, accuracy, satisfactory quality, reliability, fitness for a particular purpose in respect of the website, the material, information and/or functions therein and expressly disclaims liability for errors and omissions in such materials, information and/or functions. Without derogation of the above and/or the terms and conditions of the applicable agreements governing all the products and services of D-Bank, reasonable measures will be taken by D-Bank to ensure the accuracy and validity of all information relating to transactions and products of D-Bank. 
Further D-Bank does not warrant or represent that access to the whole or part(s) of this website, the materials, information and/or functions contained therein will be provided uninterrupted or free from errors or that any identified defect will be corrected, or that there will be no delays, failures, errors or loss of transmitted information, that no viruses or other contaminating or destructive properties will be transmitted or that no damage will occur to your computer system. 

Links 

Should you leave this site via a link contained here and view content that is not provided by D-Bank Berhad, you do so at your own risk. D-Bank Berhad does not represent nor warranty any electronic content delivered by any third party, including, without limitation, the accuracy, completeness, quality or timeliness of any electronic content or materials and is not responsible or liable for damages or losses caused by any delays, defects or omission that may exist in the services, information or other content provided in such sites, whether actual, alleged, consequential or punitive. 
Furthermore, the links provided in this website to other shall not be considered an endorsement or verification or approval of such linked websites or the contents therein. Linking to any other site is at your sole risk and D-Bank will not be responsible or liable for any damages in connection with linking. It is advisable for you to read the privacy policy statements (if any) of any websites which are linked to this website. 

Copyright 

Unless otherwise indicated, the copyright in this website and its contents, including but not limited to the text, images, graphics, sound files, animation files, video files, and their arrangement, are the property of D-Bank, and are protected by applicable Malaysian and international copyright laws. No part or parts of this website may be modified, copied, distributed, retransmitted, broadcast, displayed, performed, reproduced, published, licensed, transferred, sold or commercially dealt with in any manner without the express prior written consent of D-Bank. 
You also may not, without D-Bank�s express prior written consent, insert a link to this website on any other website, frame or "mirror" any material contained on this website on any other server. 
Any such unauthorised reproduction, retransmission or other copying or modification of any of the contents of D-Bank�s website may be in breach of statutory or common law rights which could be the subject of legal action. D-Bank disclaims all liability which may arise from any unauthorised reproduction or use of the contents of this D-Bank�s website. 

Trademarks 

All trade marks, service marks, and logos displayed in this website are the property of D-Bank and/or their respective third party proprietors as identified in the website. 
Unless the prior written consent of D-Bank or the relevant third party proprietor of any of the trade marks, service marks or logos appearing on the website has been obtained, no license or right is granted to any party accessing this website to use, download, reproduce, copy or modify such trade marks, services marks or logos. Similarly, unless the prior written consent of D-Bank or the relevant proprietor has been obtained, no such trade mark, service mark or logo may be used as a link or to mark any link to the D-Bank�s website or any other site. 

Exclusion of Liability 

D-Bank SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGES HOWSOEVER ARISING WHETHER IN CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR ANY OTHER BASIS, INCLUDING WITHOUT LIMITATION, DIRECT OR INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, OR LOSS OF PROFITS OR LOSS OR SAVINGS ARISING IN CONNECTION WITH YOUR ACCESS OR USE OR THE INABILITY TO ACCESS OR USE THIS WEBSITE (OR ANY THIRD PARTY LINK TO OR FROM D-Bank�S WEBSITE), RELIANCE ON THE INFORMATION, OPINIONS OR RECOMMENDATIONS CONTAINED IN THE WEBSITE, ANY TECHNICAL, HARDWARE OR SOFTWARE FAILURE OF ANY KIND, THE INTERRUPTION, ERROR, OMMISSION, DELAY IN OPERATION, COMPUTER VIRUSES, OR OTHERWISE,WHETHER OR NOT D-Bank HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES OR LOSS. THIS EXCLUSION CLAUSE SHALL TAKE EFFECT TO THE FULLEST EXTENT PERMITTED BY LAW. 

Indemnity 

You will on demand indemnify and keep indemnified D-Bank from all liabilities, claims, losses and expenses, including any legal fees that may be incurred by D-Bank in connection with or arising from (1) your use or misuse of this website and the services provided herein, or (2) your breach of these terms and conditions howsoever occasioned, or (3) any intellectual property right or proprietary right infringement claim made by a third party against D-Bank in connection with your use of this website. 

Termination 

D-Bank reserves the right to terminate and/or suspend your access to this website and/or your use of this website at any time, for any reason. In particular, and without limitation, D-Bank may terminate and/or suspend your access should you violate any of these terms and conditions, or violate the rights of D-Bank, of any other user, or of any third party. 

Miscellaneous 

The failure of D-Bank to exercise or enforce any right or provision of these terms and conditions shall not constitute a waiver of such right or provision. If any part of these terms and conditions is determined to be invalid or unenforceable pursuant to applicable law, then the invalid and unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the other provisions of the terms and conditions shall continue in full force and effect. Any rights not expressly granted herein are reserved. 

Law and Jurisdiction 

These terms and conditions are governed by and are to be construed in accordance with the laws of Malaysia. By accessing this website and/or using the services provided herein by D-Bank, you hereby agree to submit to the non-exclusive jurisdiction of the Malaysian courts in Kuala Lumpur, Malaysia in all disputes arising out of or relating to the use of this website. 

INTERNET SECURITY MEASURES TO SAFEGUARD D-Bank�S INTERNET BANKING SERVICES ("D-Bank Online") 

We in D-Bank will use our best efforts to ensure that all D-Bank Online transactions performed by you through our website is secure, safe. For this purpose, we have put in place privacy protection control systems designed to ensure that security and confidentiality will not be compromised.
 
Username And Password 

To control access to our D-Bank Online services, every Customer is required to input your username and password. This username and password is the access key to your financial information. To ensure the integrity of your password, you are advised to do the following:- 

- When choosing a password, do not choose one that can be easily guessed by other person(s). For this purpose try to stay away from personal information such as your name, birth date, telephone number or words or names listed in a standard dictionary. 

- Memorise your password and do not write it down. 

- The password should never be revealed nor made accessible to anyone. It should not be disclosed even when requested to do so by an authorised officer of D-Bank.
 
- For your further protection, you are encouraged to change your password from time to time.
 
- Your password can be changed via the D-Bank Online Services. 

Customer�s Responsibilities 

Although, we exercise reasonable efforts to provide a safe and secure D-Bank Online services, we do not have control over the computer you use to access the D-Bank Online services. Therefore, on your part, please be advised of the following: 

- Please ensure that your computer does not provide any opportunities for anyone to gain access to your information. As an added security we have incorporated a feature in our D-Bank Online to automatically log you off when there is no activity detected for a period of time. 

- Please satisfy yourself that the equipment you are using will not allow eavesdropping or recording of your activities. 

- Always log off before visiting other Internet sites or once you have finished conducting your transactions. 

- Please do not send information about your account via e-mail. 

You should also not use or attempt to: 

- use the D-Bank Online services for any purpose other than that for which it was intended; 

- reverse engineer, decompile, disassemble or otherwise tamper with the D-Bank Online services or any systems or software operated by or on behalf of D-Bank or assist or permit anyone to do so.
 
Data Confidentiality and Data Integrity 

To ensure data confidentiality and integrity, all information transmitted over the Internet is encrypted using the Secure Server Layer secured 40-bit from Verisign Certificate Authority 

System Security and Monitoring 

To provide a secured environment for D-Bank Online, D-Bank adopts a combination of system security and monitoring measures: 

- Firewall systems, strong data encryption, anti-virus protection and round the clock security surveillance systems to detect and prevent any form of illegitimate activities on our network systems.
 
- Regular security reviews are conducted on our systems by our internal System Audit as well as external security experts. 

- Collaboration with major vendors/manufacturers to keep abreast of information security technology developments and implement where relevant.
</textarea>
</p>
<a href="?opt=register&act=agree"><font class="text_u">Agree</font></a> | <a href="?"><font class="text_u">Disagree</font></a>
</center>
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}