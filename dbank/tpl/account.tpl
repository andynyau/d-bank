{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Account</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
{if $empty == 'no'}
<table border="0" width="500" cellpadding="1" cellspacing="1">
	<tr class="blacktb">
	{section name = field_rows loop = $field_values}
		<td>
			<a href="?opt=account&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
		</td>
	{/section}
	{foreach from=$account_info item=account}
		<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">{$account.id}</td>
		<td width="150">{$account.account_no}</td>
		<td width="100">{$account.type}</td>
		<td width="200">{$account.date_created}</td>
		<td>{$account.status}</td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="8" class="blacktb">{$pg_link}</td>
	</tr>
</table>
{else}
<font class="text">No Account is available for this service.</font>
{/if}
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}