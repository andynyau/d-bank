{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Transaction History</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
{if $empty == 'no'}
<table border="0" cellpadding="1" cellspacing="1">
<tr>
	<td valign="top">
		<form method="post" action="?">
		<table border="0" width="230" cellpadding="1" cellspacing="1">
		<tr>
			<td class="blacktb" colspan="3">Select Account</td>
		</tr>
		<tr>
			<td class="whitetb">
				<select name="account_id">
				{foreach from=$account_list item=account_list_item}
					<option value="{$account_list_item.id}">{$account_list_item.account_no}</option>
				{/foreach}
				</select>
				<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td class="blacktb">From</td>
		</tr>
		<tr>
			<td>
				<select name="from_year">
				{foreach from = $yearlist item = itemyear}
					<option value="{$itemyear}"{if $itemyear == $year} selected{/if}>{$itemyear}</option>
				{/foreach}
				</select>
				<select name="from_month">
				{foreach from=$monthlist key=keymonth item=itemmonth}
					<option value="{$keymonth}"{if $keymonth == $month} selected{/if}>{$itemmonth}</option>
				{/foreach}
				</select>
				<select name="from_day">
				{foreach from=$daylist item=itemday}
					{if $itemday lt 10}{assign var="itemday" value="0$itemday"}{/if}
					<option value="{$itemday}"{if $itemday == $day} selected{/if}>{$itemday}</option>
				{/foreach}
				</select>
				<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td class="blacktb">To</td>
		</tr>
		<tr>
			<td>
			<select name="to_year">
			{foreach from = $yearlist item = itemyear}
				<option value="{$itemyear}"{if $itemyear == $year} selected{/if}>{$itemyear}</option>
			{/foreach}
			</select>
			<select name="to_month">
			{foreach from=$monthlist key=keymonth item=itemmonth}
				<option value="{$keymonth}"{if $keymonth == $month} selected{/if}>{$itemmonth}</option>
			{/foreach}
			</select>
			<select name="to_day">
			{foreach from=$daylist item=itemday}
				{if $itemday lt 10}{assign var="itemday" value="0$itemday"}{/if}
				<option value="{$itemday}"{if $itemday == $day} selected{/if}>{$itemday}</option>
			{/foreach}
			</select>
			<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td class="blacktb">Type</td>
		</tr>
		<tr>
			<td>
				<input type="radio" name="type" value="in" checked>Transaction in
				<br>
				<input type="radio" name="type" value="out">Transaction out
				<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<input type="hidden" name="opt" value="history">
				<input type="hidden" name="act" value="view">
				<input type="submit" value="View" class="button1">
			</td>
		</tr>
		</table>
		</form>
	</td>
{if $view == 'yes'}
<td width="50">
&nbsp;
</td>
<td valign="top">
<table border="0" width="500" cellpadding="1" cellspacing="1">
	<tr class="blacktb">
	{section name = field_rows loop = $field_values}
		<td>
			<a href="?opt=history&act=view&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}{$extra}">{$field_names[field_rows]}</a>
		</td>
	{/section}
	</tr>
	{foreach from=$history_info item=history}
		<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">{$history.id}</td>
		<td width="150">{$history.datetime}</td>
		<td width="100">{if $type == 'in'}{$history.from}{else}{$history.to}{/if}</td>
		<td width="100">{$history.amount}</td>
		<td width="130">{$history.remark}</td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="8" class="blacktb">{$pg_link}</td>
	</tr>
</table>
</td>
{/if}
</table>
{else}
<font class="text">No Account is available for this service.</font>
{/if}
</td>
      </tr>
    </table></td>

{include file="footer.tpl"}