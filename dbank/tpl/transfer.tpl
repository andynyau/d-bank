{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Fund Transfer</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
{if $empty == 'no'}
<form method="post" action="?">
<table border="0" width="420" cellpadding="1" cellspacing="1">
	<tr>
		<td class="blacktb">From Account</td><td class="blacktb">To Account</td><td class="blacktb">Amount</td><td class="blacktb">Remark</td>
	</tr>
	<tr>
		<td class="whitetb">
			<select name="account_id">
			{foreach from=$account_list item=account_list_item}
				<option value="{$account_list_item.id}">{$account_list_item.account_no}</option>
			{/foreach}
			</select>
		</td>
		<td>
			<input type="text" name="to_account" class="textbox1" maxlength="10">
		</td>
		<td>
			<input type="text" name="amount" value="0.00" maxlength="13" class="textbox1">
		</td>
		<td>
			<input type="text" name="remark" maxlength="255" class="textbox1">
		</td>
	</tr>
	<tr>
	<td colspan="4" class="blacktb">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4">
			<input type="hidden" name="opt" value="transfer">
			<input type="hidden" name="act" value="process">
			<input type="submit" value="Transfer" class="button1">
		</td>
	</tr>
</table>
</form>
{else}
<font class="text">No Account is available for this service.</font>
{/if}
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}