{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Confirm Fund Transfer</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
<form method="post" action="?">
<table border="0" width="500" cellpadding="1" cellspacing="1">
	<tr>
		<td class="whitetb">
			To : <b>{$to}</b>
		</td>
	</tr>
	<tr>
		<td class="whitetb">
			Amount : <b>{$amount}</b>
		</td>
	</tr>
	<tr>
		<td class="whitetb">
			Remark : <b>{$remark}</b>
		</td>
	</tr>
	{if $limit == ''}
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="whitetb">
			* Your amount exceed the limit per transaction, this transaction will be pending for administrator approve.
		</td>
	</tr>
	{/if}
	<tr>
		<td class="whitetb">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="whitetb">
			<input type="hidden" name="aid" value="{$from}">
			<input type="hidden" name="to" value="{$to}">
			<input type="hidden" name="amount" value="{$amount}">
			<input type="hidden" name="remark" value="{$remark}">
			<input type="hidden" name="limit" value="{if $limit == ''}0{else}1{/if}">
			<input type="hidden" name="final" value="{$final}">
			<input type="hidden" name="opt" value="transfer">
			<input type="hidden" name="act" value="confirm">
			<input type="submit" value="Confirm" class="button1">
		</td>
	</tr>
</table>
</form>
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}