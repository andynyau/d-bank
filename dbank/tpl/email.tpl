{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Email</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
<table border="0" width="500" cellpadding="1" cellspacing="1">
{if $empty eq "no"}
	<tr class="blacktb">
	{section name = field_rows loop = $field_values}
		<td>
			<a href="?opt=email&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
		</td>
	{/section}
	<td>Action</td>
	{foreach from=$email_info item=email}
		{if $email.primary == 'YES'}
		<tr class="whitetb">
		{else}
		<tr class="{cycle values="orangetb1,greytb"}">
		{/if}
		<td width="20">{$email.id}</td>
		<td width="260">{$email.email}</td>
		<td width="100">{$email.status}</td>
		<td align="center">{$email.edit}&nbsp;{$email.delete}{if $email.status == 'unverify'}&nbsp;{$email.resend}{else}&nbsp;{$email.update_primary}{/if}</td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="8" class="blacktb">{$pg_link}</td>
	</tr>
	<tr><td colspan="8">&nbsp;</td></tr>
	<tr>
		<td colspan="8" class="whitetb">
		<table border="0">
		<tr><td>
			<form method="post" action="?">
				<input type="hidden" name="opt" value="email">
				<input type="hidden" name="act" value="add_new">
				<input type="submit" value="Add New Email" class="button1">
			</form>
		</td></tr>
		</table>
		</td>
	</tr>
{else}
<tr><td>
	<form method="post" action="?">
		<input type="hidden" name="opt" value="email">
		<input type="hidden" name="act" value="add_new">
		<input type="submit" value="Add New Email" class="button1">
	</form>
</td></tr>
{/if}
</table>

</td>
      </tr>
    </table></td>
{include file="footer.tpl"}