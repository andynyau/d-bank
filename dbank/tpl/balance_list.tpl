{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Balance Inquiry</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
{if $empty == 'no'}
<form method="post" action="?">
<table border="0" width="150" cellpadding="1" cellspacing="1">
	<tr>
		<td class="blacktb">
			Account :
		</td>
	</tr>
	<tr>
		<td>
			<select name="account_id">
			{foreach from=$account_list item=account_list_item}
				<option value="{$account_list_item.id}">{$account_list_item.account_no}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="opt" value="balance">
			<input type="hidden" name="act" value="view">
			<input type="submit" value="View Balance" class="button1">
		</td>
	</tr>
</table>
{else}
<font class="text">No Account is available for this service.</font>
{/if}
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}