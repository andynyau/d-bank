{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
	<a name="top"></a>
	<h3>Help</h3>
	<h4>Personal Profile</h4>
	<p>
	<b>Sign Up</b>
	<br>
	<a href="#signup1"><font class="text_u">1. What is required for sign up as a customer?</font></a><br>
	<a href="#signup2"><font class="text_u">2. How to sign up as a customer?</font></a><br>
	</p>
	<p>
	<b>Address</b>
	<br>
	<a href="#address1"><font class="text_u">1. How to add address?</font></a><br>
	<a href="#address2"><font class="text_u">2. How to edit address?</font></a><br>
	<a href="#address3"><font class="text_u">3. How to delete address?</font></a><br>
	<a href="#address4"><font class="text_u">4. How to update address as primary?</font></a><br>
	</p>
	<p>
	<b>Phone</b>
	<br>
	<a href="#phone1"><font class="text_u">1. How to add phone?</font></a><br>
	<a href="#phone2"><font class="text_u">2. How to edit phone?</font></a><br>
	<a href="#phone3"><font class="text_u">3. How to delete phone?</font></a><br>
	<a href="#phone4"><font class="text_u">4. How to update phone as primary?</font></a><br>
	</p>
	<p>
	<b>Email</b>
	<br>
	<a href="#email1"><font class="text_u">1. How to add email?</font></a><br>	
	<a href="#email2"><font class="text_u">2. How to edit email?</font></a><br>
	<a href="#email3"><font class="text_u">3. How to delete email?</font></a><br>
	<a href="#email4"><font class="text_u">4. How to update email as primary?</font></a><br>
	<a href="#email5"><font class="text_u">5. How to verify email?</font></a><br>
	</p>
	<p>
	<b>Photo</b>
	<br>
	<a href="#photo1"><font class="text_u">1. How to upload photo?</font></a><br>
	</p>
	<p>
	<b>Password</b>
	<br>
	<a href="#password1"><font class="text_u">1. How to change password?</font></a><br>
	</p>
	<br>
	<h4>Services</h4>
	<p>
	<b>Balance Inquiry</b>
	<br>
	<a href="#balance1"><font class="text_u">1. How to view balance?</font></a><br>
	</p>
	<p>
	<b>View Account</b>
	<br>
	<a href="#account1"><font class="text_u">1. How to view account?</font></a><br>
	<a href="#account2"><font class="text_u">2. How to activate barred account?</font></a><br>
	</p>
	<p>
	<b>Fund Transfer</b>
	<br>
	<a href="#transfer1"><font class="text_u">1. How to transfer fund?</font></a><br>
	<a href="#transfer2"><font class="text_u">2. What is per transaction limit?</font></a><br>
	<a href="#transfer3"><font class="text_u">3. What is daily transaction limit?</font></a><br>
	</p>
	<p>
	<b>Transaction History</b>
	<br>
	<a href="#history1"><font class="text_u">1. What is transaction history?</font></a><br>
	<a href="#history2"><font class="text_u">2. How to view transaction history?</font></a><br>
	</p>
	<p>
	<b>Credit Card Payment</b>
	<br>
	<a href="#creditcard1"><font class="text_u">1. What is credit card payment?</font></a><br>
	<a href="#creditcard2"><font class="text_u">2. How to pay credit card payment?</font></a><br>
	</p>
	<p>
	<b>Bill Payment</b>
	<br>
	<a href="#bill1"><font class="text_u">1. What is bill payment?</font></a><br>
	<a href="#bill2"><font class="text_u">2. How to pay bill payment?</font></a><br>
	</p>
	<br><br><br><br>
	<p>
	<b>Sign Up</b>
	<br>
	<a name="signup1"><font class="text">1. What is required for sign up as customer?</font></a><br><br>
	To sign up as a customer, you need to submit information like full name, IC / Passport Number and date of birth. The additional information that is required is a user name and password for login into the system.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="signup2"><font class="text">2. How to sign up as customer?</font></a><br><br>
	To sign up as a customer, click on the sign up link at the left menu, a form will display that request for the required information for sign up as new customer. Submit the valid and real information, your account will be created immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>Address</b>
	<br>
	<a name="address1"><font class="text">1. How to add address?</font></a><br><br>
	To add a new address, just click on the Add New Address button, a form will display that request for the information for add new address. Submit the valid and real information, a new address will be added immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="address2"><font class="text">2. How to edit address?</font></a><br><br>
	To edit the existing address, click on the edit image button <img src="{$img_path}button_edit.png"> for the address that you want to edit, a form will display that request for the required information for edit existing address. Submit the valid and real information, the existing address will be edited immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="address3"><font class="text">3. How to delete address?</font></a><br><br>
	To delete the existing address, click on the delete image button <img src="{$img_path}button_delete.png"> for the address that you want to delete, a message box will pop-up to reconfirm your action for deleting the existing address. If you confirm to delete the address, the existing address will be deleted immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="address4"><font class="text">4. How to update address as primary?</font></a><br><br>
	To update the specific address as primary, click on the update primary image button <img src="{$img_path}button_primary.png"> for the address that you want to update as primary, a message box will pop-up to reconfirm your action for updating the address as primary. If you confirm to update the address as primary, the address will be updated as primary and the old primary address will be updated as not primary immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>Phone</b>
	<br>
	<a name="phone1"><font class="text">1. How to add phone?</font></a><br><br>
	To add a new phone, just click on the Add New phone button, a form will display that request for the information for add new phone. Submit the valid and real information, a new phone will be added immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="phone2"><font class="text">2. How to edit phone?</font></a><br><br>
	To edit the existing phone, click on the edit image button <img src="{$img_path}button_edit.png"> for the phone that you want to edit, a form will display that request for the required information for edit existing phone. Submit the valid and real information, the existing phone will be edited immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="phone3"><font class="text">3. How to delete phone?</font></a><br><br>
	To delete the existing phone, click on the delete image button <img src="{$img_path}button_delete.png"> for the phone that you want to delete, a message box will pop-up to reconfirm your action for deleting the existing phone. If you confirm to delete the phone, the existing phone will be deleted immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="phone4"><font class="text">4. How to update phone as primary?</font></a><br><br>
	To update the specific phone as primary, click on the update primary image button <img src="{$img_path}button_primary.png"> for the phone that you want to update as primary, a message box will pop-up to reconfirm your action for updating the phone as primary. If you confirm to update the phone as primary, the phone will be updated as primary and the old primary phone will be updated as not primary immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>Email</b>
	<br>
	<a name="email1"><font class="text">1. How to add email?</font></a><br><br>
	To add a new email, just click on the Add New email button, a form will display that request for the information for add new email. Submit the valid and real information, a new email will be added immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="email2"><font class="text">2. How to edit email?</font></a><br><br>
	To edit the existing email, click on the edit image button <img src="{$img_path}button_edit.png"> for the email that you want to edit, a form will display that request for the required information for edit existing email. Submit the valid and real information, the existing email will be edited immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="email3"><font class="text">3. How to delete email?</font></a><br><br>
	To delete the existing email, click on the delete image button <img src="{$img_path}button_delete.png"> for the email that you want to delete, a message box will pop-up to reconfirm your action for deleting the existing email. If you confirm to delete the email, the existing email will be deleted immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="email4"><font class="text">4. How to update email as primary?</font></a><br><br>
	To update the specific email as primary, click on the update primary image button <img src="{$img_path}button_primary.png"> for the email that you want to update as primary, a message box will pop-up to reconfirm your action for updating the email as primary. If you confirm to update the email as primary, the email will be updated as primary and the old primary email will be updated as not primary immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="email5"><font class="text">5. How to verify email?</font></a><br><br>
	After an email is added or edited, it is required to verify it with the URL link that sent to the added or edited email. If the email is not received, you can request for resend the verify link to your email. To request verify link for the email, click on the resend verify image button <img src="{$img_path}button_verify.png"> for the email that you want to request for the verify link, a message box will pop-up to reconfirm your action for resend the verify link. If you confirm to request to resend the verify link, the verify link will send to the email immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>Photo</b>
	<br>
	<a name="photo1"><font class="text">1. How to upload photo?</font></a><br><br>
	It is optional for customer to upload a photo to the server, the photo will be use for the attachment on the credit card. To upload a photo to the server, just browse and choose which photo you want to upload and submit it the system. Maximum file size for the photo is 2 Megabytes, the photo will uploaded and shown on the screen immediately, but the status of the photo is required approve from D-Bank administrator before it use on the credit card.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>Password</b>
	<br>
	<a name="password1"><font class="text">1. How to change password?</font></a><br><br>
	It is recommended to every customer to change the password frequenty. To change the login password, click on the change password link at the side menu bar, a form will display that request for the old password and new password. Submit the valid and real information, the login password will be updated immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br><br><br><br>
	<p>
	<b>Balance Inquiry</b>
	<br>
	<a name="balance1"><font class="text">1. How to view balance?</font></a><br><br>
	To view the balance remain in the account, you are required to tell which account to view by selecting the account number at the drop down box. Only the active account will be available to use this service. When the account is selected, the balance that remain in the account will be shown in a new page.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>View Account</b>
	<br>
	<a name="account1"><font class="text">1. How to view account?</font></a><br><br>
	To view the account that you have on the D-Bank system, just click on the View Account Link at the side menu bar, a list of table will show all your accounts and the information of the accounts. The information that will show to customer are the account number, account type, date created and the current status of the account.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="account2"><font class="text">2. How to activate barred account?</font></a><br><br>
	Account is barred by the D-Bank administrator for many different types of reason. To activate the barred account, customer are required to inquire D-Bank for ways in order to active the barred account. For more information about activate barred account, please contact us at any of our local branches.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>Fund Transfer</b>
	<br>
	<a name="transfer1"><font class="text">1. How to transfer fund?</font></a><br><br>
	To transfer fund to another D-Bank account, you are required to tell which account to transfer fund by selecting the account number at the drop down box. Only the active account will be available to use this service. The account that receive the fund is required to be an existing D-Bank account. The amount must not be left blank or input a value of 0. Remark is optional that depends on the customer. If the transaction will not proceed normally if the amount exceed the daily transaction limit or per transaction limit. If the fund transfer is submited with valid information and does not exceed the daily transaction limit or per transaction limit, the balance with deducted from the source account and added to the destination account immediately.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="transfer2"><font class="text">2. What is per transaction limit?</font></a><br><br>
	Per transaction limit is the limit that the customer can transfer the fund in a single transaction, when the amount has exceeded the per transaction limit, the transaction will be pending to get approvement from D-Bank administrator when the transaction is proven to be valid with the customer. When the pending transaction is approved, the amount will deducted from the source account and added to the destination account.<br><br>
	<a name="transfer3"><font class="text">3. What is daily transaction limit?</font></a><br><br>
	Daily transaction limit is the limit that the customer can transfer the fund in a day, when the amount and the total amount of the transaction done on that day has exceeded the daily transaction limit, the transaction will not proceed under any condition and situation<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>Transaction History</b>
	<br>
	<a name="history1"><font class="text">1. What is transaction history?</font></a><br><br>
	Transaction history is a service that allow customer to view the history of transaction(s) that the customer done on the D-Bank system. The customer is allow to view the history that done with a period of time that input by the customer. Invalid input of date will not showing the right results to customers.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="history2"><font class="text">2. How to view transaction history?</font></a><br><br>
	To view transaction history, the customer need to input the from date and the to date of the period of time. Input the valid date and the transaction that done within the period will show.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>Credit Card Payment</b>
	<br>
	<a name="creditcard1"><font class="text">1. What is credit card payment?</font></a><br><br>
	Credit card payment is use D-Bank account to repay the debt that the customer owe to D-Bank on using credit card.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="creditcard2"><font class="text">2. How to pay credit card payment?</font></a><br><br>
	To pay credit card payment, the customer need to select the account for this service at the drop-down box, amount is required to input for repay how much to D-Bank. The amount must not be left blank or input a value of 0. If valid information is submited, the transaction will proceed immediately, the amount will deducted from the customer's account and added to D-Bank company bank account.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
	<br>
	<p>
	<b>Bill Payment</b>
	<br>
	<a name="bill1"><font class="text">1. What is bill payment?</font></a><br><br>
	Bill payment is use D-Bank account to repay the bill that the customer need to pay to D-Bank participant.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	<a name="bill2"><font class="text">2. How to pay bill payment?</font></a><br><br>
	To pay bill payment, the customer need to select the account for this service at the drop-down box, amount is required to input for repay how much to D-Bank participant. The amount must not be left blank or input a value of 0. If valid information is submited, the transaction will proceed immediately, the amount will deducted from the customer's account and added to D-Bank participant's company bank account.<br><br>
	Back to <a href="#top"><font class="text_u">Top</font></a><br><br>
	</p>
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}