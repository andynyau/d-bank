{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Credit Card Payment</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
<form method="post" action="?">
<table border="0" width="200" cellpadding="1" cellspacing="1">
	<tr>
		<td class="blacktb">
			Account:
		</td>
	</tr>
	<tr>
		<td>
			<select name="account_id">
			{foreach from=$account_list item=account_list_item}
				<option value="{$account_list_item.id}">{$account_list_item.account_no}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td class="blacktb">
			Amount to pay:
		</td>
	</tr>
	<tr>
		<td>
			<input type="text" name="amount" class="textbox1">
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="opt" value="creditcard">
			<input type="hidden" name="act" value="process">
			<input type="hidden" name="final" value="{$final}">
			<input type="hidden" name="cc_id" value="{$cc_id}">
			<input type="submit" value="Pay Credit Card" class="button1">
		</td>
	</tr>
</table>
</form>
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}