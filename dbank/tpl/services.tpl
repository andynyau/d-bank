{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
	<h3>Services</h3>
	<table border="0" width="750">
	<tr class="whitetb">
		<td>
		Please take a moment to read these services carefully. 
		<br><br>
		THE FOLLOWING SERVICES ARE PROVIDED BY D-BANK BANKING BERHAD. PLEASE REFER TO THE TERMS AND CONDITIONS BEFORE USING THE SERVICES FOR NOT VIOLATING THE RULES AND REGULATIONS USING THESE SERVICES.
		<br><br>
		CUSTOMERS ARE REQUIRED TO AGREE THE TERMS AND CONDITON WHILE USING THESE SERVICES. D-BANK BAKING BERHAD WILL NOT RESPONSIBLE FOR ANY LOST OR ERROR THAT CAUSE BY THE CUSTOMER HIM/HERSELF.
		<br><br>
		<b>General</b>
		<br><br>
		The term "D-Bank"  as used in these terms and conditions refers to D-Bank Banking Berhad. The term "the D-Bank Group" refers to D-Bank Banking Berhad and its subsidiaries, either individually and/or collectively as the context requires.
		<br><br><br>
		<b>Balance Inquiry</b>
		<br><br>
		Customers are allowed to view their balance remain in the accounts. For viewing balance, only accounts that are active is allowed to use this service. 
		<br><br>
		For more information about using this service, please contact us at any of our locate branches.
		<br><br><br>
		
		<b>View Account</b>
		<br><br>
		Customers are allowed to view the status of the accounts. Accounts that are allowed for this service are saving account, current account and company account.
		<br><br>
		For more information about using this service, please contact us at any of our local branches.
		<br><br><br>

		<b>Fund Transfer</b>
		<br><br>
		Customers are allowed to transfer amount in the account to any of the D-Bank accounts. For transfering fund, amount that exceeds the per transaction limit will require approvement from D-Bank administrator. Transfering fund is not allowed if the amount has exceeded the daily transaction limit.
		<br><br>
		For more information about using this service, please contact us at any of our local branches.
		<br><br><br>

		<b>Transaction History</b>
		<br><br>
		Customers are allowed to view the history of transation made. All transaction records will show for the debit and credit of the account with the date and time.
		<br><br>
		For more information about using this service, please contact us at any of our local branches.
		<br><br><br>

		<b>Credit Card Payment</b>
		<br><br>
		Customers are allowed to pay the credit card debt by using the D-Bank accounts. The amount for this service as credit card payment is to be input by customers, D-Bank will not deduct the exact amount as the total credit card detb from the D-Bank accounts.
		<br><br>
		For more information about using this service, please contact us at any of our local branches.
		<br><br><br>

		<b>Bill Payment</b>
		<br><br>
		Customers are allowed to pay the bill of the D-Bank participant by using the D-Bank accounts. The amount for this service as bill payment is to be input by customers, D-Bank will not deduct the exact amount as the total bill amount from the D-Bank accounts
		<br><br>
		For more information about using this service, please contact us at any of our local branches.
		<br><br><br>
		</td>
	</tr>
	</table>
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}