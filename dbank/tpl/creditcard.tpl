{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
<h3>Credit Card</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
{if $empty == 'no'}
<table border="0" width="650" cellpadding="1" cellspacing="1">
	<tr class="blacktb">
	{section name = field_rows loop = $field_values}
		<td>
			<a href="?opt=creditcard&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
		</td>
	{/section}
	<td>
		Action
	</td>
	{foreach from=$cc_info item=cc}
		<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">{$cc.id}</td>
		<td width="140">{$cc.cc_no}</td>
		<td width="60">{$cc.type_name}</td>
		<td width="140">{$cc.date_joined}</td>
		<td width="140">{$cc.date_expiry}</td>
		<td width="100">{$cc.status}</td>
		<td align="center">{$cc.pay}</td>
		</tr>
	{/foreach}
	<tr>
		<td colspan="8" class="blacktb">{$pg_link}</td>
	</tr>
</table>
{else}
<font class="text">No Credit Card or Account is available for this service.</font>
{/if}
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}