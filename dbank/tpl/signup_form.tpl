{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
	<h3>Sign Up Form</h3>
	{foreach from=$error item=error_item}
		<font class="error">{$error_item}</font><br>
	{/foreach}
	<form method="post" action="?">
	<table width="400" height="300" border="0" class="blacktb">
	  <tr>
		<td>Username</td>
		<td>
		  <input type="text" name="username" class="textboxname" maxlength="16">
		</td>
	  </tr>
	  <tr>
		<td>Password</td>
		<td>
		  <input type="password" name="password" class="textboxpassword" maxlength="64">
		</td>
	  </tr>
	  <tr>
		<td>Confirm password </td>
		<td><input type="password" name="password2" class="textboxpassword" maxlength="64"></td>
	  </tr>
	  <tr>
		<td>Fullname</td>
		<td><input type="text" name="fullname" class="textboxname" maxlength="64"></td>
	  </tr>
	  <tr>
		<td>IC / Passport No </td>
		<td><input type="text" name="ic" class="textboxname" maxlength="32"></td>
	  </tr>
	  <tr>
		<td>Date Of Birth </td>
		<td>
			<select name="dob_year">
				{foreach from=$year_list item=itemyear}
					<option value="{$itemyear}">{$itemyear}</option>
				{/foreach}
			</select>
			<select name="dob_month">
				{foreach from=$month_list key=key item=item}
					<option value="{$key}">{$item}</option>
				{/foreach}
			</select>
			<select name="dob_day">
				{foreach from=$day_list item=itemday}
					{if $itemday lt 10}
						<option value="0{$itemday}">{$itemday}</option>
					{else}
						<option value="{$itemday}">{$itemday}</option>
					{/if}
				{/foreach}
			</select>
		</td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
	  </tr>
	  <tr>
		<td colspan="2" align="center">
			<input type="hidden" name="opt" value="register">
			<input type="hidden" name="act" value="process">
			<input type="submit" value="Sign Up" class="button1">
			<input type="reset" value="Clear" class="button1">
		</td>
	  </tr>
	</table>
	</form>
</td>
{include file="footer.tpl"}