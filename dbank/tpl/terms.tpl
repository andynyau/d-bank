{config_load file=test.conf section="setup"}
{include file="header.tpl"}
{if $validate}
	{include file="sidebar.tpl"}
{else}
	{include file="login.tpl"}
{/if}
<td valign="top">
	<h3>Terms &amp; Conditions</h3>
	<table border="0" width="750">
	<tr class="whitetb">
		<td>
Please take a moment to read these terms and conditions carefully. 
<br><br>
THE FOLLOWING TERMS AND CONDITIONS APPLY TO YOUR ACCESS AND THE USE OF THIS WEBSITE AND THE SERVICES PROVIDED HEREIN BY D-BANK BANKING BERHAD AND ITS SUBSIDIARIES. BY ACCESSING ANY PAGE OF THIS WEBSITE AND/OR USING THE SERVICES, YOU AGREE TO BE BOUND BY THESE TERMS AND CONDITIONS WITHOUT LIMITATION OR QUALIFICATION.
<br><br>
IF YOU DO NOT ACCEPT THESE TERMS AND CONDITIONS, PLEASE IMMEDIATELY DISCONTINUE YOUR ACCESS TO THIS WEBSITE AND/OR USE OF THE SERVICES.
<br><br>
<b>General</b>
<br><br>
The term "D-Bank"  as used in these terms and conditions refers to D-Bank Banking Berhad. The term "the D-Bank Group" refers to D-Bank Banking Berhad and its subsidiaries, either individually and/or collectively as the context requires.
<br><br>
All products and services of the D-Bank Group and its partners herein provided are subject to the terms and conditions of the applicable agreements governing their use. These terms and conditions are meant to regulate your access to this website and they are to be read together with the applicable terms and conditions governing any transaction(s), product(s) and/or service(s) provided in this website. In the event of conflict between these terms and conditions and the terms and conditions governing the relevant transaction(s), product(s) and/or service(s) provided herein, the latter will prevail. 
<br><br>
The information, material, functions and content provided in the pages of the website may be changed from time to time with or without notice at D-Bank Group's absolute discretion. Your continued access or use of the website and/or the services provided herein subsequent to any such change will be deemed as your acceptance to those changes.
<br><br><br> 
 
<b>Disclaimer</b>
<br><br>
The materials and information in this website, including but not limited to services, products, information, data, text, graphics, audio, video, links or other items, are provided by the D-Bank Group on an "as is" and "as available" basis. References to material and information contained in the website include such material and information provided by third parties.
<br><br>
The D-Bank Group does not make any express or implied warranties, representations or endorsements including but not limited to any warranties of title, non-infringement, merchantability, usefulness, operation, completeness, currentness, accuracy, satisfactory quality, reliability, fitness for a particular purpose in respect of the website, the material, information and/or functions therein and expressly disclaims liability for errors and omissions in such materials, information and/or functions. Without derogation of the above and/or the terms and conditions of the applicable agreements governing all the products and services of the D-Bank Group, reasonable measures will be taken by the D-Bank Group to ensure the accuracy and validity of all information relating to transactions and products of D-Bank Group which originate exclusively from D-Bank Group.
<br><br>
Further the D-Bank Group does not warrant or represent that access to the whole or part(s) of this website, the materials, information and/or functions contained therein will be provided uninterrupted or free from errors or that any identified defect will be corrected, or that there will be no delays, failures, errors or loss of transmitted information, that no viruses or other contaminating or destructive properties will be transmitted or that no damage will occur to your computer system.
<br><br>
The materials, information and functions provided in this website shall not under any circumstances be considered or construed as an offer or solicitation to sell, buy, give, take, issue, allot or transfer, or as the giving of any advice in respect of shares, stocks, bonds, notes, interests, unit trusts, mutual funds or other securities, investments, loans, advances, credits or deposits in any jurisdiction. 
You shall be responsible to evaluate the quality, adequacy, completeness, currentness and usefulness of all services, content, advice, opinions and other information obtained or accessible through the website; further you should seek professional advice at all times and obtain independent verification of the materials and information provided herein prior to making any investment, business or commercial decision based on any such materials or information.
<br><br><br> 
 
<b>Links</b>
<br><br>
Links from or to websites outside this website are meant for convenience only. Such linked websites are owned and operated by third parties and as such are not under the control of the D-Bank Group. Therefore the D-Bank Group shall not be responsible and makes no warranties in respect of the contents of those websites, the third parties named therein or their products and services. Furthermore, the links provided in this website shall not be considered an endorsement or verification or approval of such linked websites or the contents therein. Linking to any other site is at your sole risk and the D-Bank Group will not be responsible or liable for any damages in connection with linking. It is advisable for you to read the privacy policy statements (if any) of any websites which are linked to this website.
<br><br><br>

<b>Copyright</b>
<br><br>
Unless otherwise indicated, the copyright in this website and its contents, including but not limited to the text, images, graphics, sound files, animation files, video files, and their arrangement, are the property of the D-Bank Group, and are protected by applicable Malaysian and international copyright laws. No part or parts of this website may be modified, copied, distributed, retransmitted, broadcast, displayed, performed, reproduced, published, licensed, transferred, sold or commercially dealt with in any manner without the express prior written consent of the D-Bank Group.
<br><br>
You also may not, without the D-Bank Group's expressed prior written consent, insert a link to this website on any other website, frame or "mirror" any material contained on this website on any other server.
Any such unauthorised reproduction, retransmission or other copying or modification of any of the contents of the D-Bank Group's website may be in breach of statutory or common law rights which could be the subject of legal action.
<br><br>
The D-Bank Group disclaims all liability which may arise from any unauthorised reproduction or use of the contents of this D-Bank Group's website.
<br><br><br> 
 
<b>Trademarks</b>
<br><br>
All trademarks, service marks, and logos displayed in this website are the property of the D-Bank Group and/or their respective third party proprietors as identified in the website.
<br><br>
Unless the prior written consent of the D-Bank Group or the relevant third party proprietor of any of the trademarks, service marks or logos appearing on the website has been obtained, no license or right is granted to any party accessing this website to use, download, reproduce, copy or modify such trademarks, services marks or logos. Similarly, unless the prior written consent of the D-Bank Group or the relevant proprietor has been obtained, no such trademark, service mark or logo may be used as a link or to mark any link to the D-Bank Group's website or any other site.
<br><br><br>


<b>Exclusion of Liability</b>
<br><br>
The D-Bank Group and/or its partners herein shall in no event be liable for any loss or damages howsoever arising whether in contract, tort, negligence, strict liability or any other basis, including without limitation, direct or indirect, special, incidental, consequential or punitive damages, or loss profits or savings arising in connection with your access or use or the inability to access or use this website (or any third party link to or from the D-Bank Group's website), reliance on the information contained in the website, any technical, hardware or software failure of any kind, the interruption, error, omission, delay in operation, computer viruses, or otherwise.
This exclusion clause shall take effect to the fullest extent permitted by law. 
<br><br><br>

<b>Indemnity</b>
<br><br>
You hereby irrevocably agree to indemnify and keep indemnified the D-Bank Group from all liabilities, claims, losses and expenses, including any legal fees that may be incurred by the D-Bank Group in connection with or arising from (1) your use or misuse of this website and the services provided herein, or (2) your breach of these terms and conditions howsoever occasioned, or (3) any intellectual property right or proprietary right infringement claim made by a third party against the D-Bank Group in connection with your use of this website.
<br><br><br> 

<b>Termination</b>
<br><br>
The D-Bank Group reserves the right to terminate and/or suspend your access to this website and/or your use of this website at any time, for any reason. In particular, and without limitation, the D-Bank Group may terminate and/or suspend your access should you violate any of these terms and conditions, or violate the rights of the D-Bank Group, of any other user, or of any third party.
<br><br><br>

<b>Miscellaneous</b>
<br><br>
The failure of the D-Bank Group to exercise or enforce any right or provision of these terms and conditions shall not constitute a waiver of such right or provision.
<br><br>
If any part of these terms and conditions is determined to be invalid or unenforceable pursuant to applicable law, then the invalid and unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the other provisions of the terms and conditions shall continue in full force and effect.
<br><br>
Any rights not expressly granted herein are reserved.
<br><br><br>

<b>Law and Jurisdiction</b>
<br><br>
These terms and conditions are governed by and are to be construed in accordance with the laws of Malaysia. By accessing this website and/or using the services provided herein by the D-Bank Group, you hereby consent to the exclusive jurisdiction of the Malaysian courts in Kuala Lumpur, Malaysia in all disputes arising out of or relating to the use of this website.
<br><br>
The D-Bank Group makes no representation that the materials, information, functions and/or services provided on this website are appropriate or available for use in jurisdictions other than Malaysia. 
 
			
		</td>
	</tr>
	</table>
</td>
      </tr>
    </table></td>
{include file="footer.tpl"}