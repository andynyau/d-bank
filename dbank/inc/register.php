<?php

function display_terms_condition()
{
	global $s,$smarty,$db,$tb;
	$smarty->display('signup_terms.tpl');
}

function display_signup_form()
{
	global $errormsg,$s,$smarty,$db,$tb;
	$year = getyearlist('1920','1990');
	$month = getmonthlist();
	$day = getdaylist();
	$smarty->assign('year_list',$year);
	$smarty->assign('month_list',$month);
	$smarty->assign('day_list',$day);
	$smarty->assign('error',$errormsg);
	$smarty->display('signup_form.tpl');
}

function process_signup()
{
	global $s, $smarty, $db, $tb, $errormsg;

	$username = $_POST['username'];
	usernameCheck($username);

	$password = $_POST['password'];
	$password2 = $_POST['password2'];
	passwdCheck($password,$password2);

	$fullname = $_POST['fullname'];
	check_field($fullname,"Fullname",1,"text",8);

	$ic = $_POST['ic'];
	check_field($_POST['ic'],"IC / Passport",1,"text",4);
	icCheck($ic);
	
	$year = $_POST['dob_year'];
	$month = $_POST['dob_month'];
	$day = $_POST['dob_day'];
	$dob = dateCheck($year, $month, $day, "Date of Birth");

	if ($errormsg)
	{
		display_signup_form();	
	}
	else
	{
		$q=new sql($db);
		$sql = "INSERT INTO {$tb['customer']} VALUES ('', '$username', '$fullname', '$ic', '$dob', NOW(), NOW())";
		$q->query($sql);
		$sql = "SELECT LAST_INSERT_ID() AS last_id FROM {$tb['customer']}";
		$q->query($sql);
		if ($q->numrows())
		{
			while ($rows=$q->getrows())
			{
				$lastid = $rows['last_id'];
			}
		}
		$e_password = md5($password);
		$sql = "INSERT INTO {$tb['login']} VALUES ('', '$lastid', '$e_password','')";
		$q->query($sql);
		if (!mysql_error())
		{
			$smarty->display('signup_success.tpl');
		}
	}
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'agree':
			display_signup_form();
			break;

			default:
			display_terms_condition();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'process':
			process_signup();
			break;

			default:
			display_terms_condition();
		}
	}
	else
	{
		display_terms_condition();
	}
}
?>