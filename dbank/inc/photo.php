<?php
function check_photo_exist($id)
{
	global $s,$db,$tb;
	$q=new sql($db);
	$sql = "SELECT * FROM {$tb['customer_image']} WHERE cid = '$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		return true;
	}
	else
	{
		return false;
	}
}

function display_photo()
{
	global $smarty,$s,$db,$tb,$errormsg;
	$id = get_userid();
	if (check_photo_exist($id))
	{
		$status = "Status : ".strtoupper(get_photo_status($id));
		$empty = 'no';
	}
	else
	{
		$empty = 'yes';
	}
	$img_url = "<img src=\"?opt=view_image&id=$id\">";
	$smarty->assign('cid',$id);
	$smarty->assign('status',$status);
	$smarty->assign('empty',$empty);
	$smarty->assign('error',$errormsg);
	$smarty->assign('img_url',$img_url);
	$smarty->display('photo.tpl');
}

function process()
{
	global $s,$db,$tb,$errormsg;
	$uploaddir = 'C:/tmp/file/';
	$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

	if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile))
	{
		$cid = $_POST['cid'];
		$filepath = $uploaddir.$_FILES['userfile']['name'];

		$fp = fopen($filepath, "rb");
		$content = mysql_escape_string(fread($fp, filesize($filepath)));

		if ($content)
		{
			$q=new sql($db);
			if (check_photo_exist($cid))
			{
				$sql = "UPDATE {$tb['customer_image']} SET image='$content', status='pending', date_uploaded = NOW()";
			}
			else
			{
				$sql = "INSERT INTO {$tb['customer_image']} VALUES ('','$cid','$content','pending',NOW())";
			}
			$q->query($sql);
		}
		fclose($fp);
		unlink($filepath);
	}
	else
	{
		errormsg("Image Upload Failed.");
	}
	display_photo();
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if ($_FILES)
	{
		process();
	}
	else
	{
		display_photo();
	}
}
?> 