<?php
function resend_verify()
{
	global $s, $smarty, $db, $tb, $errormsg;

	$id = $_GET['id'];
	$email_info = get_email_info($id);
	$verify_str=md5($email_info['email']);
	$q=new sql($db);
	$sql = "UPDATE {$tb['email']} SET verify_str = '$verify_str', date_last_modified = NOW() WHERE id = '$id'";
	$q->query($sql);
	sendmail_new($email_info['email'],$verify_str);
	display_email_list();
}

function process($type)
{
	global $s, $smarty, $db, $tb, $errormsg;

	$email = strip_tags($_POST['email']);
	$email2 = strip_tags($_POST['email2']);
	emailCheck($email,$email2);

	$cid = $_POST['cid'];

	if ($type=='edit')
	{
		$id = $_POST['id'];
	}

	if ($errormsg)
	{
		display_form($type,$id);
	}
	else
	{
		if ($type == 'add')
		{
			$verify_str=md5($email);
			$q=new sql($db);
			$sql = "INSERT INTO {$tb['email']} VALUES ('', '$cid', '$email', 'NO', 'unverify', '$verify_str', NOW(), NOW())";
			if (get_final_id($tb['email']) == $_POST['final'])
			{
				$q->query($sql);
				sendmail_new($email,$verify_str);
			}
			$smarty->assign('type',$type);
			$smarty->display('email_success.tpl');
		}
		else
		{
			$verify_str=md5($email);
			$q=new sql($db);
			$sql = "UPDATE {$tb['email']} SET email = '$email', status = 'unverify', verify_str = '$verify_str', date_last_modified = NOW() WHERE id = '$id'";
			$q->query($sql);
			sendmail_new($email,$verify_str);
			$smarty->assign('type',$type);
			$smarty->display('email_success.tpl');
		}
	}
}

function check_mail_primary($id)
{
	global $s,$db,$tb,$errormsg;

	$q=new sql($db);
	$sql = "SELECT `primary` FROM {$tb['email']} WHERE id = '$id' AND `primary` = 'NO'";
	$q->query($sql);
	if ($q->numrows())
	{
		return false;
	}
	else
	{
		return true;
	}
}

function display_form($type,$id='')
{
	global $smarty,$s,$db,$tb,$errormsg;
	$cid = get_userid();
	if ($type=='edit')
	{
		$email_info = get_email_info($id);
		$smarty->assign('id',$id);
		$smarty->assign('email_info',$email_info);
	}
	else
	{
		$final = get_final_id($tb['email']);
	}
	$smarty->assign('final',$final);
	$smarty->assign('error',$errormsg);
	$smarty->assign('type',$type);
	$smarty->assign('cid',$cid);
	$smarty->display('email_form.tpl');
}

function delete_email()
{
	global $s,$db,$tb,$errormsg;

	$id = $_GET['id'];
	$q=new sql($db);
	$sql = "DELETE FROM {$tb['email']} WHERE id = '$id'";
	$q->query($sql);
	display_email_list();
}

function update_primary()
{
	global $s,$db,$tb,$errormsg;

	$id = $_GET['id'];
	$cid = $_GET['cid'];
	if (!empty($id))
	{
		$q=new sql($db);
		$sql = "UPDATE {$tb['email']} SET `primary` = 'NO' WHERE cid = '$cid'";
		$q->query($sql);
		$sql = "UPDATE {$tb['email']} SET `primary` = 'YES' WHERE id = '$id'";
		$q->query($sql);
	}
	else
	{
		errormsg("No Email Is Selected");
	}
	display_email_list();
}

function display_email_list()
{
	global $smarty,$s,$db,$tb,$errormsg;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No','Email Address', 'Status');
	$field_values = array('id', 'email', 'status');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}
	$id = get_userid();
	$q=new sql($db);
	$sql="SELECT {$tb['email']}.id, {$tb['email']}.email, {$tb['email']}.status, {$tb['email']}.primary FROM {$tb['customer']} INNER JOIN {$tb['email']} ON {$tb['customer']}.id = {$tb['email']}.cid WHERE {$tb['customer']}.id = '$id' ORDER BY {$tb['email']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT {$tb['email']}.id, {$tb['email']}.email, {$tb['email']}.status, {$tb['email']}.primary FROM {$tb['customer']} INNER JOIN {$tb['email']} ON {$tb['customer']}.id = {$tb['email']}.cid WHERE {$tb['customer']}.id = '$id' ORDER BY {$tb['email']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$email_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$email_info[$i]['tid'] = $rows['id'];
			$email_info[$i]['email'] = $rows['email'];
			$email_info[$i]['status'] = $rows['status'];
			$email_info[$i]['primary'] = $rows['primary'];
			$email_info[$i]['edit'] = "<a href=\"?opt=email&act=edit&id={$rows['id']}\"><img src=\"{$s['img_path']}button_edit.png\" border=\"0\" alt=\"Edit\"></a>";
			$email_info[$i]['delete'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to delete this ?' ,'?opt=email&act=delete&id={$rows['id']}&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Delete\"></a>";
			$email_info[$i]['resend'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to resend verify link to this email?' ,'?opt=email&act=resend&id={$rows['id']}&cid=$id&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_verify.png\" border=\"0\" alt=\"Resend Verify\"></a>";
			$email_info[$i]['update_primary'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to update this as primary?' ,'?opt=email&act=primary&id={$rows['id']}&cid=$id&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_primary.png\" border=\"0\" alt=\"Update Primary\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"email",'',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("empty","no");
		$smarty->assign("errormsg",$errormsg);
		$smarty->assign("cid",$id);
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign("field_names", $field_names);
		$smarty->assign("field_values", $field_values);
		$smarty->assign('email_info',$email_info);
		$smarty->display('email.tpl');
	}
	else
	{
		$smarty->assign("empty","yes");
		$smarty->display('email.tpl');
	}
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'edit':
			display_form('edit',$_GET['id']);
			break;

			case 'delete':
			delete_email();
			break;

			case 'resend':
			resend_verify();
			break;
			
			case 'primary':
			update_primary();
			break;

			default:
			display_email_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{

			case 'add_new':
			display_form('add');
			break;

			case 'process':
			process($_POST['type']);
			break;

			default:
			display_email_list();
		}
	}
	else
	{
		display_email_list();
	}
}
?>