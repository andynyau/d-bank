<?php
function proceed()
{
	global $smarty,$s,$db,$tb;
	
	$from = $_POST['aid'];
	$to = $_POST['to'];
	$amount = $_POST['amount'];
	$remark = $_POST['remark'];
	$final = $_POST['final'];
	$limit = $_POST['limit'];
	
	if ($limit == 1)
	{
		//do transaction
		if ($final == $final = get_final_id($tb['transaction_out']))
		{
			do_transaction($from,$to,$amount,$remark);
			$smarty->assign($limit,'1');
		}
	}
	else
	{
		//pending transaction
		if ($final == get_final_id($tb['transaction_pending']))
		{
			pending_transaction($from,$to,$amount,$remark);
			$smarty->assign($limit,'0');
		}
	}
	$smarty->display('transfer_success.tpl');
}

function process()
{
	global $smarty,$s,$db,$tb,$errormsg;

	$amount = $_POST['amount'];
	$aid = $_POST['account_id'];
	$to = $_POST['to_account'];
	$remark = strip_tags($_POST['remark']);

	currencyCheck($amount,"Amount");
	if (!$errormsg)
	{
		check_balance_enough($aid,$amount);
	}
	if (!$errormsg)
	{
		check_account_exist($to);
	}
	if (!$errormsg)
	{
		check_daily_limit($aid,$amount);
	}
	if (!$errormsg)
	{
		$limit = check_per_limit($amount);
		if ($limit)
		{
			$final = get_final_id($tb['transaction_out']);
		}
		else
		{
			$final = get_final_id($tb['transaction_pending']);
		}
		$smarty->assign('final',$final);
		$smarty->assign('limit',$limit);
		$smarty->assign('from',$aid);
		$smarty->assign('to',$to);
		$smarty->assign('amount',sprintf("%.2f",$amount));
		$smarty->assign('remark',$remark);
		$smarty->display('transfer_confirm.tpl');
	}
	else
	{
		display_transfer_form();
	}
}

function display_transfer_form()
{
	global $s,$db,$tb,$smarty,$errormsg;

	$id = get_userid();
	$account_list = get_account_list($id);
	if ($account_list)
	{
		$smarty->assign("empty","no");
		$smarty->assign('errormsg',$errormsg);
		$smarty->assign('account_list',$account_list);
	}
	else
	{
		$smarty->assign("empty","yes");
	}
	$smarty->display('transfer.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			default:
			display_transfer_form();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'confirm':
			proceed();
			break;

			case 'process':
			process();
			break;

			default:
			display_transfer_form();
		}
	}
	else
	{
		display_transfer_form();
	}
}
?>