<?php
function process()
{
	global $s,$db,$tb,$smarty,$errormsg;

	$id = get_userid();

	$oldpassword = md5($_POST['oldpassword']);
	$password1 = $_POST['password1'];
	$password2 = $_POST['password2'];
	$epassword = md5($password1);

	$q = new sql($db);
	$sql = "SELECT id FROM {$tb['login']} WHERE cid = '$id' AND password = '$oldpassword'";
	$q->query($sql);
	if ($q->numrows())
	{
		passwdCheck($password1,$password2);
		if ($errormsg)
		{
			display_changePass_form();
		}
		else
		{
			$sql = "UPDATE login SET password = '$epassword' WHERE cid = '$id'";
			$q->query($sql);
			$smarty->display('change_pass_success.tpl');
		}
	}
	else
	{
		errormsg("<b>Old password<b> not correct");
		display_changepass_form();
	}
}

function display_changepass_form()
{
	global $s,$db,$tb,$smarty,$errormsg;
	$smarty->assign('errormsg',$errormsg);
	$smarty->display('change_pass.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			default:
			display_changepass_form();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'process':
			process();
			break;

			default:
			display_changepass_form();
		}
	}
	else
	{
		display_changepass_form();
	}
}
?>