<?php
if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	global $smarty;

	if (!validate_login())
	{
		$smarty->assign('error_login','Username or Password Error');
		$smarty->display('index.tpl');
	}
	else
	{
		$smarty->display('index.tpl');
	}
}
?>