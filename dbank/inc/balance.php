<?php
function view_balance($id)
{
	global $s,$db,$tb,$smarty;
	$account_no = get_account_no($id);
	$q=new sql($db);
	$sql = "SELECT `balance` FROM {$tb['balance']} WHERE aid = '$id'";
	$q->query($sql);
	if ($q->numrows())
	{
		while ($rows=$q->getrows())
		{
			$balance = $rows['balance'];
		}
	}
	$balance = sprintf("%0.2f", $balance);
	$smarty->assign('account_no',$account_no);
	$smarty->assign('balance',$balance);
	$smarty->display('balance_view.tpl');
}

function display_account_list()
{
	global $s,$db,$tb,$smarty;
	$id = get_userid();
	$account_list = get_account_list($id);
	if ($account_list)
	{
		$smarty->assign("empty","no");
		$smarty->assign('account_list',$account_list);
	}
	else
	{
		$smarty->assign("empty","yes");
	}
	$smarty->display('balance_list.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			default:
			display_account_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'view':
			view_balance($_POST['account_id']);
			break;

			default:
			display_account_list();
		}
	}
	else
	{
		display_account_list();
	}
}
?>