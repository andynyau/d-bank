<?php
function process($type)
{
	global $s, $smarty, $db, $tb, $errormsg;

	$street = strip_tags($_POST['street']);
	check_field($street,"Street",1,"any",5);

	$city = strip_tags($_POST['city']);
	check_field($city,"City",1,"any",2);

	$postcode = strip_tags($_POST['postcode']);
	check_field($postcode,"Postcode",1,"text",2);

	$state = strip_tags($_POST['state']);
	check_field($state,"State",0,"text",2);

	$cid = $_POST['cid'];
	$countries = $_POST['country'];

	if ($type=='edit')
	{
		$id = $_POST['id'];
	}

	if ($errormsg)
	{
		display_form($type,$id);
	}
	else
	{
		if ($type == 'add')
		{
			$q=new sql($db);
			$sql = "INSERT INTO {$tb['address']} VALUES ('', '$cid', '$street', '$city', '$postcode', '$state', '$countries', 'NO', NOW(), NOW())";
			if (get_final_id($tb['address']) == $_POST['final'])
			{
				$q->query($sql);
			}
			$smarty->assign('type',$type);
			$smarty->display('address_success.tpl');
		}
		else
		{
			$q=new sql($db);
			$sql = "UPDATE {$tb['address']} SET street = '$street', city = '$city', postcode = '$postcode', state = '$state', countries_id = '$countries', date_last_modified = NOW() WHERE id = '$id'";
			$q->query($sql);
			$smarty->assign('type',$type);
			$smarty->display('address_success.tpl');
		}
	}
}

function display_form($type,$id='')
{
	global $smarty,$s,$db,$tb,$errormsg;
	$cid = get_userid();
	$countrylist = get_country_list();
	if ($type=='edit')
	{
		$address_info = get_address_info($id);
		$smarty->assign('id',$id);
		$smarty->assign('address_info',$address_info);
	}
	else
	{
		$final = get_final_id($tb['address']);
		$smarty->assign('final',$final);
	}
	$smarty->assign('error',$errormsg);
	$smarty->assign('type',$type);
	$smarty->assign('cid',$cid);
	$smarty->assign('countrylist',$countrylist);
	$smarty->display('address_form.tpl');
}

function delete_address()
{
	global $s,$db,$tb,$errormsg;

	$id = $_GET['id'];
	$q=new sql($db);
	$sql = "DELETE FROM {$tb['address']} WHERE id = '$id'";
	$q->query($sql);
	display_address_list();
}

function update_primary()
{
	global $s,$db,$tb,$errormsg;

	$id = $_GET['id'];
	$cid = $_GET['cid'];
	if (!empty($id))
	{
		$q=new sql($db);
		$sql = "UPDATE {$tb['address']} SET `primary` = 'NO' WHERE cid = '$cid'";
		$q->query($sql);
		$sql = "UPDATE {$tb['address']} SET `primary` = 'YES' WHERE id = '$id'";
		$q->query($sql);
	}
	else
	{
		errormsg("No Address Is Selected");
	}
	display_address_list();
}

function display_address_list()
{
	global $smarty,$s,$db,$tb,$errormsg;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Street', 'City', 'Postcode', 'State', 'Country');
	$field_values = array('id', 'street', 'city', 'postcode', 'state', 'country');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}
	if ($sort_by=='country')
	{
		$sort_by=$tb['countries'].".countries_name";
	}
	$id = get_userid();
	$q=new sql($db);
	$sql="SELECT {$tb['address']}.id, {$tb['address']}.street, {$tb['address']}.city, {$tb['address']}.postcode, {$tb['address']}.state, {$tb['countries']}.countries_name, {$tb['address']}.primary FROM {$tb['customer']} INNER JOIN {$tb['address']} ON {$tb['customer']}.id = {$tb['address']}.cid INNER JOIN {$tb['countries']} ON {$tb['address']}.countries_id = {$tb['countries']}.id WHERE {$tb['customer']}.id = '$id' ORDER BY {$tb['address']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT {$tb['address']}.id, {$tb['address']}.street, {$tb['address']}.city, {$tb['address']}.postcode, {$tb['address']}.state, {$tb['countries']}.countries_name, {$tb['address']}.primary FROM {$tb['customer']} INNER JOIN {$tb['address']} ON {$tb['customer']}.id = {$tb['address']}.cid INNER JOIN {$tb['countries']} ON {$tb['address']}.countries_id = {$tb['countries']}.id WHERE {$tb['customer']}.id = '$id' ORDER BY {$tb['address']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$address_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$address_info[$i]['tid'] = $rows['id'];
			$address_info[$i]['street'] = $rows['street'];
			$address_info[$i]['city'] = $rows['city'];
			$address_info[$i]['postcode'] = $rows['postcode'];
			$address_info[$i]['state'] = $rows['state'];
			$address_info[$i]['countries'] = $rows['countries_name'];
			$address_info[$i]['primary'] = $rows['primary'];
			$address_info[$i]['edit'] = "<a href=\"?opt=address&act=edit&id={$rows['id']}\"><img src=\"{$s['img_path']}button_edit.png\" border=\"0\" alt=\"Edit\"></a>";
			$address_info[$i]['delete'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to delete this ?' ,'?opt=address&act=delete&id={$rows['id']}&cid=$id&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Delete\"></a>";
			$address_info[$i]['update_primary'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to update this as primary?' ,'?opt=address&act=primary&id={$rows['id']}&cid=$id&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_primary.png\" border=\"0\" alt=\"Update Primary\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"address",'',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("empty","no");
		$smarty->assign("errormsg",$errormsg);
		$smarty->assign("cid",$id);
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign("field_names", $field_names);
		$smarty->assign("field_values", $field_values);
		$smarty->assign('address_info',$address_info);
		$smarty->display('address.tpl');
	}
	else
	{
		$smarty->assign("empty","yes");
		$smarty->display('address.tpl');
	}
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'edit':
			display_form('edit',$_GET['id']);
			break;

			case 'delete':
			delete_address();
			break;
			
			case 'primary':
			update_primary();
			break;

			default:
			display_address_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{

			case 'add_new':
			display_form('add');
			break;

			case 'process':
			process($_POST['type']);
			break;

			default:
			display_address_list();
		}
	}
	else
	{
		display_address_list();
	}
}
?>