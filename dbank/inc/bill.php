<?php
function process()
{
	global $s,$db,$tb,$smarty,$errormsg;
	
	$pid = $_POST['participant'];
	$aid = $_POST['account_id'];
	$amount = $_POST['amount'];
	$remark = strip_tags($_POST['remark']);

	currencyCheck($amount,"Amount");
	if (!$errormsg)
	{
		check_balance_enough($aid,$amount);
	}
	if (!$errormsg)
	{
		check_field($remark,"Bill No",'1','any','4');
	}
	if (!$errormsg)
	{
		$id = get_userid();
		if (get_final_id($tb['bill']) == $_POST['final'])
		{
			do_bill_payment($id,$pid,$aid,$amount,$remark);
		}
		$participant = get_participant_name($pid);
		$account_no = get_account_no($aid);
		$smarty->assign('participant',$participant);
		$smarty->assign('account_no',$account_no);
		$smarty->assign('amount',$amount);
		$smarty->assign('billno',$remark);
		$smarty->display('bill_success.tpl');
	}
	else
	{
		display_bill_form();
	}
}

function display_bill_form()
{
	global $s,$db,$tb,$smarty,$errormsg;

	$id = get_userid();
	$account_list = get_account_list($id);
	if ($account_list)
	{
		$final = get_final_id($tb['bill']);
		$participant_list = get_participant_list();
		$smarty->assign("empty","no");
		$smarty->assign('errormsg',$errormsg);
		$smarty->assign("final",$final);
		$smarty->assign('account_list',$account_list);
		$smarty->assign('participant_list',$participant_list);
	}
	else
	{
		$smarty->assign("empty","yes");
	}
	$smarty->display('bill.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			default:
			display_bill_form();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{

			case 'process':
			process();
			break;

			default:
			display_bill_form();
		}
	}
	else
	{
		display_bill_form();
	}
}
?>