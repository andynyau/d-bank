<?php
function process()
{
	global $smarty,$s,$db,$tb,$errormsg;

	$aid = $_POST['account_id'];
	$cc_id = $_POST['cc_id'];
	$amount = $_POST['amount'];

	currencyCheck($amount,"Balance");
	
	if (!$errormsg)
	{
		check_balance_enough($aid,$amount);
	}

	if ($errormsg)
	{
		pay_cc();
	}
	else
	{
		if (get_final_id($tb['credit_card_payment']) == $_POST['final'])
		{
			do_cc_payment($cc_id,$aid,$amount);
		}
		$cc_no = get_cc_no($cc_id);
		$a_no = get_account_no($aid);
		$smarty->assign('cc_no',$cc_no);
		$smarty->assign('a_no',$a_no);
		$smarty->assign('amount',$amount);
		$smarty->display('creditcard_success.tpl');
	}
}

function pay_cc()
{
	global $smarty,$s,$db,$tb,$errormsg;

	$id = get_userid();
	$account_list = get_account_list($id);
	$final = get_final_id($tb['credit_card_payment']);
	$cc_id = $_GET['id'];

	$smarty->assign('cc_id',$cc_id);
	$smarty->assign('final',$final);
	$smarty->assign('errormsg',$errormsg);
	$smarty->assign('account_list',$account_list);
	$smarty->display('creditcard_pay.tpl');
}

function display_cc_list()
{
	global $smarty,$s,$db,$tb;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Credit Card No', 'Type', 'Date Since', 'Date Expiry', 'Status');
	$field_values = array('id', 'cc_no', 'type_name', 'date_joined', 'date_expiry', 'status');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;	
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	if ($sort_by == 'type_name')
	{
		$sort_by = "{$tb['credit_card_type']}.type_name";
	}

	$id = get_userid();
	$account_list = get_account_list($id);
	if ($account_list)
	{
		$q=new sql($db);
		$sql="SELECT {$tb['credit_card']}.id, {$tb['credit_card']}.cc_no, {$tb['credit_card']}.date_joined, {$tb['credit_card']}.date_expiry, {$tb['credit_card']}.status, {$tb['credit_card_type']}.type_name FROM {$tb['credit_card']} INNER JOIN {$tb['credit_card_type']} ON {$tb['credit_card']}.tid = {$tb['credit_card_type']}.id WHERE {$tb['credit_card']}.cid = '$id' ORDER BY {$tb['credit_card']}.$sort_by $sort_order LIMIT $start,10";
		$q->query($sql);
		$sql="SELECT {$tb['credit_card']}.id, {$tb['credit_card']}.cc_no, {$tb['credit_card']}.date_joined, {$tb['credit_card']}.date_expiry, {$tb['credit_card']}.status, {$tb['credit_card_type']}.type_name FROM {$tb['credit_card']} INNER JOIN {$tb['credit_card_type']} ON {$tb['credit_card']}.tid = {$tb['credit_card_type']}.id WHERE {$tb['credit_card']}.cid = '$id' ORDER BY {$tb['credit_card']}.id";
		if ($q->numrows())
		{
			$i=0;
			while ($rows=$q->getrows())
			{
				$cc_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
				$cc_info[$i]['tid'] = $rows['id'];
				$cc_info[$i]['cc_no'] = $rows['cc_no'];
				$cc_info[$i]['date_joined'] = $rows['date_joined'];
				$cc_info[$i]['date_expiry'] = $rows['date_expiry'];
				$cc_info[$i]['status'] = $rows['status'];
				$cc_info[$i]['type_name'] = $rows['type_name'];
				$cc_info[$i]['pay'] = "<a href=\"?opt=creditcard&act=pay&id={$rows['id']}\"><img src=\"{$s['img_path']}button_pay.png\" border=\"0\" alt=\"Pay\"></a>";
				$i++;
			}
			$pg_link=pagination("10","10",$start,"",$sql,"creditcard",'',"&sort_by=$sort_by&sort_order=$sort_order");
			$smarty->assign("empty","no");
			$smarty->assign("pg_link",$pg_link);
			$smarty->assign("sort_by", $tsort_by);
			$smarty->assign("sort_order", $sort_order);
			$smarty->assign("start", $start);
			$smarty->assign("field_names", $field_names);
			$smarty->assign("field_values", $field_values);
			$smarty->assign('cc_info',$cc_info);
		}		
		else
		{
			$smarty->assign("empty","yes");
		}
	}
	else
	{
		$smarty->assign("empty","yes");
	}
	$smarty->display('creditcard.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'pay':
			pay_cc();
			break;

			default:
			display_cc_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'process':
			process();
			break;

			default:
			display_cc_list();
		}
	}
	else
	{
		display_cc_list();
	}
}
?>