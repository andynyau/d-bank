<?php
function display_history_list($from,$to)
{
	global $smarty,$s,$db,$tb;

	if (empty($_GET))
	{
		$_GET = $_POST;
	}

	$aid = $_GET['account_id'];
	$from_year = $_GET['from_year'];
	$from_month = $_GET['from_month'];
	$from_day = $_GET['from_day'];
	$to_year = $_GET['to_year'];
	$to_month = $_GET['to_month'];
	$to_day = $_GET['to_day'];
	$type = $_GET['type'];

	if ($type == 'in')
	{
		$field_names = array('No', 'Date Time', 'From', 'Amount','Remark');
		$field_values = array('id', '`datetime`', '`from`', 'amount','remark');
		$table = $tb['transaction_in'];
		$col = 'from';
	}
	else
	{
		$field_names = array('No', 'Date Time', 'To', 'Amount','Remark');
		$field_values = array('id', '`datetime`', '`to`', 'amount','remark');
		$table = $tb['transaction_out'];
		$col = 'to';
	}
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}

	$tsort_by = $sort_by;

	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}
	
	$q=new sql($db);
	$sql="SELECT * FROM {$table} WHERE aid = '$aid' AND LEFT(`datetime`,10) >= '$from' AND LEFT(`datetime`,10) <= '$to' ORDER BY $sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT * FROM {$table} WHERE aid = '$aid' AND LEFT(`datetime`,10) >= '$from' AND LEFT(`datetime`,10) <= '$to' ORDER BY id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$history_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$history_info[$i]['tid'] = $rows['id'];
			$history_info[$i]['datetime'] = $rows['datetime'];
			$history_info[$i][$col] = $rows[$col];
			$history_info[$i]['amount'] = $rows['amount'];
			$history_info[$i]['remark'] = $rows['remark'];
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"history",'view',"&sort_by=$sort_by&sort_order=$sort_order&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day");
		$extra="&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day";
		$smarty->assign("type", $type);
		$smarty->assign("extra",$extra);
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign("field_names", $field_names);
		$smarty->assign("field_values", $field_values);
		$smarty->assign('history_info',$history_info);
		$smarty->assign('view','yes');
	}
	else
	{
		$smarty->assign("view","no");
	}
	display_history_form();
}

function view()
{
	global $smarty,$s,$db,$tb,$errormsg;
	
	if (empty($_POST))
	{
		$_POST = $_GET;
	}

	$from = dateCheck($_POST['from_year'], $_POST['from_month'], $_POST['from_day'], "From");
	$to = dateCheck($_POST['to_year'], $_POST['to_month'], $_POST['to_day'], "To");
	if ($from > $to)
	{
		errormsg("To cannot earlier than From.");
	}

	if ($errormsg)
	{
		display_history_form();
	}
	else
	{
		display_history_list($from,$to);
	}
}

function display_history_form()
{
	global $s,$db,$tb,$smarty,$errormsg;

	$id = get_userid();
	$account_list = get_account_list($id);

	if ($account_list)
	{
		$cyear = date("Y");
		$cmonth = date("m");
		$cday = date("d");
		$year = getyearlist('2004',$cyear);
		$month = getmonthlist();
		$day = getdaylist();

		$smarty->assign("empty","no");
		$smarty->assign('errormsg',$errormsg);
		$smarty->assign('yearlist',$year);
		$smarty->assign('monthlist',$month);
		$smarty->assign('daylist',$day);
		$smarty->assign('year',$cyear);
		$smarty->assign('month',$cmonth);
		$smarty->assign('day',$cday);
		$smarty->assign('account_list',$account_list);
	}
	else
	{
		$smarty->assign("empty","yes");
	}
	$smarty->display('history.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'view':
			view();
			break;

			default:
			display_history_form();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'view':
			view();
			break;

			default:
			display_history_form();
		}
	}
	else
	{
		display_history_form();
	}
}
?>