<?php
function process($type)
{
	global $s, $smarty, $db, $tb, $errormsg;

	$phone_no = strip_tags($_POST['phone_no']);
	check_field($phone_no,"Phone Number",1,"num",5);

	$cid = $_POST['cid'];
	$phone_type = $_POST['phone_type'];

	if ($type=='edit')
	{
		$id = $_POST['id'];
	}

	if ($errormsg)
	{
		display_form($type,$id);
	}
	else
	{
		if ($type == 'add')
		{
			$q=new sql($db);
			$sql = "INSERT INTO {$tb['phone']} VALUES ('', '$cid', '$phone_no', '$phone_type', 'NO', NOW(), NOW())";
			if (get_final_id($tb['phone']) == $_POST['final'])
			{
				$q->query($sql);
			}
			$smarty->assign('type',$type);
			$smarty->display('phone_success.tpl');
		}
		else
		{
			$q=new sql($db);
			$sql = "UPDATE {$tb['phone']} SET phone_no = '$phone_no', type_id = '$phone_type', date_last_modified = NOW() WHERE id = '$id'";
			$q->query($sql);
			$smarty->assign('type',$type);
			$smarty->display('phone_success.tpl');
		}
	}
}

function display_form($type,$id='')
{
	global $smarty,$s,$db,$tb,$errormsg;
	$cid = get_userid();
	$typelist = get_phone_type_list();
	if ($type=='edit')
	{
		$phone_info = get_phone_info($id);
		$smarty->assign('id',$id);
		$smarty->assign('phone_info',$phone_info);
	}
	else
	{
		$final=get_final_id($tb['phone']);
		$smarty->assign('final',$final);
	}
	$smarty->assign('error',$errormsg);
	$smarty->assign('type',$type);
	$smarty->assign('cid',$cid);
	$smarty->assign('typelist',$typelist);
	$smarty->display('phone_form.tpl');
}

function delete_phone()
{
	global $s,$db,$tb,$errormsg;

	$id = $_GET['id'];
	$q=new sql($db);
	$sql = "DELETE FROM {$tb['phone']} WHERE id = '$id'";
	$q->query($sql);
	display_phone_list();
}

function update_primary()
{
	global $s,$db,$tb,$errormsg;

	$id = $_GET['id'];
	$cid = $_GET['cid'];
	if (!empty($id))
	{
		$q=new sql($db);
		$sql = "UPDATE {$tb['phone']} SET `primary` = 'NO' WHERE cid = '$cid'";
		$q->query($sql);
		$sql = "UPDATE {$tb['phone']} SET `primary` = 'YES' WHERE id = '$id'";
		$q->query($sql);
	}
	else
	{
		errormsg("No Address Is Selected");
	}
	display_phone_list();
}

function display_phone_list()
{
	global $smarty,$s,$db,$tb,$errormsg;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Phone Number', 'Phone Type');
	$field_values = array('id', 'phone_no', 'phone_type');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}
	if ($sort_by=='phone_type')
	{
		$sort_by=$tb['phone_type'].".type_name";
	}
	$id = get_userid();
	$q=new sql($db);
	$sql="SELECT {$tb['phone']}.id, {$tb['phone']}.phone_no, {$tb['phone_type']}.type_name, {$tb['phone']}.primary FROM {$tb['customer']} INNER JOIN {$tb['phone']} ON {$tb['customer']}.id = {$tb['phone']}.cid INNER JOIN {$tb['phone_type']} ON {$tb['phone']}.type_id = {$tb['phone_type']}.id WHERE {$tb['customer']}.id = '$id' ORDER BY {$tb['phone']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT {$tb['phone']}.id, {$tb['phone']}.phone_no, {$tb['phone_type']}.type_name, {$tb['phone']}.primary FROM {$tb['customer']} INNER JOIN {$tb['phone']} ON {$tb['customer']}.id = {$tb['phone']}.cid INNER JOIN {$tb['phone_type']} ON {$tb['phone']}.type_id = {$tb['phone_type']}.id WHERE {$tb['customer']}.id = '$id' ORDER BY {$tb['phone']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$phone_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$phone_info[$i]['tid'] = $rows['id'];
			$phone_info[$i]['phone_no'] = $rows['phone_no'];
			$phone_info[$i]['phone_type'] = $rows['type_name'];
			$phone_info[$i]['primary'] = $rows['primary'];
			$phone_info[$i]['edit'] = "<a href=\"?opt=phone&act=edit&id={$rows['id']}\"><img src=\"{$s['img_path']}button_edit.png\" border=\"0\" alt=\"Edit\"></a>";
			$phone_info[$i]['delete'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to delete this ?' ,'?opt=phone&act=delete&id={$rows['id']}&cid=$id&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Delete\"></a>";
			$phone_info[$i]['update_primary'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to update this as primary?' ,'?opt=phone&act=primary&id={$rows['id']}&cid=$id&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_primary.png\" border=\"0\" alt=\"Update Primary\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"phone",'',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("empty","no");
		$smarty->assign("errormsg",$errormsg);
		$smarty->assign("cid",$id);
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign("field_names", $field_names);
		$smarty->assign("field_values", $field_values);
		$smarty->assign('phone_info',$phone_info);
		$smarty->display('phone.tpl');
	}
	else
	{
		$smarty->assign("empty","yes");
		$smarty->display('phone.tpl');
	}
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'edit':
			display_form('edit',$_GET['id']);
			break;

			case 'delete':
			delete_phone();
			break;
			
			case 'primary':
			update_primary();
			break;

			default:
			display_phone_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{

			case 'add_new':
			display_form('add');
			break;

			case 'process':
			process($_POST['type']);
			break;

			default:
			display_phone_list();
		}
	}
	else
	{
		display_phone_list();
	}
}
?>