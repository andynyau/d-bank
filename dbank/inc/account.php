<?php
/* removed
function insert_account($type)
{
	global $smarty,$s,$db,$tb;
	$finalid = get_final_id($tb['account']);
	$account_no = $_POST['account_no'];
	if ($finalid == $_POST['final'])
	{
		$id = get_userid();
		
		$q=new sql($db);
		$sql = "INSERT INTO {$tb['account']} VALUES ('','$id','$account_no','$type','pending(open)',NOW(),NOW())";
		$q->query($sql);
	}
	$smarty->assign('account_no',$account_no);
	$smarty->display('account_success.tpl');
}

function display_agree($type)
{
	global $smarty,$s,$db,$tb;

	$type_name = get_account_type($type);
	$account_no = make_accountno($type);
	$finalid = get_final_id($tb['account']);
	$smarty->assign('final',$finalid);
	$smarty->assign('account_no',$account_no);
	$smarty->assign('account_type',$type);
	$smarty->assign('account_name',$type_name);
	$smarty->display('account_terms.tpl');
}
*/
function display_account_list()
{
	global $smarty,$s,$db,$tb,$errormsg;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Account No', 'Type', 'Date Created', 'Status');
	$field_values = array('id', 'account_no', 'type_name', 'date_created', 'status');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;	
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	if ($sort_by == 'type_name')
	{
		$sort_by = "{$tb['account_type']}.type_name";
	}

	$id = get_userid();
	$q=new sql($db);
	$sql="SELECT {$tb['account']}.id, {$tb['account']}.account_no, {$tb['account']}.date_created, {$tb['account']}.status, {$tb['account_type']}.type_name FROM {$tb['account']} INNER JOIN {$tb['account_type']} ON {$tb['account']}.type = {$tb['account_type']}.id WHERE {$tb['account']}.cid = '$id' ORDER BY {$tb['account']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT {$tb['account']}.id, {$tb['account']}.account_no, {$tb['account']}.date_created, {$tb['account']}.status, {$tb['account_type']}.type_name FROM {$tb['account']} INNER JOIN {$tb['account_type']} ON {$tb['account']}.type = {$tb['account_type']}.id WHERE {$tb['account']}.cid = '$id' ORDER BY {$tb['account']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$account_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$account_info[$i]['tid'] = $rows['id'];
			$account_info[$i]['account_no'] = $rows['account_no'];
			$account_info[$i]['type'] = $rows['type_name'];
			$account_info[$i]['status'] = $rows['status'];
			$account_info[$i]['date_created'] = $rows['date_created'];
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"account",'',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("empty","no");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign("field_names", $field_names);
		$smarty->assign("field_values", $field_values);
		$smarty->assign('account_info',$account_info);
		$smarty->display('account.tpl');
	}
	else
	{
		$smarty->assign("empty","yes");
		$smarty->display('account.tpl');
	}
}

if ($_SERVER['PHP_SELF'] != '/dbank/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			default:
			display_account_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			default:
			display_account_list();
		}
	}
	else
	{
		display_account_list();
	}
}
?>