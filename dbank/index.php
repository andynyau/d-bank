<?php
require_once('../lib/config.php');
$benchmark = new Benchmark_Timer();
$benchmark->start();
$smarty = new Smarty_Default;
$smarty->assign('validate',validate_login());
$smarty->assign('url',$s['url']);
$smarty->assign('site_name',$s['site_name']);
$smarty->assign('img_path',$s['img_path']);
$smarty->assign('meta_desc',$s['meta_desc']);
$smarty->assign('meta_keywds',$s['meta_keywds']);
$smarty->assign('meta_copy',$s['meta_copy']);

function display_default()
{
	global $s,$smarty;
	$smarty->display('index.tpl');
}

if ((empty($_GET['opt'])) && (empty($_POST['opt'])))
{	
	display_default();
}
else
{
	if (validate_login())
	{
		if (!empty($_POST['opt']))
		{
			$inc = $_POST['opt'];
			include $s['inc_path'].$inc.'.php';
		}
		elseif (!empty($_GET['opt']))
		{
			$inc = $_GET['opt'];
			include $s['inc_path'].$inc.'.php';
		}
		else
		{
			display_default();
		}
	}
	elseif (($_POST['opt'] == 'login') || ($_POST['opt'] == 'register'))
	{
		$inc = $_POST['opt'];
		include './inc/'.$inc.'.php';
	}
	elseif (($_GET['opt'] == 'register') || ($_GET['opt'] == 'services') || ($_GET['opt'] == 'terms') || ($_GET['opt'] == 'help') || ($_GET['opt'] == 'verify_email'))
	{
		$inc = $_GET['opt'];
		include './inc/'.$inc.'.php';
	}
	else
	{
		display_default();
	}
}
writeWebLog('customer');
$benchmark->stop();
//$benchmark->display();
?>
