<?php
require_once('../lib/config.php');
$smarty = new Smarty_Default;
$smarty->assign('url',$s['url']);
$smarty->assign('customer_url',$s['customer_url']);
$smarty->assign('admin_url',$s['admin_url']);
$smarty->assign('site_name',$s['site_name']);
$smarty->assign('img_path',$s['img_path']);
$smarty->assign('meta_desc',$s['meta_desc']);
$smarty->assign('meta_keywds',$s['meta_keywds']);
$smarty->assign('meta_copy',$s['meta_copy']);

function display_default()
{
	global $s,$smarty;
	$smarty->display('index.tpl');
}

display_default();
writeWebLog('customer');
?>
