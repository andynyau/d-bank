function setCheckboxes(the_form, do_check)
{
    var elts      = (typeof(document.forms[the_form].elements['selected_num[]']) != 'undefined')
                  ? document.forms[the_form].elements['selected_num[]']
                  : document.forms[the_form].elements['selected_num[]'];
    var elts_cnt  = (typeof(elts.length) != 'undefined')
                  ? elts.length
                  : 0;

    if (elts_cnt) {
        for (var i = 0; i < elts_cnt; i++) {
            elts[i].checked = do_check;
        } // end for
    } else {
        elts.checked        = do_check;
    } // end if... else

    return true;
} // end of the 'setCheckboxes()' function

/**
  * Checks/unchecks all options of a <select> element
  *
  * @param   string   the form name
  * @param   string   the element name
  * @param   boolean  whether to check or to uncheck the element
  *
  * @return  boolean  always true
  */

function lock(){		
	for(var i=0 ; i<arguments.length ; i++) {		
		document.getElementById(arguments[i]+"_1\]").className='disableInput';
		document.getElementById(arguments[i]+"_2\]").className='disableInput';
		document.getElementById(arguments[i]+"_2\]").value='';
		document.getElementById(arguments[i]+"_1\]").value='';		
		document.getElementById(arguments[i]+"_1\]").disabled = true;
		document.getElementById(arguments[i]+"_2\]").disabled = true;
	}	
}
function unlock(){		
	for(var i=0 ; i<arguments.length ; i++) {		
		document.getElementById(arguments[i]+"_1\]").className='';
		document.getElementById(arguments[i]+"_2\]").className='';
		document.getElementById(arguments[i]+"_1\]").disabled = false;
		document.getElementById(arguments[i]+"_2\]").disabled = false;
	}	
}

function clearText(thefield){
if (thefield.defaultValue==thefield.value)
thefield.value = ""
} 

function printPage() {
	if (window.print) {
		agree = confirm('This page contains sensitive information that \\nwe recommend you print a copy of at this time. \\n\\nOK to print now?');
		if (agree) window.print(); 
	}
}

function confirmmsg(msg, loc){
	question = confirm(msg)
	if (question != "0"){
		top.location = loc
	}
}

function rowOverEffect(object) {
  if (object.className == 'dataTableRow') object.className = 'dataTableRowOver';
}

function rowOutEffect(object) {
  if (object.className == 'dataTableRowOver') object.className = 'dataTableRow';
}