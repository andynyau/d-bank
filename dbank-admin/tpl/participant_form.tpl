{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>{if $type == 'add'}Add Address{else}Edit Address{/if}</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<form method="post" action="?">
<table width="300" border="0">
	<tr>
		<td class="blacktb">Company Name</td>
	</tr>
	<tr>
		<td>
		  <input type="text" name="company_name" class="textboxlong" maxlength="64"{if $type=='edit'} value="{$participant_info.company_name}"{/if}>
		</td>
	</tr>
	<tr>
		<td class="blacktb">Contact Person</td>
	</tr>
	<tr>
		<td>
			<input type="text" name="contact_person" class="textboxlong" maxlength="64"{if $type=='edit'} value="{$participant_info.contact_person}"{/if}>
		</td>
	</tr>
	<tr>
		<td class="blacktb">Company Registration No</td>
	</tr>
	<tr>
		<td>
			<input type="text" name="company_registration_no" class="textboxlong" maxlength="16"{if $type=='edit'} value="{$participant_info.company_registration_no}"{/if}>
		</td>
	</tr>
	<tr>
		<td class="blacktb">Phone</td>
	</tr>
	<tr>
		<td>
			<input type="text" name="phone" class="textboxlong" maxlength="64"{if $type=='edit'} value="{$participant_info.phone}"{/if}>
		</td>
	</tr>
	<tr>
		<td class="blacktb">Address</td>
	</tr>
	<tr>
		<td>
			<input type="text" name="address" class="textboxlong" maxlength="64"{if $type=='edit'} value="{$participant_info.address}"{/if}>
		</td>
	</tr>
	<tr>
		<td><br>
			{if $type=='edit'}<input type="hidden" name="id" value="{$id}">{else}<input type="hidden" name="final" value="{$final}">{/if}
			<input type="hidden" name="opt" value="participant">
			<input type="hidden" name="act" value="process">
			<input type="hidden" name="type" value="{$type}">
			<input type="submit" value={if $type=="add"}"Add"{else}"Edit"{/if} class="button1">
			<input type="reset" value="Clear" class="button1">
		</td>
	</tr>
</table>
</form>
{include file="footer.tpl"}