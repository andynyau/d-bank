{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Participant</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<table border="0" width="950" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=participant&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
{if $validate == '1'}
<td>Action</td>
{/if}
</tr>
{foreach from=$participant_info item=participant}
	<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">
			{$participant.id}
		</td>
		<td width="200">
			{$participant.company_name}
		</td>
		<td width="180">
			{$participant.contact_person}
		</td>
		<td width="200">
			{$participant.company_registration_no}
		</td>
		<td width="100">
			{$participant.phone}
		</td>
		<td width="200">
			{$participant.address}
		</td>
		{if $validate == '1'}
		<td align="center">
			{$participant.edit}&nbsp{$participant.delete}
		</td>
		{/if}
	</tr>
{/foreach}
<tr>
	<td colspan="8" class="blacktb">{$pg_link}</td>
</tr>
</table>
{if $validate == '1'}
<form method="post" action="?">
	<input type="hidden" name="opt" value="participant">
	<input type="hidden" name="act" value="add">
	<input type="submit" value="Add New Participant" class="button1">
</form>
{/if}
{include file="footer.tpl"}