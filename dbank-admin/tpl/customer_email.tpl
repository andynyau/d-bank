{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Customer Email</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<table border="0" width="950" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=customer&act=email&id={$cid}&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
{if $validate != '2'}
<td>Action</td>
{/if}
</tr>
{foreach from=$email_info item=email}
		{if $email.primary == 'YES'}
		<tr class="whitetb">
		{else}
		<tr class="{cycle values="orangetb1,greytb"}">
		{/if}
		<td width="20">
			{$email.id}
		</td>
		<td width="350">
			{$email.email}
		</td>
		<td width="100">
			{$email.status}
		</td>
		{if $validate != '2'}
		<td align="center">			
			{$email.edit}&nbsp{$email.delete}{if $email.status == 'unverify'}&nbsp;{$email.resend}{else}&nbsp;{$email.update_primary}{/if}
		</td>
		{/if}
	</tr>
{/foreach}
<tr>
	<td colspan="8" class="blacktb">{$pg_link}</td>
</tr>
{if $validate != '2'}
<tr>
	<td colspan="8">
	<form method="post" action="?">
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="add_email">
		<input type="hidden" name="cid" value="{$cid}">
		<input type="submit" value="Add New Email" class="button1">
	</form>
	</td>
</tr>
{/if}
</table>
{include file="footer.tpl"}