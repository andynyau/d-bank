{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Report</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
<table border="0" cellpadding="1" cellspacing="1">
<tr>
	<td valign="top">
		<form method="post" action="?">
		<table border="0" width="230" cellpadding="1" cellspacing="1">
		<tr>
			<td class="blacktb">From</td>
		</tr>
		<tr>
			<td>
				<select name="from_year">
				{foreach from = $yearlist item = itemyear}
					<option value="{$itemyear}"{if $itemyear == $year} selected{/if}>{$itemyear}</option>
				{/foreach}
				</select>
				<select name="from_month">
				{foreach from=$monthlist key=keymonth item=itemmonth}
					<option value="{$keymonth}"{if $keymonth == $month-1} selected{/if}>{$itemmonth}</option>
				{/foreach}
				</select>
				<select name="from_day">
				{foreach from=$daylist item=itemday}
					{if $itemday lt 10}{assign var="itemday" value="0$itemday"}{/if}
					<option value="{$itemday}"{if $itemday == $day} selected{/if}>{$itemday}</option>
				{/foreach}
				</select>
				<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td class="blacktb">To</td>
		</tr>
		<tr>
			<td>
			<select name="to_year">
			{foreach from = $yearlist item = itemyear}
				<option value="{$itemyear}"{if $itemyear == $year} selected{/if}>{$itemyear}</option>
			{/foreach}
			</select>
			<select name="to_month">
			{foreach from=$monthlist key=keymonth item=itemmonth}
				<option value="{$keymonth}"{if $keymonth == $month} selected{/if}>{$itemmonth}</option>
			{/foreach}
			</select>
			<select name="to_day">
			{foreach from=$daylist item=itemday}
				{if $itemday lt 10}{assign var="itemday" value="0$itemday"}{/if}
				<option value="{$itemday}"{if $itemday == $day} selected{/if}>{$itemday}</option>
			{/foreach}
			</select>
			<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td class="blacktb">Type</td>
		</tr>
		<tr>
			<td>
				<input type="radio" name="type" value="in" checked>Transaction in
				<br>
				<input type="radio" name="type" value="out">Transaction out
				<br>
				<input type="radio" name="type" value="cc">Credit Card Payment
				<br>
				<input type="radio" name="type" value="bill">Bill Payment
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<input type="hidden" name="opt" value="report">
				<input type="hidden" name="act" value="view">
				<input type="submit" value="View" class="button1">
			</td>
		</tr>
		</table>
		</form>
	</td>
{if $view == 'yes'}
<td width="50">
&nbsp;
</td>
<td valign="top">
<table border="0" width="650" cellpadding="1" cellspacing="1">
	<tr class="blacktb">
	{section name = field_rows loop = $field_values}
		<td>
			<a href="?opt=report&act=view&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}{$extra}">{$field_names[field_rows]}</a>
		</td>
	{/section}
	</tr>
	{if $type == 'in'}
	{foreach from=$report_info item=report}
		<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">{$report.id}</td>
		<td width="140">{$report.datetime}</td>
		<td width="90">{$report.from}</td>
		<td width="90">{$report.account_no}</td>
		<td width="80" align="right">{$report.amount}</td>
		<td>{$report.remark}</td>
		</tr>
	{/foreach}
	{elseif $type == 'out'}
	{foreach from=$report_info item=report}
		<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">{$report.id}</td>
		<td width="140">{$report.datetime}</td>
		<td width="90">{$report.to}</td>
		<td width="90">{$report.account_no}</td>
		<td width="80" align="right">{$report.amount}</td>
		<td>{$report.remark}</td>
		</tr>
	{/foreach}
	{elseif $type == 'cc'}
	{foreach from=$report_info item=report}
		<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">{$report.id}</td>
		<td width="140">{$report.date_pay}</td>
		<td width="150">{$report.cc_no}</td>
		<td width="150">{$report.account_no}</td>
		<td align="right">{$report.amount}</td>
		</tr>
	{/foreach}
	{elseif $type == 'bill'}
	{foreach from=$report_info item=report}
		<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">{$report.id}</td>
		<td width="140">{$report.date_pay}</td>
		<td width="160">{$report.company_name}</td>
		<td width="90">{$report.account_no}</td>
		<td width="80"align="right">{$report.amount}</td>
		<td>{$report.remark}</td>
		</tr>
	{/foreach}
	{/if}
	<tr>
		<td colspan="8" class="blacktb">{$pg_link}</td>
	</tr>
	<tr>
		<td colspan="8" class="whitetb">Total Amount : {$total}</td>
	</tr>
</table>
</td>
{/if}
</table>
{include file="footer.tpl"}