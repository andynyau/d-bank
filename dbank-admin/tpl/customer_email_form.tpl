{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>{if $type == 'add'}Add Email{else}Edit Email{/if}</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<form method="post" action="?">
<table width="400" height="80" border="0" class="blacktb">
  <tr>
	<td>Email Address</td>
	<td>
	  <input type="text" name="email" class="textboxname" maxlength="64"{if $type=='edit'} value="{$email_info.email}"{/if}>
	</td>
  </tr>
  <tr>
	<td>Confirm Email Address</td>
	<td>
	  <input type="text" name="email2" class="textboxname" maxlength="64">
	</td>
  </tr>
  <tr>
	<td>&nbsp;</td>
  </tr>
  <tr>
	<td colspan="2" align="center">
		{if $type=='edit'}<input type="hidden" name="id" value="{$id}">{else}<input type="hidden" name="final" value="{$final}">{/if}
		<input type="hidden" name="cid" value="{$cid}">
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="process_email">
		<input type="hidden" name="type" value="{$type}">
		<input type="submit" value={if $type=="add"}"Add"{else}"Edit"{/if} class="button1">
		<input type="reset" value="Clear" class="button1">
	</td>
  </tr>
</table>
<table border="0">
  <tr>
	<td colspan="2" align="center">
		<a href="?opt=email"><font class="text_u">Back</font></a><font class="text"> To Email List.</font>
	</td>
  </tr>
</table>
</form>
{include file="footer.tpl"}