{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Customer Phone</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<table border="0" width="950" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=customer&act=phone&id={$cid}&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
{if $validate != '2'}
<td>Action</td>
{/if}
</tr>
{foreach from=$phone_info item=phone}
		{if $phone.primary == 'YES'}
		<tr class="whitetb">
		{else}
		<tr class="{cycle values="orangetb1,greytb"}">
		{/if}
		<td width="20">
			{$phone.id}
		</td>
		<td width="350">
			{$phone.phone_no}
		</td>
		<td width="100">
			{$phone.type_name}
		</td>
		{if $validate != '2'}
		<td align="center">			
			{$phone.edit}&nbsp{$phone.delete}&nbsp{$phone.update_primary}
		</td>
		{/if}
	</tr>
{/foreach}
<tr>
	<td colspan="8" class="blacktb">{$pg_link}</td>
</tr>
{if $validate != '2'}
<tr>
	<td colspan="8">
	<form method="post" action="?">
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="add_phone">
		<input type="hidden" name="cid" value="{$cid}">
		<input type="submit" value="Add New Phone" class="button1">
	</form>
	</td>
</tr>
{/if}
</table>
{include file="footer.tpl"}