{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Photo</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
<table border="0" width="900" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=photo&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
<td>Photo</td><td>Action</td>
</tr>
{foreach from=$photo_info item=photo}
	<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">
			{$photo.id}
		</td>
		<td width="200">
			{$photo.username}
		</td>
		<td width="620">
			{$photo.image}
		</td>
		<td align="center">
			{$photo.approve}&nbsp;{$photo.decline}
		</td>
	</tr>
{/foreach}
<tr>
	<td colspan="4" class="blacktb">{$pg_link}</td>
</tr>
</table>
{include file="footer.tpl"}