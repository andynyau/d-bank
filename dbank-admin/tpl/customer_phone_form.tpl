{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>{if $type == 'add'}Add Phone{else}Edit Phone{/if}</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<form method="post" action="?">
<table width="400" height="100" border="0" class="blacktb">
  <tr>
	<td>Phone Number</td>
	<td>
	  <input type="text" name="phone_no" class="textboxname" maxlength="64"{if $type=='edit'} value="{$phone_info.phone_no}"{/if}>
	</td>
  </tr>
  <tr>
	<td>Phone Type</td>
	<td>
		<select name="phone_type">
		{foreach from=$typelist item=ptype}
			<option value="{$ptype.id}"{if $type=='edit'}{if $ptype.id == $phone_info.type_id} selected{/if}{else}{if $ptype.id == '1'} selected{/if}{/if}>{$ptype.type_name}</option>
		{/foreach}
		</select>
	</td>
  </tr>
  
  <tr>
	<td>&nbsp;</td>
  </tr>
  <tr>
	<td colspan="2" align="center">
		{if $type=='edit'}<input type="hidden" name="id" value="{$id}">{else}<input type="hidden" name="final" value="{$final}">{/if}
		<input type="hidden" name="cid" value="{$cid}">
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="process_phone">
		<input type="hidden" name="type" value="{$type}">
		<input type="submit" value={if $type=="add"}"Add"{else}"Edit"{/if} class="button1">
		<input type="reset" value="Clear" class="button1">
	</td>
  </tr>
</table>
</form>
{include file="footer.tpl"}