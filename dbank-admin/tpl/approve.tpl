{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Approve Transaction</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
<table border="0" width="900" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=approve&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
<td>Action</td>
</tr>
{foreach from=$pending_info item=pending}
	<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">
			{$pending.id}
		</td>
		<td width="140">
			{$pending.datetime}
		</td>
		<td width="90">
			{$pending.from}
		</td>
		<td width="90">
			{$pending.to}
		</td>
		<td width="80">
			{$pending.amount}
		</td>
		<td width="240">
			{$pending.remark}
		</td>
		<td align="center">
			{$pending.approve}&nbsp;{$pending.decline}
		</td>
	</tr>
{/foreach}
<tr>
	<td colspan="7" class="blacktb">{$pg_link}</td>
</tr>
</table>
{include file="footer.tpl"}