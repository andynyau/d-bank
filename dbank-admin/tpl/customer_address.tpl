{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Customer Address</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<table border="0" width="950" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=customer&act=address&id={$cid}&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
{if $validate != '2'}
<td>Action</td>
{/if}
</tr>
{foreach from=$address_info item=address}
		{if $address.primary == 'YES'}
		<tr class="whitetb">
		{else}
		<tr class="{cycle values="orangetb1,greytb"}">
		{/if}
		<td width="20">
			{$address.id}
		</td>
		<td width="350">
			{$address.street}
		</td>
		<td width="100">
			{$address.city}
		</td>
		<td width="80">
			{$address.postcode}
		</td>
		<td width="150">
			{$address.state}
		</td>
		<td width="100">
			{$address.countries_name}
		</td>
		{if $validate != '2'}
		<td align="center">			
			{$address.edit}&nbsp{$address.delete}&nbsp{$address.update_primary}
		</td>
		{/if}
	</tr>
{/foreach}
<tr>
	<td colspan="8" class="blacktb">{$pg_link}</td>
</tr>
{if $validate != '2'}
<tr>
	<td colspan="8">
	<form method="post" action="?">
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="add_address">
		<input type="hidden" name="cid" value="{$cid}">
		<input type="submit" value="Add New Address" class="button1">
	</form>
	</td>
</tr>
{/if}
</table>
{include file="footer.tpl"}