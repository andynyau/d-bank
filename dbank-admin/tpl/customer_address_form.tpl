{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>{if $type == 'add'}Add Address{else}Edit Address{/if}</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<form method="post" action="?">
<table width="400" height="300" border="0" class="blacktb">
  <tr>
	<td>Street</td>
	<td>
	  <input type="text" name="street" class="textboxlong" maxlength="64"{if $type=='edit'} value="{$address_info.street}"{/if}>
	</td>
  </tr>
  <tr>
	<td>City</td>
	<td>
	  <input type="text" name="city" class="textboxname" maxlength="64"{if $type=='edit'} value="{$address_info.city}"{/if}>
	</td>
  </tr>
  <tr>
	<td>Postcode</td>
	<td><input type="text" name="postcode" class="textboxname" maxlength="8"{if $type=='edit'} value="{$address_info.postcode}"{/if}></td>
  </tr>
  <tr>
	<td>State</td>
	<td><input type="text" name="state" class="textboxname" maxlength="64"{if $type=='edit'} value="{$address_info.state}"{/if}></td>
  </tr>
  <tr>
	<td>Country</td>
	<td>
		<select name="country">
		{foreach from=$countrylist item=country}
			<option value="{$country.id}"{if $type=='edit'}{if $country.id == $address_info.countries_id} selected{/if}{else}{if $country.id == '129'} selected{/if}{/if}>{$country.countries_name}</option>
		{/foreach}
		</select>
	</td>
  </tr>
  
  <tr>
	<td>&nbsp;</td>
  </tr>
  <tr>
	<td colspan="2" align="center">
		{if $type=='edit'}<input type="hidden" name="id" value="{$id}">{else}<input type="hidden" name="final" value="{$final}">{/if}
		<input type="hidden" name="cid" value="{$cid}">
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="process_address">
		<input type="hidden" name="type" value="{$type}">
		<input type="submit" value={if $type=="add"}"Add"{else}"Edit"{/if} class="button1">
		<input type="reset" value="Clear" class="button1">
	</td>
  </tr>
</table>
</form>
{include file="footer.tpl"}