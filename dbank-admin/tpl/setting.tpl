{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Global Setting</h3>
{foreach from=$errormsg item=error}
	<font class="error">{$error}</font><br>
{/foreach}
<table border="0" width="900" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=setting&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
<td>
Action
</td>
</tr>
{foreach from=$setting_list item=setting}
	<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">
			{$setting.id}
		</td>
		<td width="150">
			{$setting.key}
		</td>
		<td width="320">
			{$setting.value}
		</td>
		<td width="350">
			{$setting.description}
		</td>
		<td align="center">
			{$setting.edit}
		</td>
	</tr>
{/foreach}
</table>
{if $edit == 'yes'}
<table border="0" width="900" cellspacing="0">
<form method="post" action="?">
	<tr class="blacktb">
		<td width="100">{$key}</td>
		<td width="150"><input type="text" name="value" class="textbox1" value="{$value}" maxlength="64"></td>
		<td width="50"><input type="submit" value="Edit" class="button1"></td>
		<td>
			<input type="hidden" name="opt" value="setting">
			<input type="hidden" name="act" value="process">
			<input type="hidden" name="id" value="{$eid}">
			<input type="hidden" name="sort_by" value="{$sort_by}">
			<input type="hidden" name="sort_order" value="{$sort_order}">
			<input type="hidden" name="start" value="{$start}">
		</td>
	</tr>
	</form>
</table>
{/if}
{include file="footer.tpl"}