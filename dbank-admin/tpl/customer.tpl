{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Customer</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<table border="0" width="950" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=customer&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
<td>Action</td>
</tr>
{foreach from=$customer_info item=customer}
	<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">
			{$customer.id}
		</td>
		<td width="130">
			{$customer.username}
		</td>
		<td width="200">
			{$customer.fullname}
		</td>
		<td width="150">
			{$customer.ic_passport}
		</td>
		<td width="100">
			{$customer.dob}
		</td>
		<td width="200">
			{$customer.date_created}
		</td>
		<td align="center">			{$customer.address}&nbsp&nbsp{$customer.phone}&nbsp&nbsp{$customer.email}&nbsp&nbsp{$customer.account}&nbsp&nbsp{$customer.cc}
		</td>
	</tr>
{/foreach}
<tr>
	<td colspan="8" class="blacktb">{$pg_link}</td>
</tr>
</table>
{include file="footer.tpl"}