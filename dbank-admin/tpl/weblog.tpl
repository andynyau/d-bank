{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Web Log</h3>
<table border="0" width="900" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=weblog&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
</tr>
{foreach from=$log_info item=log}
	<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">
			{$log.id}
		</td>
		<td>
			{$log.datetime}
		</td>
		<td>
			{$log.query_string}
		</td>
		<td>
			{$log.remote_addr}
		</td>
		<td>
			{$log.customer_user}
		</td>
		<td>
			{$log.admin_user}
		</td>
		<td>
			{$log.user_agent}
		</td>
	</tr>
{/foreach}
<tr>
	<td colspan="8" class="blacktb">{$pg_link}</td>
</tr>
</table>
{include file="footer.tpl"}