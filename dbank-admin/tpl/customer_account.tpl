{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Account</h3>
<table border="0" width="900" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=customer&act=account&id={$cid}&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
{if $validate != '2'}
<td>Action</td>
{/if}
</tr>
{foreach from=$account_info item=account}
	<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">
			{$account.id}
		</td>
		<td width="150">
			{$account.account_no}
		</td>
		<td width="100">
			{$account.type}
		</td>
		<td width="200">
			{$account.date_created}
		</td>
		<td width="100" align="right">
			{$account.balance}
		</td>
		<td width="100" align="right">
			{$account.status}
		</td>
		{if $validate != '2'}
		<td align="center">
			{$account.action}&nbsp;{$account.delete}
		</td>
		{/if}
	</tr>
{/foreach}
<tr>
	<td colspan="8" class="blacktb">{$pg_link}</td>
</tr>
{if $validate != '2'}
<tr>
	<td colspan="8">
	<form method="post" action="?">
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="add_account">
		<input type="hidden" name="cid" value="{$cid}">
		<input type="submit" value="Add New Account" class="button1">
	</form>
	</td>
</tr>
{/if}
</table>
{include file="footer.tpl"}