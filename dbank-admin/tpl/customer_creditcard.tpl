{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Customer Credit Card</h3>
<table border="0" width="900" cellpadding="1" cellspacing="1">
<tr class="blacktb">
{section name = field_rows loop = $field_values}
	<td>
		<a href="?opt=customer&act=cc&id={$cid}&sort_by={$field_values[field_rows]}{if $field_values[field_rows] == $sort_by && $sort_order == 'desc'}&sort_order={elseif $field_values[field_rows] == $sort_by && $sort_order == ''}&sort_order=desc{/if}&start={$start}">{$field_names[field_rows]}</a>
	</td>
{/section}
{if $validate != '2'}
<td>Action</td>
{/if}
</tr>
{foreach from=$cc_info item=cc}
	<tr class="{cycle values="orangetb1,greytb"}">
		<td width="20">
			{$cc.id}
		</td>
		<td width="150">
			{$cc.fullname}
		</td>
		<td width="150">
			{$cc.cc_no}
		</td>
		<td width="120">
			{$cc.type_name}
		</td>
		<td width="150">
			{$cc.since}
		</td>
		<td width="150">
			{$cc.expires}
		</td>
		<td width="100">
			{$cc.status}
		</td>
		{if $validate != '2'}
		<td align="center">
			{$cc.action}&nbsp;{$cc.delete}
		</td>
		{/if}
	</tr>
{/foreach}
<tr>
	<td colspan="8" class="blacktb">{$pg_link}</td>
</tr>
{if $validate != '2'}
<tr>
	<td colspan="8">
	<form method="post" action="?">
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="add_cc">
		<input type="hidden" name="cid" value="{$cid}">
		<input type="submit" value="Add New Credit Card" class="button1">
	</form>
	</td>
</tr>
{/if}
</table>
{include file="footer.tpl"}