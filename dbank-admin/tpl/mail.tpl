{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Mail</h3>
<table border="0" width="100%">
<form method="post" action="?">
	<tr class="blacktb">
		<td height="21">
			To
		</td>
		<td>
			{if $from == 'mail'}All customers' primary email{/if}
		</td>
	</tr>
	<tr class="whitetb">
		<td colspan="2">
			&nbsp;
		</td>
	</tr>
	<tr class="blacktb">
		<td height="21">
			Subject
		</td>
		<td>
			<input type="text" name="subject" class="textboxlong">
		</td>
	</tr>
	<tr class="whitetb">
		<td colspan="2">
			&nbsp;
		</td>
	</tr>
	<tr class="blacktb">
		<td colspan="2" height="21">
			Message
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="message" rows="10" cols="115"></textarea>
		</td>
	</tr>
	<tr class="whitetb">
		<td colspan="2">
			&nbsp;
			<input type="hidden" name="opt" value="{$from}">
			<input type="hidden" name="act" value="sendmail">
			<input type="hidden" name="final" value="{$final}">
		</td>
	</tr>
	<tr class="blacktb">
		<td colspan="2" height="21" align="center">
			<input type="submit" value="Send" class="button1">
			&nbsp;
			<input type="reset" value="Clear" class="button1">
		</td>
	</tr>
</form>
</table>
{include file="footer.tpl"}