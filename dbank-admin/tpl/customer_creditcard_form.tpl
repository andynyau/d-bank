{config_load file=test.conf section="setup"}
{include file="header.tpl"}
<h3>Add Account</h3>
{foreach from=$error item=error_item}
	<font class="error">{$error_item}</font><br>
{/foreach}
<form method="post" action="?">
<table border="0" class="blacktb">
	<tr>
		<td>Customer : {$customer_name}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Credit Card Type : 
		<select name="cc_type">
		{foreach from=$type_list item=type}
			<option value="{$type.id}">{$type.type_name}</option>
		{/foreach}
		</select>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="process_cc">
		<input type="hidden" name="cid" value="{$cid}">
		<input type="hidden" name="final" value="{$final}">
		<input type="submit" value="Add Credit Card" class="button1">
		</td>
	</tr>
</form>
{include file="footer.tpl"}