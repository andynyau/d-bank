<?php /* Smarty version 2.6.5-dev, created on 2004-09-27 05:34:03
         compiled from customer_email_form.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'customer_email_form.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h3><?php if ($this->_tpl_vars['type'] == 'add'): ?>Add Email<?php else: ?>Edit Email<?php endif; ?></h3>
<?php if (count($_from = (array)$this->_tpl_vars['error'])):
    foreach ($_from as $this->_tpl_vars['error_item']):
?>
	<font class="error"><?php echo $this->_tpl_vars['error_item']; ?>
</font><br>
<?php endforeach; unset($_from); endif; ?>
<form method="post" action="?">
<table width="400" height="80" border="0" class="blacktb">
  <tr>
	<td>Email Address</td>
	<td>
	  <input type="text" name="email" class="textboxname" maxlength="64"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['email_info']['email']; ?>
"<?php endif; ?>>
	</td>
  </tr>
  <tr>
	<td>Confirm Email Address</td>
	<td>
	  <input type="text" name="email2" class="textboxname" maxlength="64">
	</td>
  </tr>
  <tr>
	<td>&nbsp;</td>
  </tr>
  <tr>
	<td colspan="2" align="center">
		<?php if ($this->_tpl_vars['type'] == 'edit'): ?><input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
"><?php else: ?><input type="hidden" name="final" value="<?php echo $this->_tpl_vars['final']; ?>
"><?php endif; ?>
		<input type="hidden" name="cid" value="<?php echo $this->_tpl_vars['cid']; ?>
">
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="process_email">
		<input type="hidden" name="type" value="<?php echo $this->_tpl_vars['type']; ?>
">
		<input type="submit" value=<?php if ($this->_tpl_vars['type'] == 'add'): ?>"Add"<?php else: ?>"Edit"<?php endif; ?> class="button1">
		<input type="reset" value="Clear" class="button1">
	</td>
  </tr>
</table>
<table border="0">
  <tr>
	<td colspan="2" align="center">
		<a href="?opt=email"><font class="text_u">Back</font></a><font class="text"> To Email List.</font>
	</td>
  </tr>
</table>
</form>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>