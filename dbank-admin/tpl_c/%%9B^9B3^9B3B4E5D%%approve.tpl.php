<?php /* Smarty version 2.6.5-dev, created on 2004-09-26 22:00:50
         compiled from approve.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'approve.tpl', 1, false),array('function', 'cycle', 'approve.tpl', 17, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h3>Approve Transaction</h3>
<?php if (count($_from = (array)$this->_tpl_vars['errormsg'])):
    foreach ($_from as $this->_tpl_vars['error']):
?>
	<font class="error"><?php echo $this->_tpl_vars['error']; ?>
</font><br>
<?php endforeach; unset($_from); endif; ?>
<table border="0" width="900" cellpadding="1" cellspacing="1">
<tr class="blacktb">
<?php unset($this->_sections['field_rows']);
$this->_sections['field_rows']['name'] = 'field_rows';
$this->_sections['field_rows']['loop'] = is_array($_loop=$this->_tpl_vars['field_values']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['field_rows']['show'] = true;
$this->_sections['field_rows']['max'] = $this->_sections['field_rows']['loop'];
$this->_sections['field_rows']['step'] = 1;
$this->_sections['field_rows']['start'] = $this->_sections['field_rows']['step'] > 0 ? 0 : $this->_sections['field_rows']['loop']-1;
if ($this->_sections['field_rows']['show']) {
    $this->_sections['field_rows']['total'] = $this->_sections['field_rows']['loop'];
    if ($this->_sections['field_rows']['total'] == 0)
        $this->_sections['field_rows']['show'] = false;
} else
    $this->_sections['field_rows']['total'] = 0;
if ($this->_sections['field_rows']['show']):

            for ($this->_sections['field_rows']['index'] = $this->_sections['field_rows']['start'], $this->_sections['field_rows']['iteration'] = 1;
                 $this->_sections['field_rows']['iteration'] <= $this->_sections['field_rows']['total'];
                 $this->_sections['field_rows']['index'] += $this->_sections['field_rows']['step'], $this->_sections['field_rows']['iteration']++):
$this->_sections['field_rows']['rownum'] = $this->_sections['field_rows']['iteration'];
$this->_sections['field_rows']['index_prev'] = $this->_sections['field_rows']['index'] - $this->_sections['field_rows']['step'];
$this->_sections['field_rows']['index_next'] = $this->_sections['field_rows']['index'] + $this->_sections['field_rows']['step'];
$this->_sections['field_rows']['first']      = ($this->_sections['field_rows']['iteration'] == 1);
$this->_sections['field_rows']['last']       = ($this->_sections['field_rows']['iteration'] == $this->_sections['field_rows']['total']);
?>
	<td>
		<a href="?opt=approve&sort_by=<?php echo $this->_tpl_vars['field_values'][$this->_sections['field_rows']['index']];  if ($this->_tpl_vars['field_values'][$this->_sections['field_rows']['index']] == $this->_tpl_vars['sort_by'] && $this->_tpl_vars['sort_order'] == 'desc'): ?>&sort_order=<?php elseif ($this->_tpl_vars['field_values'][$this->_sections['field_rows']['index']] == $this->_tpl_vars['sort_by'] && $this->_tpl_vars['sort_order'] == ''): ?>&sort_order=desc<?php endif; ?>&start=<?php echo $this->_tpl_vars['start']; ?>
"><?php echo $this->_tpl_vars['field_names'][$this->_sections['field_rows']['index']]; ?>
</a>
	</td>
<?php endfor; endif; ?>
<td>Action</td>
</tr>
<?php if (count($_from = (array)$this->_tpl_vars['pending_info'])):
    foreach ($_from as $this->_tpl_vars['pending']):
?>
	<tr class="<?php echo smarty_function_cycle(array('values' => "orangetb1,greytb"), $this);?>
">
		<td width="20">
			<?php echo $this->_tpl_vars['pending']['id']; ?>

		</td>
		<td width="140">
			<?php echo $this->_tpl_vars['pending']['datetime']; ?>

		</td>
		<td width="90">
			<?php echo $this->_tpl_vars['pending']['from']; ?>

		</td>
		<td width="90">
			<?php echo $this->_tpl_vars['pending']['to']; ?>

		</td>
		<td width="80">
			<?php echo $this->_tpl_vars['pending']['amount']; ?>

		</td>
		<td width="240">
			<?php echo $this->_tpl_vars['pending']['remark']; ?>

		</td>
		<td align="center">
			<?php echo $this->_tpl_vars['pending']['approve']; ?>
&nbsp;<?php echo $this->_tpl_vars['pending']['decline']; ?>

		</td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
<tr>
	<td colspan="7" class="blacktb"><?php echo $this->_tpl_vars['pg_link']; ?>
</td>
</tr>
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>