<?php /* Smarty version 2.6.5-dev, created on 2004-09-27 23:40:50
         compiled from customer_creditcard_form.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'customer_creditcard_form.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h3>Add Account</h3>
<?php if (count($_from = (array)$this->_tpl_vars['error'])):
    foreach ($_from as $this->_tpl_vars['error_item']):
?>
	<font class="error"><?php echo $this->_tpl_vars['error_item']; ?>
</font><br>
<?php endforeach; unset($_from); endif; ?>
<form method="post" action="?">
<table border="0" class="blacktb">
	<tr>
		<td>Customer : <?php echo $this->_tpl_vars['customer_name']; ?>
</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Credit Card Type : 
		<select name="cc_type">
		<?php if (count($_from = (array)$this->_tpl_vars['type_list'])):
    foreach ($_from as $this->_tpl_vars['type']):
?>
			<option value="<?php echo $this->_tpl_vars['type']['id']; ?>
"><?php echo $this->_tpl_vars['type']['type_name']; ?>
</option>
		<?php endforeach; unset($_from); endif; ?>
		</select>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
		<input type="hidden" name="opt" value="customer">
		<input type="hidden" name="act" value="process_cc">
		<input type="hidden" name="cid" value="<?php echo $this->_tpl_vars['cid']; ?>
">
		<input type="hidden" name="final" value="<?php echo $this->_tpl_vars['final']; ?>
">
		<input type="submit" value="Add Credit Card" class="button1">
		</td>
	</tr>
</form>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>