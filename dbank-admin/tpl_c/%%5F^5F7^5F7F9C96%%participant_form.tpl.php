<?php /* Smarty version 2.6.5-dev, created on 2004-09-26 06:45:41
         compiled from participant_form.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'config_load', 'participant_form.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_config_load(array('file' => "test.conf",'section' => 'setup'), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h3><?php if ($this->_tpl_vars['type'] == 'add'): ?>Add Address<?php else: ?>Edit Address<?php endif; ?></h3>
<?php if (count($_from = (array)$this->_tpl_vars['error'])):
    foreach ($_from as $this->_tpl_vars['error_item']):
?>
	<font class="error"><?php echo $this->_tpl_vars['error_item']; ?>
</font><br>
<?php endforeach; unset($_from); endif; ?>
<form method="post" action="?">
<table width="300" border="0">
	<tr>
		<td class="blacktb">Company Name</td>
	</tr>
	<tr>
		<td>
		  <input type="text" name="company_name" class="textboxlong" maxlength="64"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['participant_info']['company_name']; ?>
"<?php endif; ?>>
		</td>
	</tr>
	<tr>
		<td class="blacktb">Contact Person</td>
	</tr>
	<tr>
		<td>
			<input type="text" name="contact_person" class="textboxlong" maxlength="64"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['participant_info']['contact_person']; ?>
"<?php endif; ?>>
		</td>
	</tr>
	<tr>
		<td class="blacktb">Company Registration No</td>
	</tr>
	<tr>
		<td>
			<input type="text" name="company_registration_no" class="textboxlong" maxlength="16"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['participant_info']['company_registration_no']; ?>
"<?php endif; ?>>
		</td>
	</tr>
	<tr>
		<td class="blacktb">Phone</td>
	</tr>
	<tr>
		<td>
			<input type="text" name="phone" class="textboxlong" maxlength="64"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['participant_info']['phone']; ?>
"<?php endif; ?>>
		</td>
	</tr>
	<tr>
		<td class="blacktb">Address</td>
	</tr>
	<tr>
		<td>
			<input type="text" name="address" class="textboxlong" maxlength="64"<?php if ($this->_tpl_vars['type'] == 'edit'): ?> value="<?php echo $this->_tpl_vars['participant_info']['address']; ?>
"<?php endif; ?>>
		</td>
	</tr>
	<tr>
		<td><br>
			<?php if ($this->_tpl_vars['type'] == 'edit'): ?><input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id']; ?>
"><?php else: ?><input type="hidden" name="final" value="<?php echo $this->_tpl_vars['final']; ?>
"><?php endif; ?>
			<input type="hidden" name="opt" value="participant">
			<input type="hidden" name="act" value="process">
			<input type="hidden" name="type" value="<?php echo $this->_tpl_vars['type']; ?>
">
			<input type="submit" value=<?php if ($this->_tpl_vars['type'] == 'add'): ?>"Add"<?php else: ?>"Edit"<?php endif; ?> class="button1">
			<input type="reset" value="Clear" class="button1">
		</td>
	</tr>
</table>
</form>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>