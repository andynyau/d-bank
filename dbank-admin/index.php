<?php
require_once('../lib/config.php');

$smarty = new Smarty_Default;
$smarty->assign('validate',validate_admin_login());
$smarty->assign('url',$s['url']);
$smarty->assign('site_name',$s['site_name']);
$smarty->assign('img_path',$s['img_path']);
$smarty->assign('meta_desc',$s['meta_desc']);
$smarty->assign('meta_keywds',$s['meta_keywds']);
$smarty->assign('meta_copy',$s['meta_copy']);

function display_login()
{
	global $s,$smarty;
	$smarty->display('login.tpl');
}

function display_default()
{
	global $s,$smarty;
	$smarty->display('index.tpl');
}

if (validate_admin_login())
{
	if (!empty($_POST['opt']))
	{
		$inc = $_POST['opt'];
		include $s['inc_path'].$inc.'.php';
	}
	elseif (!empty($_GET['opt']))
	{
		$inc = $_GET['opt'];
		include $s['inc_path'].$inc.'.php';
	}
	else
	{
		display_default();
	}
}
else
{
	display_login();
}
writeWebLog('admin');
?>
