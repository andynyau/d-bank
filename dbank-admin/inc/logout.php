<?php
if ($_SERVER['PHP_SELF'] != '/dbank-admin/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
else
{
	global $smarty;

	admin_logout();
	$smarty->assign('validate',false);
	$smarty->display('login.tpl');
}
?>