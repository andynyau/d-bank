<?php
function approve_transaction()
{
	global $s,$db,$tb,$errormsg;

	$id = $_GET['id'];

	$pending_info = get_transaction_pending_info($id);
	$from = get_account_id($pending_info['from']);
	$to = $pending_info['to'];
	$amount = $pending_info['amount'];
	$remark = $pending_info['remark'];
	$date = $pending_info['datetime'];

	check_balance_enough($from,$amount);
	if ($errormsg)
	{
		display_pending_list();
	}
	else
	{
		do_transaction($from,$to,$amount,$remark,$date);
		decline_transaction();
	}
}

function decline_transaction()
{
	global $s,$db,$tb;

	$id = $_GET['id'];

	$q = new sql($db);
	$sql = "DELETE FROM {$tb['transaction_pending']} WHERE id = '$id'";
	$q->query($sql);

	display_pending_list();
}

function display_pending_list()
{
	global $smarty,$s,$db,$tb,$errormsg;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Date Time', 'From', 'To', 'Amount', 'Remark');
	$field_values = array('id', '`datetime`', 'from', 'to', 'amount', 'remark');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}
	$q=new sql($db);
	$sql="SELECT * FROM {$tb['transaction_pending']} ORDER BY {$tb['transaction_pending']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT * FROM {$tb['transaction_pending']} ORDER BY {$tb['transaction_pending']}.id";	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$id = $rows['id'];
			$pending_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$pending_info[$i]['datetime'] = $rows['datetime'];
			$pending_info[$i]['from'] = $rows['from'];
			$pending_info[$i]['to'] = $rows['to'];
			$pending_info[$i]['amount'] = $rows['amount'];
			$pending_info[$i]['remark'] = $rows['remark'];
			$pending_info[$i]['approve'] = "<a href=\"?opt=approve&act=approve&id={$rows['id']}&sort_by=$sort_by&sort_order=$sort_order&start=$start\"><img src=\"{$s['img_path']}button_unfreeze.png\" border=\"0\" alt=\"Approve\"></a>";
			$pending_info[$i]['decline'] = "<a href=\"?opt=approve&act=decline&id={$rows['id']}&sort_by=$sort_by&sort_order=$sort_order&start=$start\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Decline\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"approve",'',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('pending_info',$pending_info);
	}
	$smarty->assign("errormsg", $errormsg);
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('approve.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank-admin/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
elseif (validate_admin_login() == '1')
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'decline':
			decline_transaction();
			break;

			case 'approve':
			approve_transaction();
			break;

			default:
			display_pending_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			default:
			display_pending_list();
		}
	}
	else
	{
		display_pending_list();
	}
}
else
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"./?\">here</a> to go back to the main page.";
	echo "</center>";
}
?>