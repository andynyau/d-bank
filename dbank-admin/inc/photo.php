<?php
function approve_photo()
{
	global $s,$db,$tb;

	$id = $_GET['id'];

	$q = new sql($db);
	$sql = "UPDATE {$tb['customer_image']} SET status = 'approved' WHERE cid = '$id'";
	$q->query($sql);
	display_photo_list();
}

function decline_photo()
{
	global $s,$db,$tb;

	$id = $_GET['id'];

	$q = new sql($db);
	$sql = "DELETE FROM {$tb['customer_image']} WHERE cid = '$id'";
	$q->query($sql);

	display_photo_list();
}

function display_photo_list()
{
	global $smarty,$s,$db,$tb;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'username');
	$field_values = array('id', 'username');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}
	$q=new sql($db);
	$sql="SELECT {$tb['customer']}.id, {$tb['customer']}.username FROM {$tb['customer']} INNER JOIN {$tb['customer_image']} ON {$tb['customer']}.id = {$tb['customer_image']}.cid WHERE {$tb['customer_image']}.status = 'pending' ORDER BY {$tb['customer']}.$sort_by $sort_order LIMIT $start,3";
	$q->query($sql);
	$sql="SELECT {$tb['customer']}.id, {$tb['customer']}.username FROM {$tb['customer']} INNER JOIN {$tb['customer_image']} ON {$tb['customer']}.id = {$tb['customer_image']}.cid WHERE {$tb['customer_image']}.status = 'pending' ORDER BY {$tb['customer']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$id = $rows['id'];
			$photo_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$photo_info[$i]['username'] = $rows['username'];
			$photo_info[$i]['image'] = "<a href=\"?opt=view_image&id=$id\" target=\"_blank\"><img src=\"?opt=view_image&id=$id\" height=\"100\" border=\"0\" alt=\"View Image\"></a>";
			$photo_info[$i]['approve'] = "<a href=\"?opt=photo&act=approve&id={$rows['id']}&sort_by=$sort_by&sort_order=$sort_order&start=$start\"><img src=\"{$s['img_path']}button_unfreeze.png\" border=\"0\" alt=\"Approve\"></a>";
			$photo_info[$i]['decline'] = "<a href=\"?opt=photo&act=decline&id={$rows['id']}&sort_by=$sort_by&sort_order=$sort_order&start=$start\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Decline\"></a>";
			$i++;
		}
		$pg_link=pagination("3","10",$start,"",$sql,"photo",'',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('photo_info',$photo_info);
	}
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('photo.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank-admin/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
elseif (validate_admin_login() == '1')
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'decline':
			decline_photo();
			break;

			case 'approve':
			approve_photo();
			break;

			default:
			display_photo_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			default:
			display_photo_list();
		}
	}
	else
	{
		display_photo_list();
	}
}
else
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"./?\">here</a> to go back to the main page.";
	echo "</center>";
}
?>