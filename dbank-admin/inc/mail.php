<?php
function sendmail()
{
	global $smarty,$s,$db,$tb;

	$final = $_POST['final'];
	$email_list = get_email_list();
	$subject = $_POST['subject'];
	$message = $_POST['message'];
	if ($final+1 == get_final_id($tb['web_log']))
	{
		send_mail($email_list,$subject,$message);
	}
	$email_receiver = explode(",",$email_list);
	$smarty->assign('from','mail');
	$smarty->assign('from_t','Mail');
	$smarty->assign('receiver',$email_receiver);
	$smarty->display('mail_success.tpl');
}

function display_mail_form()
{
	global $smarty,$s,$db,$tb;

	$final = get_final_id($tb['web_log']);

	$smarty->assign('final',$final);
	$smarty->assign('from','mail');
	$smarty->display('mail.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank-admin/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
elseif (validate_admin_login() == '1')
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			default:
			display_mail_form();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'sendmail':
			sendmail();
			break;

			default:
			display_mail_form();
		}
	}
	else
	{
		display_mail_form();
	}
}
else
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"./?\">here</a> to go back to the main page.";
	echo "</center>";
}
?>