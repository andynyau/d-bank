<?php
function delete_participant()
{
	global $smarty,$s,$db,$tb,$errormsg;
	
	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		if ($id != '1')
		{
			$q=new sql($db);
			$sql = "DELETE FROM {$tb['participant']} WHERE id = '$id'";
			$q->query($sql);
			delete_participant_account($id);
		}
		else
		{
			errormsg("Cannot Delete D-Bank information");
		}
	}
	$smarty->assign('error',$errormsg);
	display_participant_list();
}

function process($type)
{
	global $s, $smarty, $db, $tb, $errormsg;

	$company_name = strip_tags($_POST['company_name']);
	check_field($company_name,"Company Name",1,"any",2);

	$contact_person = strip_tags($_POST['contact_person']);
	check_field($contact_person,"Contact Person",1,"any",2);

	$company_registration_no = strip_tags($_POST['company_registration_no']);
	check_field($company_registration_no,"Company Registration No",1,"any",2);

	$phone = strip_tags($_POST['phone']);
	check_field($phone,"Phone",0,"text",2);

	$address = strip_tags($_POST['address']);
	check_field($address,"Address",0,"text",2);

	if ($type=='edit')
	{
		$id = $_POST['id'];
	}

	if ($errormsg)
	{
		display_form($type,$id);
	}
	else
	{
		if ($type == 'add')
		{
			$q=new sql($db);
			$sql = "INSERT INTO {$tb['participant']} VALUES ('', '$company_name', '$contact_person', '$company_registration_no', '$phone', '$address', NOW(), NOW())";
			if (get_final_id($tb['participant']) == $_POST['final'])
			{
				$q->query($sql);
				$id = get_last_insert_id($tb['participant']);
				insert_participant_account($id);
			}
			$smarty->assign('type',$type);
			$smarty->display('participant_success.tpl');
		}
		else
		{
			$q=new sql($db);
			$sql = "UPDATE {$tb['participant']} SET company_name = '$company_name', contact_person = '$contact_person', company_registration_no = '$company_registration_no', phone = '$phone', address = '$address', date_last_modified = NOW() WHERE id = '$id'";
			$q->query($sql);
			$smarty->assign('type',$type);
			$smarty->display('participant_success.tpl');
		}
	}
}

function display_form($type,$id='')
{
	global $smarty,$s,$db,$tb,$errormsg;
	if (validate_admin_login() == '1')
	{
		if ($type=='edit')
		{
			$id = $_GET['id'];
			$participant_info = get_participant_info($id);
			$smarty->assign('id',$id);
			$smarty->assign('participant_info',$participant_info);
		}
		else
		{
			$final = get_final_id($tb['participant']);
			$smarty->assign('final',$final);
		}
		$smarty->assign('error',$errormsg);
		$smarty->assign('type',$type);
		$smarty->display('participant_form.tpl');
	}
	else
	{
		display_participant_list();
	}
}

function display_participant_list()
{
	global $smarty,$s,$db,$tb,$errormsg;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Company Name', 'Contact Person', 'Company Registration No', 'Phone', 'Address');
	$field_values = array('id', 'company_name', 'contact_person', 'company_registration_no', 'phone', 'address');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	$q=new sql($db);
	$sql="SELECT * FROM {$tb['participant']} ORDER BY {$tb['participant']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT * FROM {$tb['participant']} ORDER BY {$tb['participant']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$participant_info[$i]['tid'] = $rows['id'];
			$participant_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$participant_info[$i]['company_name'] = $rows['company_name'];
			$participant_info[$i]['contact_person'] = $rows['contact_person'];
			$participant_info[$i]['company_registration_no'] = $rows['company_registration_no'];
			$participant_info[$i]['phone'] = $rows['phone'];
			$participant_info[$i]['address'] = $rows['address'];
			$participant_info[$i]['edit'] = "<a href=\"?opt=participant&act=edit&id={$rows['id']}&sort_by=$sort_by&sort_order=$sort_order&start=$start\"><img src=\"{$s['img_path']}button_edit.png\" border=\"0\" alt=\"Edit\"></a>";
			$participant_info[$i]['delete'] = "<a href=\"?opt=participant&act=delete&id={$rows['id']}&sort_by=$sort_by&sort_order=$sort_order&start=$start\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Delete\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"creditcard",'',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('participant_info',$participant_info);
	}
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('participant.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank-admin/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
elseif (validate_admin_login() != '3')
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'delete':
			delete_participant();
			break;

			case 'edit':
			display_form('edit');
			break;

			default:
			display_participant_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'process':
			process($_POST['type']);
			break;

			case 'add':
			display_form('add');
			break;

			default:
			display_participant_list();
		}
	}
	else
	{
		display_participant_list();
	}
}
else
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"./?\">here</a> to go back to the main page.";
	echo "</center>";
}
?>