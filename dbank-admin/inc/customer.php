<?php
function delete_cc()
{
	global $s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		delete_customer_cc($id);
		$_GET['id'] = $_GET['cid'];
	}
	display_cc_list();
}

function update_cc_status($type)
{
	global $s,$db,$tb;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		
		if ($type == 'bar')
		{
			$sql = "UPDATE {$tb['credit_card']} SET status = 'barred', date_last_modified = NOW() WHERE id = '$id'";
		}
		elseif ($type == 'activate')
		{
			$sql = "UPDATE {$tb['credit_card']} SET status = 'active', date_last_modified = NOW() WHERE id = '$id'";
		}
		$q=new sql($db);
		$q->query($sql);

		$_GET['id'] = $_GET['cid'];
	}
	display_cc_list();
}

function process_cc()
{
	global $smarty,$s,$db,$tb,$errormsg;

	$cid = $_POST['cid'];
	$finalid = get_final_id($tb['credit_card']);
	$type = $_POST['cc_type'];
	if ($finalid == $_POST['final']);
	{
		$cc_no = makeCardNo($type);
		insert_customer_cc($cid,$type,$cc_no);
	}
	$smarty->assign('cc_no',$cc_no);
	$smarty->assign('cid',$cid);
	$smarty->display('customer_creditcard_success.tpl');
}

function display_cc_form()
{
	global $smarty,$s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$cid = $_POST['cid'];

		$fullname = get_customer_fullname($cid);

		$type_list = get_cc_type_list();

		$finalid = get_final_id($tb['credit_card']);
		$smarty->assign('cid',$cid);
		$smarty->assign('error',$errormsg);	
		$smarty->assign('customer_name',$fullname);
		$smarty->assign('final',$finalid);
		$smarty->assign('type_list',$type_list);
		$smarty->display('customer_creditcard_form.tpl');
	}
	else
	{
		display_cc_list();
	}
}

function delete_account()
{
	global $s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		delete_customer_account($id);
		$_GET['id'] = $_GET['cid'];
	}
	display_account_list();
}

function update_account_status($type)
{
	global $s,$db,$tb;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		
		if ($type == 'bar')
		{
			$sql = "UPDATE {$tb['account']} SET status = 'barred', date_last_modified = NOW() WHERE id = '$id'";
		}
		elseif ($type == 'activate')
		{
			$sql = "UPDATE {$tb['account']} SET status = 'active', date_last_modified = NOW() WHERE id = '$id'";
		}
		$q=new sql($db);
		$q->query($sql);

		$_GET['id'] = $_GET['cid'];
	}
	display_account_list();
}

function process_account()
{
	global $smarty,$s,$db,$tb,$errormsg;

	$cid = $_POST['cid'];
	$finalid = get_final_id($tb['account']);
	$type = $_POST['account_type'];
	$account_no = make_accountno($type);
	$amount = $_POST['balance'];
	currencyCheck($amount,'Balance');
	if ($errormsg)
	{
		display_account_form();
	}
	else
	{
		if ($finalid == $_POST['final']);
		{
			insert_customer_account($cid,$type,$account_no,$amount);
		}
		$smarty->assign('account_no',$account_no);
		$smarty->assign('cid',$cid);
		$smarty->display('customer_account_success.tpl');
	}
}

function display_account_form()
{
	global $smarty,$s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$cid = $_POST['cid'];

		$fullname = get_customer_fullname($cid);

		$type_list = get_account_type_list();

		$finalid = get_final_id($tb['account']);
		$smarty->assign('cid',$cid);
		$smarty->assign('error',$errormsg);	
		$smarty->assign('customer_name',$fullname);
		$smarty->assign('final',$finalid);
		$smarty->assign('type_list',$type_list);
		$smarty->display('customer_account_form.tpl');
	}
	else
	{
		display_account_list();
	}
}

function update_email_primary()
{
	global $s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		$cid = $_GET['cid'];
		if (!empty($id))
		{
			$q=new sql($db);
			$sql = "UPDATE {$tb['email']} SET `primary` = 'NO' WHERE cid = '$cid'";
			$q->query($sql);
			$sql = "UPDATE {$tb['email']} SET `primary` = 'YES' WHERE id = '$id'";
			$q->query($sql);
		}
		else
		{
			errormsg("No Email Is Selected");
		}
		$_GET['id'] = $_GET['cid'];
	}
	display_email_list();
}

function delete_email()
{
	global $s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		$q=new sql($db);
		$sql = "DELETE FROM {$tb['email']} WHERE id = '$id'";
		$q->query($sql);
		$_GET['id'] = $_GET['cid'];
	}
	display_email_list();
}

function process_email($type)
{
	global $s, $smarty, $db, $tb, $errormsg;

	$email = strip_tags($_POST['email']);
	$email2 = strip_tags($_POST['email2']);
	emailCheck($email,$email2);

	$cid = $_POST['cid'];

	if ($type=='edit')
	{
		$id = $_POST['id'];
	}

	if ($errormsg)
	{
		display_email_form($type);
	}
	else
	{
		if ($type == 'add')
		{
			$verify_str=md5($email);
			$q=new sql($db);
			$sql = "INSERT INTO {$tb['email']} VALUES ('', '$cid', '$email', 'NO', 'unverify', '$verify_str', NOW(), NOW())";
			if (get_final_id($tb['email']) == $_POST['final'])
			{
				$q->query($sql);
				sendmail_new($email,$verify_str);
			}
			$smarty->assign('type',$type);
			$smarty->assign('cid',$cid);
			$smarty->display('customer_email_success.tpl');
		}
		else
		{
			$verify_str=md5($email);
			$q=new sql($db);
			$sql = "UPDATE {$tb['email']} SET email = '$email', status = 'unverify', verify_str = '$verify_str', date_last_modified = NOW() WHERE id = '$id'";
			$q->query($sql);
			sendmail_new($email,$verify_str);
			$smarty->assign('type',$type);
			$smarty->assign('cid',$cid);
			$smarty->display('customer_email_success.tpl');
		}
	}
}

function display_email_form($type)
{
	global $smarty,$s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$cid = get_userid();
		if ($type=='edit')
		{
			$id = $_GET['id'];
			if (empty($id))
			{
				$id = $_POST['id'];
			}
			$cid = $_GET['cid'];
			if (empty($cid))
			{
				$cid = $_POST['cid'];
			}
			$email_info = get_email_info($id);
			$smarty->assign('id',$id);
			$smarty->assign('email_info',$email_info);
		}
		else
		{
			$cid = $_POST['cid'];
			$final = get_final_id($tb['email']);
		}
		$smarty->assign('final',$final);
		$smarty->assign('error',$errormsg);
		$smarty->assign('type',$type);
		$smarty->assign('cid',$cid);
		$smarty->display('customer_email_form.tpl');
	}
	else
	{
		display_email_list();
	}
}

function update_phone_primary()
{
	global $s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		$cid = $_GET['cid'];
		if (!empty($id))
		{
			$q=new sql($db);
			$sql = "UPDATE {$tb['phone']} SET `primary` = 'NO' WHERE cid = '$cid'";
			$q->query($sql);
			$sql = "UPDATE {$tb['phone']} SET `primary` = 'YES' WHERE id = '$id'";
			$q->query($sql);
		}
		else
		{
			errormsg("No Address Is Selected");
		}
		$_GET['id'] = $_GET['cid'];
	}
	display_phone_list();
}

function delete_phone()
{
	global $s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		$q=new sql($db);
		$sql = "DELETE FROM {$tb['phone']} WHERE id = '$id'";
		$q->query($sql);
		$_GET['id'] = $_GET['cid'];
	}
	display_phone_list();
}

function process_phone($type)
{
	global $s, $smarty, $db, $tb, $errormsg;

	$phone_no = strip_tags($_POST['phone_no']);
	check_field($phone_no,"Phone Number",1,"num",5);

	$cid = $_POST['cid'];
	$phone_type = $_POST['phone_type'];

	if ($type=='edit')
	{
		$id = $_POST['id'];
	}

	if ($errormsg)
	{
		display_phone_form($type);
	}
	else
	{
		if ($type == 'add')
		{
			$q=new sql($db);
			$sql = "INSERT INTO {$tb['phone']} VALUES ('', '$cid', '$phone_no', '$phone_type', 'NO', NOW(), NOW())";
			if (get_final_id($tb['phone']) == $_POST['final'])
			{
				$q->query($sql);
			}
			$smarty->assign('type',$type);
			$smarty->assign('cid',$cid);
			$smarty->display('customer_phone_success.tpl');
		}
		else
		{
			$q=new sql($db);
			$sql = "UPDATE {$tb['phone']} SET phone_no = '$phone_no', type_id = '$phone_type', date_last_modified = NOW() WHERE id = '$id'";
			$q->query($sql);
			$smarty->assign('type',$type);
			$smarty->assign('cid',$cid);
			$smarty->display('customer_phone_success.tpl');
		}
	}
}

function display_phone_form($type)
{
	global $smarty,$s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$cid = get_userid();
		$typelist = get_phone_type_list();
		if ($type=='edit')
		{
			$id = $_GET['id'];
			if (empty($id))
			{
				$id = $_POST['id'];
			}
			$cid = $_GET['cid'];
			if (empty($cid))
			{
				$cid = $_POST['cid'];
			}
			$phone_info = get_phone_info($id);
			$smarty->assign('id',$id);
			$smarty->assign('phone_info',$phone_info);
		}
		else
		{
			$cid = $_POST['cid'];
			$final=get_final_id($tb['phone']);
			$smarty->assign('final',$final);
		}
		$smarty->assign('error',$errormsg);
		$smarty->assign('type',$type);
		$smarty->assign('cid',$cid);
		$smarty->assign('typelist',$typelist);
		$smarty->display('customer_phone_form.tpl');
	}
	else
	{
		display_phone_list();
	}
}

function update_address_primary()
{
	global $s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		$cid = $_GET['cid'];
		if (!empty($id))
		{
			$q=new sql($db);
			$sql = "UPDATE {$tb['address']} SET `primary` = 'NO' WHERE cid = '$cid'";
			$q->query($sql);
			$sql = "UPDATE {$tb['address']} SET `primary` = 'YES' WHERE id = '$id'";
			$q->query($sql);
		}
		else
		{
			errormsg("No Address Is Selected");
		}
		$_GET['id'] = $_GET['cid'];
	}
	display_address_list();
}

function delete_address()
{
	global $s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$id = $_GET['id'];
		$q=new sql($db);
		$sql = "DELETE FROM {$tb['address']} WHERE id = '$id'";
		$q->query($sql);
		$_GET['id'] = $_GET['cid'];
	}
	display_address_list();
}

function process_address($type)
{
	global $s, $smarty, $db, $tb, $errormsg;

	$street = strip_tags($_POST['street']);
	check_field($street,"Street",1,"any",5);

	$city = strip_tags($_POST['city']);
	check_field($city,"City",1,"any",2);

	$postcode = strip_tags($_POST['postcode']);
	check_field($postcode,"Postcode",1,"text",2);

	$state = strip_tags($_POST['state']);
	check_field($state,"State",0,"text",2);

	$cid = $_POST['cid'];
	$countries = $_POST['country'];

	if ($type=='edit')
	{
		$id = $_POST['id'];
	}

	if ($errormsg)
	{
		display_address_form($type);
	}
	else
	{
		if ($type == 'add')
		{
			$q=new sql($db);
			$sql = "INSERT INTO {$tb['address']} VALUES ('', '$cid', '$street', '$city', '$postcode', '$state', '$countries', 'NO', NOW(), NOW())";
			if (get_final_id($tb['address']) == $_POST['final'])
			{
				$q->query($sql);
			}
			$smarty->assign('type',$type);
			$smarty->assign('cid',$cid);
			$smarty->display('customer_address_success.tpl');
		}
		else
		{
			$q=new sql($db);
			$sql = "UPDATE {$tb['address']} SET street = '$street', city = '$city', postcode = '$postcode', state = '$state', countries_id = '$countries', date_last_modified = NOW() WHERE id = '$id'";
			$q->query($sql);
			$smarty->assign('type',$type);
			$smarty->assign('cid',$cid);
			$smarty->display('customer_address_success.tpl');
			print_arr($_POST);
		}
	}
}

function display_address_form($type)
{
	global $smarty,$s,$db,$tb,$errormsg;

	if (validate_admin_login() == '1')
	{
		$countrylist = get_country_list();
		if ($type=='edit')
		{
			$id = $_GET['id'];
			if (empty($id))
			{
				$id = $_POST['id'];
			}
			$cid = $_GET['cid'];
			if (empty($cid))
			{
				$cid = $_POST['cid'];
			}
			$address_info = get_address_info($id);
			$smarty->assign('id',$id);
			$smarty->assign('address_info',$address_info);
		}
		else
		{
			$cid = $_POST['cid'];
			$final = get_final_id($tb['address']);
			$smarty->assign('final',$final);
		}
		$smarty->assign('error',$errormsg);
		$smarty->assign('type',$type);
		$smarty->assign('cid',$cid);
		$smarty->assign('countrylist',$countrylist);
		$smarty->display('customer_address_form.tpl');
	}
	else
	{
		display_address_list();
	}
}

function display_cc_list()
{
	global $smarty,$s,$db,$tb;

	$cid = $_GET['id'];
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Customer', 'Credit Card No', 'Credit Card Type', 'Since', 'Expires', 'Status');
	$field_values = array('id', 'fullname', 'cc_no', 'type_name', 'date_joined', 'date_expiry', 'status');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	if($sort_by == 'fullname')
	{
		$sort_by = $tb['customer'].".fullname";
	}
	elseif($sort_by == 'type_name')
	{
		$sort_by = $tb['credit_card_type'].".type_name";
	}

	$q=new sql($db);
	$sql="SELECT {$tb['credit_card']}.id, {$tb['customer']}.fullname, {$tb['credit_card']}.cc_no, {$tb['credit_card_type']}.type_name, {$tb['credit_card']}.date_joined, {$tb['credit_card']}.date_expiry, {$tb['credit_card']}.status FROM {$tb['credit_card']} INNER JOIN {$tb['customer']} ON {$tb['credit_card']}.cid = {$tb['customer']}.id INNER JOIN {$tb['credit_card_type']} ON {$tb['credit_card']}.tid = {$tb['credit_card_type']}.id WHERE {$tb['credit_card']}.cid = '$cid' ORDER BY {$tb['credit_card']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT {$tb['credit_card']}.id, {$tb['customer']}.fullname, {$tb['credit_card']}.cc_no, {$tb['credit_card_type']}.type_name, {$tb['credit_card']}.date_joined, {$tb['credit_card']}.date_expiry, {$tb['credit_card']}.status FROM {$tb['credit_card']} INNER JOIN {$tb['customer']} ON {$tb['credit_card']}.cid = {$tb['customer']}.id INNER JOIN {$tb['credit_card_type']} ON {$tb['credit_card']}.tid = {$tb['credit_card_type']}.id WHERE {$tb['credit_card']}.cid = '$cid' ORDER BY {$tb['credit_card']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$cc_info[$i]['tid'] = $rows['id'];
			$cc_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$cc_info[$i]['fullname'] = $rows['fullname'];
			$cc_info[$i]['cc_no'] = $rows['cc_no'];
			$cc_info[$i]['type_name'] = $rows['type_name'];
			$cc_info[$i]['since'] = $rows['date_joined'];
			$cc_info[$i]['expires'] = $rows['date_expiry'];
			$cc_info[$i]['status'] = $rows['status'];
			if ($rows['status'] == 'active')
			{
				$cc_info[$i]['action'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to freeze this Credit Card?' ,'?opt=customer&act=cc_bar&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_freeze.png\" border=\"0\" alt=\"Freeze\"></a>";
			}
			else
			{
				$cc_info[$i]['action'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to unfreeze this Credit Card?' ,'?opt=customer&act=cc_activate&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_unfreeze.png\" border=\"0\" alt=\"Unfreeze\"></a>";
			}
			$cc_info[$i]['delete'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to delete this Credit Card?' ,'?opt=customer&act=cc_delete&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Delete\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"customer",'cc',"&id=$cid&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("cid", $cid);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('cc_info',$cc_info);
	}
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('customer_creditcard.tpl');
}

function display_account_list()
{
	global $smarty,$s,$db,$tb,$errormsg;

	$cid = $_GET['id'];
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Account No', 'Type', 'Date Created', 'Balance', 'Status');
	$field_values = array('id', 'account_no', 'type_name', 'date_created', 'balance','status');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;	
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	if ($sort_by == 'type_name')
	{
		$sort_by = "{$tb['account_type']}.type_name";
	}
	elseif ($sort_by == 'balance')
	{
		$sort_by = "{$tb['balance']}.balance";
	}

	$q=new sql($db);
	$sql="SELECT {$tb['account']}.id, {$tb['account']}.account_no, {$tb['account']}.date_created, {$tb['balance']}.balance, {$tb['account']}.status, {$tb['account_type']}.type_name FROM {$tb['account']} INNER JOIN {$tb['account_type']} ON {$tb['account']}.type = {$tb['account_type']}.id INNER JOIN {$tb['balance']} ON {$tb['account']}.id = {$tb['balance']}.aid WHERE {$tb['account']}.cid = '$cid' ORDER BY {$tb['account']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT {$tb['account']}.id, {$tb['account']}.account_no, {$tb['account']}.date_created, {$tb['balance']}.balance, {$tb['account']}.status, {$tb['account_type']}.type_name FROM {$tb['account']} INNER JOIN {$tb['account_type']} ON {$tb['account']}.type = {$tb['account_type']}.id INNER JOIN {$tb['balance']} ON {$tb['account']}.id = {$tb['balance']}.aid WHERE {$tb['account']}.cid = '$cid' ORDER BY {$tb['account']}.id";	
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$account_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$account_info[$i]['tid'] = $rows['id'];
			$account_info[$i]['account_no'] = $rows['account_no'];
			$account_info[$i]['type'] = $rows['type_name'];
			$account_info[$i]['status'] = $rows['status'];
			$account_info[$i]['balance'] = sprintf("%0.2f", $rows['balance']);
			$account_info[$i]['date_created'] = $rows['date_created'];
			if ($rows['status'] == 'active')
			{
				$account_info[$i]['action'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to freeze this Account?' ,'?opt=customer&act=account_bar&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_freeze.png\" border=\"0\" alt=\"Freeze\"></a>";
			}
			else
			{
				$account_info[$i]['action'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to unfreeze this Account?' ,'?opt=customer&act=account_activate&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_unfreeze.png\" border=\"0\" alt=\"Unfreeze\"></a>";
			}
			$account_info[$i]['delete'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to delete this Account?' ,'?opt=customer&act=account_delete&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Delete\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"customer",'account',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("cid", $cid);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('account_info',$account_info);
	}
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('customer_account.tpl');
}

function display_email_list()
{
	global $smarty,$s,$db,$tb;

	$cid = $_GET['id'];
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Email', 'Status');
	$field_values = array('id', 'email', 'status');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	$q=new sql($db);
	$sql="SELECT * FROM {$tb['email']} WHERE {$tb['email']}.cid = '$cid' ORDER BY {$tb['email']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT * FROM {$tb['email']} WHERE {$tb['email']}.cid = '$cid' ORDER BY {$tb['email']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$id = $rows['id'];
			$email_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$email_info[$i]['email'] = $rows['email'];
			$email_info[$i]['status'] = $rows['status'];
			$email_info[$i]['primary'] = $rows['primary'];
			$email_info[$i]['edit'] = "<a href=\"?opt=customer&act=email_edit&id={$rows['id']}&cid=$cid\"><img src=\"{$s['img_path']}button_edit.png\" border=\"0\" alt=\"Edit\"></a>";
			$email_info[$i]['delete'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to delete this ?' ,'?opt=customer&act=email_delete&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Delete\"></a>";
			$email_info[$i]['resend'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to resend verify link to this email?' ,'?opt=customer&act=email_resend&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_verify.png\" border=\"0\" alt=\"Resend Verify\"></a>";
			$email_info[$i]['update_primary'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to update this as primary?' ,'?opt=customer&act=email_primary&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_primary.png\" border=\"0\" alt=\"Update Primary\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"customer",'email',"&id=$cid&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("cid", $cid);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('email_info',$email_info);
	}
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('customer_email.tpl');
}

function display_phone_list()
{
	global $smarty,$s,$db,$tb;

	$cid = $_GET['id'];
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Phone No', 'Type');
	$field_values = array('id', 'phone_no', 'type_name');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	if ($sort_by == 'type_name')
	{
		$sort_by = $tb['phone_type'].'.type_name';
	}

	$q=new sql($db);
	$sql="SELECT {$tb['phone']}.id, {$tb['phone']}.phone_no, {$tb['phone']}.primary, {$tb['phone_type']}.type_name FROM {$tb['phone']} INNER JOIN {$tb['phone_type']} ON {$tb['phone']}.type_id = {$tb['phone_type']}.id WHERE {$tb['phone']}.cid = '$cid' ORDER BY {$tb['phone']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT {$tb['phone']}.id, {$tb['phone']}.phone_no, {$tb['phone']}.primary, {$tb['phone_type']}.type_name FROM {$tb['phone']} INNER JOIN {$tb['phone_type']} ON {$tb['phone']}.type_id = {$tb['phone_type']}.id WHERE {$tb['phone']}.cid = '$cid' ORDER BY {$tb['phone']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$id = $rows['id'];
			$phone_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$phone_info[$i]['phone_no'] = $rows['phone_no'];
			$phone_info[$i]['type_name'] = $rows['type_name'];
			$phone_info[$i]['primary'] = $rows['primary'];
			$phone_info[$i]['edit'] = "<a href=\"?opt=customer&act=phone_edit&id={$rows['id']}&cid=$cid\"><img src=\"{$s['img_path']}button_edit.png\" border=\"0\" alt=\"Edit\"></a>";
			$phone_info[$i]['delete'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to delete this ?' ,'?opt=customer&act=phone_delete&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Delete\"></a>";
			$phone_info[$i]['update_primary'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to update this as primary?' ,'?opt=customer&act=phone_primary&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_primary.png\" border=\"0\" alt=\"Update Primary\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"customer",'phone',"&id=$cid&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("cid", $cid);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('phone_info',$phone_info);
	}
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('customer_phone.tpl');
}

function display_address_list()
{
	global $smarty,$s,$db,$tb;

	$cid = $_GET['id'];
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Street', 'City', 'PostCode', 'State', 'Country');
	$field_values = array('id', 'street', 'city', 'postcode', 'state', 'countries_name');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	if ($sort_by == 'countries_name')
	{
		$sort_by = $tb['countries'].'.countries_name';
	}

	$q=new sql($db);
	$sql="SELECT {$tb['address']}.id, {$tb['address']}.street, {$tb['address']}.city, {$tb['address']}.postcode, {$tb['address']}.state, {$tb['address']}.primary, {$tb['countries']}.countries_name FROM {$tb['address']} INNER JOIN {$tb['countries']} ON {$tb['address']}.countries_id = {$tb['countries']}.id WHERE {$tb['address']}.cid = '$cid' ORDER BY {$tb['address']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT {$tb['address']}.id, {$tb['address']}.street, {$tb['address']}.city, {$tb['address']}.postcode, {$tb['address']}.state, {$tb['address']}.primary, {$tb['countries']}.countries_name FROM {$tb['address']} INNER JOIN {$tb['countries']} ON {$tb['address']}.countries_id = {$tb['countries']}.id WHERE {$tb['address']}.cid = '$cid' ORDER BY {$tb['address']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$id = $rows['id'];
			$address_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$address_info[$i]['street'] = $rows['street'];
			$address_info[$i]['city'] = $rows['city'];
			$address_info[$i]['postcode'] = $rows['postcode'];
			$address_info[$i]['state'] = $rows['state'];
			$address_info[$i]['countries_name'] = $rows['countries_name'];
			$address_info[$i]['primary'] = $rows['primary'];
			$address_info[$i]['edit'] = "<a href=\"?opt=customer&act=address_edit&id={$rows['id']}&cid=$cid\"><img src=\"{$s['img_path']}button_edit.png\" border=\"0\" alt=\"Edit\"></a>";
			$address_info[$i]['delete'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to delete this ?' ,'?opt=customer&act=address_delete&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_delete.png\" border=\"0\" alt=\"Delete\"></a>";
			$address_info[$i]['update_primary'] = "<a href=\"javascript:;\" onClick=\"confirmmsg('Are you sure to update this as primary?' ,'?opt=customer&act=address_primary&id={$rows['id']}&cid=$cid&sort_by=$sort_by&sort_order=$sort_order&start=$start', 'D-Bank'); return false;\"><img src=\"{$s['img_path']}button_primary.png\" border=\"0\" alt=\"Update Primary\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"customer",'address',"&id=$cid&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("cid", $cid);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('address_info',$address_info);
	}
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('customer_address.tpl');
}

function display_customer_list()
{
	global $smarty,$s,$db,$tb;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'User Name', 'Full Name', 'IC / Passport', 'Date of Birth', 'Date Created');
	$field_values = array('id', 'username', 'fullname', 'ic_passport', 'dob', 'date_created');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}
	$q=new sql($db);
	$sql="SELECT * FROM {$tb['customer']} ORDER BY {$tb['customer']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT * FROM {$tb['customer']} ORDER BY {$tb['customer']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$id = $rows['id'];
			$customer_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$customer_info[$i]['username'] = $rows['username'];
			$customer_info[$i]['fullname'] = $rows['fullname'];
			$customer_info[$i]['ic_passport'] = $rows['ic_passport'];
			$customer_info[$i]['dob'] = $rows['dob'];
			$customer_info[$i]['date_created'] = $rows['date_created'];
			$customer_info[$i]['address'] = "<a href=\"?opt=customer&act=address&id={$rows['id']}\"><img src=\"{$s['img_path']}button_address.png\" border=\"0\" alt=\"Address\"></a>";
			$customer_info[$i]['phone'] = "<a href=\"?opt=customer&act=phone&id={$rows['id']}\"><img src=\"{$s['img_path']}button_phone.png\" border=\"0\" alt=\"Phone\"></a>";
			$customer_info[$i]['email'] = "<a href=\"?opt=customer&act=email&id={$rows['id']}\"><img src=\"{$s['img_path']}button_mail.png\" border=\"0\" alt=\"Email\"></a>";
			$customer_info[$i]['account'] = "<a href=\"?opt=customer&act=account&id={$rows['id']}\"><img src=\"{$s['img_path']}button_account.png\" border=\"0\" alt=\"Account\"></a>";
			$customer_info[$i]['cc'] = "<a href=\"?opt=customer&act=cc&id={$rows['id']}\"><img src=\"{$s['img_path']}button_cc.png\" border=\"0\" alt=\"Credit Card\"></a>";
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"customer",'',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('customer_info',$customer_info);
	}
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('customer.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank-admin/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
elseif (validate_admin_login())
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'cc_delete':
			delete_cc();
			break;

			case 'cc_activate':
			update_cc_status('activate');
			break;

			case 'cc_bar':
			update_cc_status('bar');
			break;	

			case 'account_delete':
			delete_account();
			break;

			case 'account_activate':
			update_account_status('activate');
			break;

			case 'account_bar':
			update_account_status('bar');
			break;			

			case 'email_primary':
			update_email_primary();
			break;

			case 'email_delete':
			delete_email();
			break;
			
			case 'email_edit':
			display_email_form('edit');
			break;

			case 'phone_primary':
			update_phone_primary();
			break;

			case 'phone_delete':
			delete_phone();
			break;

			case 'phone_edit':
			display_phone_form('edit');
			break;

			case 'address_primary':
			update_address_primary();
			break;
			
			case 'address_delete':
			delete_address();
			break;

			case 'address_edit':
			display_address_form('edit');
			break;

			case 'cc':
			display_cc_list();
			break;

			case 'account':
			display_account_list();
			break;

			case 'email':
			display_email_list();
			break;
			
			case 'phone':
			display_phone_list();
			break;

			case 'address':
			display_address_list();
			break;

			default:
			display_customer_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'process_cc':
			process_cc();
			break;

			case 'add_cc':
			display_cc_form();
			break;

			case 'process_account':
			process_account();
			break;

			case 'add_account':
			display_account_form();
			break;

			case 'process_email':
			process_email($_POST['type']);
			break;

			case 'add_email':
			display_email_form('add');
			break;

			case 'process_phone':
			process_phone($_POST['type']);
			break;

			case 'add_phone':
			display_phone_form('add');
			break;

			case 'process_address':
			process_address($_POST['type']);
			break;
			
			case 'add_address':
			display_address_form('add');
			break;

			default:
			display_customer_list();
		}
	}
	else
	{
		display_customer_list();
	}
}
else
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"./?\">here</a> to go back to the main page.";
	echo "</center>";
}
?>