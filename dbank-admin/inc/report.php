<?php
function display_report_list($from,$to)
{
	global $smarty,$s,$db,$tb;

	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}

	$tsort_by = $sort_by;

	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	$from_year = $_GET['from_year'];
	$from_month = $_GET['from_month'];
	$from_day = $_GET['from_day'];
	$to_year = $_GET['to_year'];
	$to_month = $_GET['to_month'];
	$to_day = $_GET['to_day'];
	$type = $_GET['type'];

	if ($type == 'in')
	{
		if ($sort_by == 'to')
		{
			$sort_by = $tb['account'].'.account_no';
		}
		$field_names = array('No', 'Date Time', 'From', 'To', 'Amount','Remark');
		$field_values = array('id', '`datetime`', 'from', 'to', 'amount','remark');
		$q=new sql($db);
		$sql="SELECT {$tb['transaction_in']}.id, {$tb['transaction_in']}.`datetime`, {$tb['transaction_in']}.from, {$tb['account']}.account_no, {$tb['transaction_in']}.amount, {$tb['transaction_in']}.remark FROM {$tb['transaction_in']} INNER JOIN {$tb['account']} ON {$tb['transaction_in']}.aid = {$tb['account']}.id  WHERE LEFT({$tb['transaction_in']}.`datetime`,10) >= '$from' AND LEFT({$tb['transaction_in']}.`datetime`,10) <= '$to' ORDER BY {$tb['transaction_in']}.$sort_by $sort_order LIMIT $start,10";
		$q->query($sql);
		$sql="SELECT {$tb['transaction_in']}.id, {$tb['transaction_in']}.`datetime`, {$tb['transaction_in']}.from, {$tb['account']}.account_no, {$tb['transaction_in']}.amount, {$tb['transaction_in']}.remark FROM {$tb['transaction_in']} INNER JOIN {$tb['account']} ON {$tb['transaction_in']}.aid = {$tb['account']}.id  WHERE LEFT({$tb['transaction_in']}.`datetime`,10) >= '$from' AND LEFT({$tb['transaction_in']}.`datetime`,10) <= '$to' ORDER BY {$tb['transaction_in']}.id";
		if ($q->numrows())
		{
			$i=0;
			while ($rows=$q->getrows())
			{
				$report_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
				$report_info[$i]['datetime'] = $rows['datetime'];
				$report_info[$i]['from'] = $rows['from'];
				$report_info[$i]['account_no'] = $rows['account_no'];
				$report_info[$i]['amount'] = $rows['amount'];
				$report_info[$i]['remark'] = $rows['remark'];
				$i++;
			}
			$pg_link=pagination("10","10",$start,"",$sql,"report",'view',"&sort_by=$sort_by&sort_order=$sort_order&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day");
			$extra="&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day";
			$sql = "SELECT SUM(amount) AS total FROM {$tb['transaction_in']} WHERE LEFT(`datetime`,10) >= '$from' AND LEFT(`datetime`,10) <= '$to'";
			$q->query($sql);
			if ($q->numrows())
			{
				while ($rows=$q->getrows())
				{
					$total = $rows['total'];
				}
			}
			$smarty->assign("total", $total);
			$smarty->assign("type", $type);
			$smarty->assign("extra",$extra);
			$smarty->assign("pg_link",$pg_link);
			$smarty->assign("sort_by", $tsort_by);
			$smarty->assign("sort_order", $sort_order);
			$smarty->assign("start", $start);
			$smarty->assign("field_names", $field_names);
			$smarty->assign("field_values", $field_values);
			$smarty->assign('report_info',$report_info);
			$smarty->assign('view','yes');
		}
		else
		{
			$smarty->assign("view","no");
		}
	}
	elseif ($type == 'out')
	{
		if ($sort_by == 'from')
		{
			$sort_by = $tb['account'].'.account_no';
		}
		$field_names = array('No', 'Date Time', 'To', 'From', 'Amount','Remark');
		$field_values = array('id', '`datetime`', 'To', 'from', 'amount','remark');
		$q=new sql($db);
		$sql="SELECT {$tb['transaction_out']}.id, {$tb['transaction_out']}.`datetime`, {$tb['transaction_out']}.to, {$tb['account']}.account_no, {$tb['transaction_out']}.amount, {$tb['transaction_out']}.remark FROM {$tb['transaction_out']} INNER JOIN {$tb['account']} ON {$tb['transaction_out']}.aid = {$tb['account']}.id  WHERE LEFT({$tb['transaction_out']}.`datetime`,10) >= '$from' AND LEFT({$tb['transaction_out']}.`datetime`,10) <= '$to' ORDER BY {$tb['transaction_out']}.$sort_by $sort_order LIMIT $start,10";
		$q->query($sql);
		$sql="SELECT {$tb['transaction_out']}.id, {$tb['transaction_out']}.`datetime`, {$tb['transaction_out']}.to, {$tb['account']}.account_no, {$tb['transaction_out']}.amount, {$tb['transaction_out']}.remark FROM {$tb['transaction_out']} INNER JOIN {$tb['account']} ON {$tb['transaction_out']}.aid = {$tb['account']}.id  WHERE LEFT({$tb['transaction_out']}.`datetime`,10) >= '$from' AND LEFT({$tb['transaction_out']}.`datetime`,10) <= '$to' ORDER BY {$tb['transaction_out']}.id";
		if ($q->numrows())
		{
			$i=0;
			while ($rows=$q->getrows())
			{
				$report_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
				$report_info[$i]['datetime'] = $rows['datetime'];
				$report_info[$i]['account_no'] = $rows['account_no'];
				$report_info[$i]['to'] = $rows['to'];
				$report_info[$i]['amount'] = $rows['amount'];
				$report_info[$i]['remark'] = $rows['remark'];
				$i++;
			}
			$pg_link=pagination("10","10",$start,"",$sql,"report",'view',"&sort_by=$sort_by&sort_order=$sort_order&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day");
			$extra="&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day";
			$sql = "SELECT SUM(amount) AS total FROM {$tb['transaction_out']} WHERE LEFT(`datetime`,10) >= '$from' AND LEFT(`datetime`,10) <= '$to'";
			$q->query($sql);
			if ($q->numrows())
			{
				while ($rows=$q->getrows())
				{
					$total = $rows['total'];
				}
			}
			$smarty->assign("total", $total);
			$smarty->assign("type", $type);
			$smarty->assign("extra",$extra);
			$smarty->assign("pg_link",$pg_link);
			$smarty->assign("sort_by", $tsort_by);
			$smarty->assign("sort_order", $sort_order);
			$smarty->assign("start", $start);
			$smarty->assign("field_names", $field_names);
			$smarty->assign("field_values", $field_values);
			$smarty->assign('report_info',$report_info);
			$smarty->assign('view','yes');
		}
		else
		{
			$smarty->assign("view","no");
		}
	}
	elseif ($type == 'cc')
	{
		if ($sort_by == 'cc_no')
		{
			$sort_by = $tb['credit_card'].'.cc_no';
		}
		elseif ($sort_by == 'account_no')
		{
			$sort_by = $tb['account'].'.account_no';
		}
		$field_names = array('No', 'Date Paid', 'Credit Card No', 'Account No', 'Amount');
		$field_values = array('id', 'date_pay', 'cc_no', 'account_no', 'amount');
		$q=new sql($db);
		$sql="SELECT {$tb['credit_card_payment']}.id, {$tb['credit_card_payment']}.date_pay, {$tb['credit_card']}.cc_no, {$tb['account']}.account_no, {$tb['credit_card_payment']}.amount FROM {$tb['credit_card_payment']} INNER JOIN {$tb['credit_card']} ON {$tb['credit_card_payment']}.cc_id = {$tb['credit_card']}.id INNER JOIN {$tb['account']} ON {$tb['credit_card_payment']}.aid = {$tb['account']}.id WHERE LEFT({$tb['credit_card_payment']}.`date_pay`,10) >= '$from' AND LEFT({$tb['credit_card_payment']}.`date_pay`,10) <= '$to' ORDER BY {$tb['credit_card_payment']}.$sort_by $sort_order LIMIT $start,10";
		$q->query($sql);
		$sql="SELECT {$tb['credit_card_payment']}.id, {$tb['credit_card_payment']}.date_pay, {$tb['credit_card']}.cc_no, {$tb['account']}.account_no, {$tb['credit_card_payment']}.amount FROM {$tb['credit_card_payment']} INNER JOIN {$tb['credit_card']} ON {$tb['credit_card_payment']}.cc_id = {$tb['credit_card']}.id INNER JOIN {$tb['account']} ON {$tb['credit_card_payment']}.aid = {$tb['account']}.id WHERE LEFT({$tb['credit_card_payment']}.`date_pay`,10) >= '$from' AND LEFT({$tb['credit_card_payment']}.`date_pay`,10) <= '$to' ORDER BY {$tb['credit_card_payment']}.id";
		if ($q->numrows())
		{
			$i=0;
			while ($rows=$q->getrows())
			{
				$report_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
				$report_info[$i]['date_pay'] = $rows['date_pay'];
				$report_info[$i]['cc_no'] = $rows['cc_no'];
				$report_info[$i]['account_no'] = $rows['account_no'];
				$report_info[$i]['amount'] = $rows['amount'];
				$i++;
			}
			$pg_link=pagination("10","10",$start,"",$sql,"report",'view',"&sort_by=$sort_by&sort_order=$sort_order&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day");
			$extra="&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day";
			$sql = "SELECT SUM(amount) AS total FROM {$tb['credit_card_payment']} WHERE LEFT(`date_pay`,10) >= '$from' AND LEFT(`date_pay`,10) <= '$to'";
			$q->query($sql);
			if ($q->numrows())
			{
				while ($rows=$q->getrows())
				{
					$total = $rows['total'];
				}
			}
			$smarty->assign("total", $total);
			$smarty->assign("type", $type);
			$smarty->assign("extra",$extra);
			$smarty->assign("pg_link",$pg_link);
			$smarty->assign("sort_by", $tsort_by);
			$smarty->assign("sort_order", $sort_order);
			$smarty->assign("start", $start);
			$smarty->assign("field_names", $field_names);
			$smarty->assign("field_values", $field_values);
			$smarty->assign('report_info',$report_info);
			$smarty->assign('view','yes');
		}
		else
		{
			$smarty->assign("view","no");
		}
	}
	elseif ($type == 'bill')
	{
		if ($sort_by == 'company_name')
		{
			$sort_by = $tb['participant'].'.company_name';
		}
		elseif ($sort_by == 'account_no')
		{
			$sort_by = $tb['account'].'.account_no';
		}
		$field_names = array('No', 'Date Paid', 'Company Name', 'Account No', 'Amount', 'Remark');
		$field_values = array('id', 'date_pay', 'company_name', 'account_no', 'amount', 'remark');
		$q=new sql($db);
		$sql="SELECT {$tb['bill']}.id, {$tb['bill']}.date_pay, {$tb['participant']}.company_name, {$tb['account']}.account_no, {$tb['bill']}.amount, {$tb['bill']}.remark FROM {$tb['bill']} INNER JOIN {$tb['participant']} ON {$tb['bill']}.pid = {$tb['participant']}.id INNER JOIN {$tb['account']} ON {$tb['bill']}.aid = {$tb['account']}.id WHERE LEFT({$tb['bill']}.`date_pay`,10) >= '$from' AND LEFT({$tb['bill']}.`date_pay`,10) <= '$to' ORDER BY {$tb['bill']}.$sort_by $sort_order LIMIT $start,10";
		$q->query($sql);
		$sql="SELECT {$tb['bill']}.id, {$tb['bill']}.date_pay, {$tb['participant']}.company_name, {$tb['account']}.account_no, {$tb['bill']}.amount, {$tb['bill']}.remark FROM {$tb['bill']} INNER JOIN {$tb['participant']} ON {$tb['bill']}.pid = {$tb['participant']}.id INNER JOIN {$tb['account']} ON {$tb['bill']}.aid = {$tb['account']}.id WHERE LEFT({$tb['bill']}.`date_pay`,10) >= '$from' AND LEFT({$tb['bill']}.`date_pay`,10) <= '$to' ORDER BY {$tb['bill']}.id";
		if ($q->numrows())
		{
			$i=0;
			while ($rows=$q->getrows())
			{
				$report_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
				$report_info[$i]['date_pay'] = $rows['date_pay'];
				$report_info[$i]['company_name'] = $rows['company_name'];
				$report_info[$i]['account_no'] = $rows['account_no'];
				$report_info[$i]['amount'] = $rows['amount'];
				$report_info[$i]['remark'] = $rows['remark'];
				$i++;
			}
			$pg_link=pagination("10","10",$start,"",$sql,"report",'view',"&sort_by=$sort_by&sort_order=$sort_order&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day");
			$extra="&type=$type&account_id=$aid&from_year=$from_year&from_month=$from_month&from_day=$from_day&to_year=$to_year&to_month=$to_month&to_day=$to_day";
			$sql = "SELECT SUM(amount) AS total FROM {$tb['bill']} WHERE LEFT(`date_pay`,10) >= '$from' AND LEFT(`date_pay`,10) <= '$to'";
			$q->query($sql);
			if ($q->numrows())
			{
				while ($rows=$q->getrows())
				{
					$total = $rows['total'];
				}
			}
			$smarty->assign("total", $total);
			$smarty->assign("type", $type);
			$smarty->assign("extra",$extra);
			$smarty->assign("pg_link",$pg_link);
			$smarty->assign("sort_by", $tsort_by);
			$smarty->assign("sort_order", $sort_order);
			$smarty->assign("start", $start);
			$smarty->assign("field_names", $field_names);
			$smarty->assign("field_values", $field_values);
			$smarty->assign('report_info',$report_info);
			$smarty->assign('view','yes');
		}
		else
		{
			$smarty->assign("view","no");
		}
	}
	display_report_form();
}

function view()
{
	global $smarty,$s,$db,$tb,$errormsg;
	
	if (empty($_POST))
	{
		$_POST = $_GET;
	}

	$from = dateCheck($_POST['from_year'], $_POST['from_month'], $_POST['from_day'], "From");
	$to = dateCheck($_POST['to_year'], $_POST['to_month'], $_POST['to_day'], "To");
	if ($from > $to)
	{
		errormsg("To cannot earlier than From.");
	}

	if ($errormsg)
	{
		display_report_form();
	}
	else
	{
		display_report_list($from,$to);
	}
}

function display_report_form()
{
	global $s,$db,$tb,$smarty,$errormsg;

	$cyear = date("Y");
	$cmonth = date("m");
	$cday = date("d");
	$year = getyearlist('2004',$cyear);
	$month = getmonthlist();
	$day = getdaylist();

	$smarty->assign('errormsg',$errormsg);
	$smarty->assign('yearlist',$year);
	$smarty->assign('monthlist',$month);
	$smarty->assign('daylist',$day);
	$smarty->assign('year',$cyear);
	$smarty->assign('month',$cmonth);
	$smarty->assign('day',$cday);
	$smarty->display('report.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank-admin/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
elseif (validate_admin_login() != '3')
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'view':
			view();
			break;

			default:
			display_report_form();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'view':
			view();
			break;

			default:
			display_report_form();
		}
	}
	else
	{
		display_report_form();
	}
}
else
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"./?\">here</a> to go back to the main page.";
	echo "</center>";
}
?>