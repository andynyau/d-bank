<?php
function process()
{
	global $s,$db,$tb;

	$id = $_POST['id'];
	$value = $_POST['value'];

	$q=new sql($db);
	$sql = "UPDATE {$tb['setting']} SET `value` = '$value', date_last_modified = NOW() WHERE id = '$id'";
	$q->query($sql);
	display_setting_list();
}

function display_form()
{
	global $smarty;
	$id = $_GET['id'];
	$setting_key = get_setting_key($id);
	$setting_value = get_setting_value($id);

	$smarty->assign('edit','yes');
	$smarty->assign('key',$setting_key);
	$smarty->assign('value',$setting_value);
	$smarty->assign('eid',$id);
	display_setting_list();
}

function display_setting_list()
{
	global $smarty,$s,$db,$tb,$errormsg;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Key', 'Value', 'Description');
	$field_values = array('id', '`key`', '`value`', 'description');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	$q=new sql($db);
	$sql="SELECT * FROM {$tb['setting']} ORDER BY $sort_by $sort_order";
	$q->query($sql);
	$sql="SELECT * FROM {$tb['setting']} ORDER BY id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$setting_list[$i] = $rows;
			$setting_list[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$setting_list[$i]['edit'] = "<a href=\"?opt=setting&act=edit&id={$rows['id']}&sort_by=$sort_by&sort_order=$sort_order&start=$start\"><img src=\"{$s['img_path']}button_edit.png\" border=\"0\" alt=\"Edit\"></a>";
			$i++;
		}
		$smarty->assign("errormsg",$errormsg);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign("field_names", $field_names);
		$smarty->assign("field_values", $field_values);
		$smarty->assign('setting_list',$setting_list);
		$smarty->display('setting.tpl');
	}
}

if ($_SERVER['PHP_SELF'] != '/dbank-admin/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
elseif (validate_admin_login() == '1')
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			case 'edit':
			display_form();
			break;

			default:
			display_setting_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			case 'process':
			process();
			break;

			default:
			display_setting_list();
		}
	}
	else
	{
		display_setting_list();
	}
}
else
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"./?\">here</a> to go back to the main page.";
	echo "</center>";
}
?>