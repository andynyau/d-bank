<?php
function display_log_list()
{
	global $smarty,$s,$db,$tb;
	if (empty($_GET))
	{
		$_GET = $_POST;
	}
	$field_names = array('No', 'Date Time', 'Query String', 'Remote Address', 'Customer', 'Admin', 'User Agent');
	$field_values = array('id', '`datetime`', 'query_string', 'remote_addr', 'customer_user', 'admin_user', 'user_agent');
	
	if(empty($_GET['sort_by']))
	{
			$sort_by = 'id';
	}
	else
	{
		$sort_by = $_GET['sort_by'];
	}
	$tsort_by = $sort_by;
	if(empty($_GET['sort_order']))
	{
		$sort_order = '';
	}
	else
	{
		$sort_order = 'desc';
	}

	if (empty($_GET['start']))
	{
		$start = '0';
	}
	else
	{
		$start = $_GET['start'];
	}

	$q=new sql($db);
	$sql="SELECT * FROM {$tb['web_log']} ORDER BY {$tb['web_log']}.$sort_by $sort_order LIMIT $start,10";
	$q->query($sql);
	$sql="SELECT * FROM {$tb['web_log']} ORDER BY {$tb['web_log']}.id";
	if ($q->numrows())
	{
		$i=0;
		while ($rows=$q->getrows())
		{
			$log_info[$i]['id'] = get_record_no($rows['id'],'id',$sql);
			$log_info[$i]['datetime'] = $rows['datetime'];
			$log_info[$i]['query_string'] = $rows['query_string'];
			$log_info[$i]['remote_addr'] = $rows['remote_addr'];
			$log_info[$i]['customer_user'] = $rows['customer_user'];
			$log_info[$i]['admin_user'] = $rows['admin_user'];
			$log_info[$i]['user_agent'] = $rows['user_agent'];
			$i++;
		}
		$pg_link=pagination("10","10",$start,"",$sql,"weblog",'',"&sort_by=$sort_by&sort_order=$sort_order");
		$smarty->assign("pg_link",$pg_link);
		$smarty->assign("sort_by", $tsort_by);
		$smarty->assign("sort_order", $sort_order);
		$smarty->assign("start", $start);
		$smarty->assign('log_info',$log_info);
	}
	$smarty->assign("field_names", $field_names);
	$smarty->assign("field_values", $field_values);
	$smarty->display('weblog.tpl');
}

if ($_SERVER['PHP_SELF'] != '/dbank-admin/index.php')
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"../?\">here</a> to go back to the main page.";
	echo "</center>";
}
elseif (validate_admin_login() == '1')
{
	if(!empty($_GET['act']))
	{
		settype($_GET['act'],'string');

		switch ($_GET['act']) 
		{
			default:
			display_log_list();
		}
	}
	elseif(!empty($_POST['act']))
	{
		settype($_POST['act'],'string');

		switch ($_POST['act']) 
		{
			default:
			display_log_list();
		}
	}
	else
	{
		display_log_list();
	}
}
else
{
	echo "<center>";
	echo "<font size=\"4\" color=\"#ff0000\">Access Denied</font><br>";
	echo "click <a href=\"./?\">here</a> to go back to the main page.";
	echo "</center>";
}
?>