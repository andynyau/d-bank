-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2014 at 11:35 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbank`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL DEFAULT '0',
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `account_no` int(10) unsigned NOT NULL DEFAULT '0',
  `type` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(16) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `cid`, `pid`, `account_no`, `type`, `status`, `date_created`, `date_last_modified`) VALUES
(1, 1, 0, 1000000001, 1, 'active', '2004-08-06 06:24:33', '2004-08-06 06:25:24'),
(2, 1, 0, 2000000001, 2, 'barred', '2004-08-06 06:24:33', '2004-08-06 06:25:24'),
(3, 1, 0, 3000000001, 3, 'active', '2004-08-06 06:24:33', '2004-08-06 06:25:24'),
(4, 0, 1, 3999999999, 3, 'active', '2004-08-06 06:24:33', '2004-08-06 06:25:24'),
(5, 0, 2, 3888888888, 3, 'active', '2004-08-06 06:24:33', '2004-08-06 06:25:24'),
(6, 0, 3, 3777777777, 3, 'active', '2004-08-06 06:24:33', '2004-08-06 06:25:24'),
(7, 0, 4, 3666666666, 3, 'active', '2004-08-06 06:24:33', '2004-08-06 06:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `account_reserve`
--

CREATE TABLE IF NOT EXISTS `account_reserve` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_no` int(10) unsigned NOT NULL DEFAULT '0',
  `type` int(10) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `account_reserve`
--

INSERT INTO `account_reserve` (`id`, `account_no`, `type`, `date_created`, `date_last_modified`) VALUES
(1, 1999999999, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2999999999, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1888888888, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2888888888, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `account_type`
--

CREATE TABLE IF NOT EXISTS `account_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `account_type`
--

INSERT INTO `account_type` (`id`, `type_name`) VALUES
(1, 'Saving'),
(2, 'Current'),
(3, 'Company');

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL DEFAULT '0',
  `street` varchar(64) NOT NULL DEFAULT '',
  `city` varchar(64) NOT NULL DEFAULT '',
  `postcode` varchar(8) NOT NULL DEFAULT '',
  `state` varchar(64) NOT NULL DEFAULT '',
  `countries_id` int(10) unsigned NOT NULL DEFAULT '0',
  `primary` varchar(4) NOT NULL DEFAULT 'NO',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `cid`, `street`, `city`, `postcode`, `state`, `countries_id`, `primary`, `date_created`, `date_last_modified`) VALUES
(1, 1, '29, Persiaran Rishah 13, Taman Tinggi', 'Ipoh', '30100', 'Perak', 129, 'YES', '2004-08-06 06:27:42', '2004-08-06 06:27:53'),
(2, 1, '5-1-24, Jalan 2/124, Taman Connaught', 'Cheras', '56000', 'Wilayah Persekutuan', 129, 'NO', '2004-08-06 06:27:42', '2004-08-06 06:27:53');

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(16) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `session` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `level`, `username`, `password`, `session`) VALUES
(1, 3, 'staff', 'de9bf5643eabf80f4a56fda3bbb84483', ''),
(2, 2, 'manager', '0795151defba7a4b5dfa89170de46277', 'bWFuYWdlcjoxMDk2ODEyNzMw'),
(3, 1, 'admin', '0192023a7bbd73250516f069df18b500', 'YWRtaW46MTA5NzAxMzEzOQ==');

-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

CREATE TABLE IF NOT EXISTS `balance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aid` int(10) unsigned NOT NULL DEFAULT '0',
  `balance` double(12,4) unsigned NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `balance`
--

INSERT INTO `balance` (`id`, `aid`, `balance`) VALUES
(1, 1, 873.7000),
(2, 2, 15060.0000),
(3, 3, 2743.0000),
(4, 4, 10744.2998),
(5, 5, 1175.0000),
(6, 6, 1000.0000),
(7, 7, 1105.0000);

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE IF NOT EXISTS `bill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) NOT NULL DEFAULT '0',
  `pid` int(10) NOT NULL DEFAULT '0',
  `amount` float(8,4) NOT NULL DEFAULT '0.0000',
  `aid` int(10) NOT NULL DEFAULT '0',
  `date_pay` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remark` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`id`, `cid`, `pid`, `amount`, `aid`, `date_pay`, `remark`) VALUES
(30, 1, 4, 98.0000, 98, '2004-10-06 06:45:49', 'AS6-2863-3659');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `countries_name` varchar(64) NOT NULL DEFAULT '',
  `countries_iso_code_2` char(2) NOT NULL DEFAULT '',
  `countries_iso_code_3` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `IDX_COUNTRIES_NAME` (`countries_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`) VALUES
(1, 'Afghanistan', 'AF', 'AFG'),
(2, 'Albania', 'AL', 'ALB'),
(3, 'Algeria', 'DZ', 'DZA'),
(4, 'American Samoa', 'AS', 'ASM'),
(5, 'Andorra', 'AD', 'AND'),
(6, 'Angola', 'AO', 'AGO'),
(7, 'Anguilla', 'AI', 'AIA'),
(8, 'Antarctica', 'AQ', 'ATA'),
(9, 'Antigua and Barbuda', 'AG', 'ATG'),
(10, 'Argentina', 'AR', 'ARG'),
(11, 'Armenia', 'AM', 'ARM'),
(12, 'Aruba', 'AW', 'ABW'),
(13, 'Australia', 'AU', 'AUS'),
(14, 'Austria', 'AT', 'AUT'),
(15, 'Azerbaijan', 'AZ', 'AZE'),
(16, 'Bahamas', 'BS', 'BHS'),
(17, 'Bahrain', 'BH', 'BHR'),
(18, 'Bangladesh', 'BD', 'BGD'),
(19, 'Barbados', 'BB', 'BRB'),
(20, 'Belarus', 'BY', 'BLR'),
(21, 'Belgium', 'BE', 'BEL'),
(22, 'Belize', 'BZ', 'BLZ'),
(23, 'Benin', 'BJ', 'BEN'),
(24, 'Bermuda', 'BM', 'BMU'),
(25, 'Bhutan', 'BT', 'BTN'),
(26, 'Bolivia', 'BO', 'BOL'),
(27, 'Bosnia and Herzegowina', 'BA', 'BIH'),
(28, 'Botswana', 'BW', 'BWA'),
(29, 'Bouvet Island', 'BV', 'BVT'),
(30, 'Brazil', 'BR', 'BRA'),
(31, 'British Indian Ocean Territory', 'IO', 'IOT'),
(32, 'Brunei Darussalam', 'BN', 'BRN'),
(33, 'Bulgaria', 'BG', 'BGR'),
(34, 'Burkina Faso', 'BF', 'BFA'),
(35, 'Burundi', 'BI', 'BDI'),
(36, 'Cambodia', 'KH', 'KHM'),
(37, 'Cameroon', 'CM', 'CMR'),
(38, 'Canada', 'CA', 'CAN'),
(39, 'Cape Verde', 'CV', 'CPV'),
(40, 'Cayman Islands', 'KY', 'CYM'),
(41, 'Central African Republic', 'CF', 'CAF'),
(42, 'Chad', 'TD', 'TCD'),
(43, 'Chile', 'CL', 'CHL'),
(44, 'China', 'CN', 'CHN'),
(45, 'Christmas Island', 'CX', 'CXR'),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK'),
(47, 'Colombia', 'CO', 'COL'),
(48, 'Comoros', 'KM', 'COM'),
(49, 'Congo', 'CG', 'COG'),
(50, 'Cook Islands', 'CK', 'COK'),
(51, 'Costa Rica', 'CR', 'CRI'),
(52, 'Cote D''Ivoire', 'CI', 'CIV'),
(53, 'Croatia', 'HR', 'HRV'),
(54, 'Cuba', 'CU', 'CUB'),
(55, 'Cyprus', 'CY', 'CYP'),
(56, 'Czech Republic', 'CZ', 'CZE'),
(57, 'Denmark', 'DK', 'DNK'),
(58, 'Djibouti', 'DJ', 'DJI'),
(59, 'Dominica', 'DM', 'DMA'),
(60, 'Dominican Republic', 'DO', 'DOM'),
(61, 'East Timor', 'TP', 'TMP'),
(62, 'Ecuador', 'EC', 'ECU'),
(63, 'Egypt', 'EG', 'EGY'),
(64, 'El Salvador', 'SV', 'SLV'),
(65, 'Equatorial Guinea', 'GQ', 'GNQ'),
(66, 'Eritrea', 'ER', 'ERI'),
(67, 'Estonia', 'EE', 'EST'),
(68, 'Ethiopia', 'ET', 'ETH'),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK'),
(70, 'Faroe Islands', 'FO', 'FRO'),
(71, 'Fiji', 'FJ', 'FJI'),
(72, 'Finland', 'FI', 'FIN'),
(73, 'France', 'FR', 'FRA'),
(74, 'France, Metropolitan', 'FX', 'FXX'),
(75, 'French Guiana', 'GF', 'GUF'),
(76, 'French Polynesia', 'PF', 'PYF'),
(77, 'French Southern Territories', 'TF', 'ATF'),
(78, 'Gabon', 'GA', 'GAB'),
(79, 'Gambia', 'GM', 'GMB'),
(80, 'Georgia', 'GE', 'GEO'),
(81, 'Germany', 'DE', 'DEU'),
(82, 'Ghana', 'GH', 'GHA'),
(83, 'Gibraltar', 'GI', 'GIB'),
(84, 'Greece', 'GR', 'GRC'),
(85, 'Greenland', 'GL', 'GRL'),
(86, 'Grenada', 'GD', 'GRD'),
(87, 'Guadeloupe', 'GP', 'GLP'),
(88, 'Guam', 'GU', 'GUM'),
(89, 'Guatemala', 'GT', 'GTM'),
(90, 'Guinea', 'GN', 'GIN'),
(91, 'Guinea-bissau', 'GW', 'GNB'),
(92, 'Guyana', 'GY', 'GUY'),
(93, 'Haiti', 'HT', 'HTI'),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD'),
(95, 'Honduras', 'HN', 'HND'),
(96, 'Hong Kong', 'HK', 'HKG'),
(97, 'Hungary', 'HU', 'HUN'),
(98, 'Iceland', 'IS', 'ISL'),
(99, 'India', 'IN', 'IND'),
(100, 'Indonesia', 'ID', 'IDN'),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN'),
(102, 'Iraq', 'IQ', 'IRQ'),
(103, 'Ireland', 'IE', 'IRL'),
(104, 'Israel', 'IL', 'ISR'),
(105, 'Italy', 'IT', 'ITA'),
(106, 'Jamaica', 'JM', 'JAM'),
(107, 'Japan', 'JP', 'JPN'),
(108, 'Jordan', 'JO', 'JOR'),
(109, 'Kazakhstan', 'KZ', 'KAZ'),
(110, 'Kenya', 'KE', 'KEN'),
(111, 'Kiribati', 'KI', 'KIR'),
(112, 'Korea, Democratic People''s Republic of', 'KP', 'PRK'),
(113, 'Korea, Republic of', 'KR', 'KOR'),
(114, 'Kuwait', 'KW', 'KWT'),
(115, 'Kyrgyzstan', 'KG', 'KGZ'),
(116, 'Lao People''s Democratic Republic', 'LA', 'LAO'),
(117, 'Latvia', 'LV', 'LVA'),
(118, 'Lebanon', 'LB', 'LBN'),
(119, 'Lesotho', 'LS', 'LSO'),
(120, 'Liberia', 'LR', 'LBR'),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY'),
(122, 'Liechtenstein', 'LI', 'LIE'),
(123, 'Lithuania', 'LT', 'LTU'),
(124, 'Luxembourg', 'LU', 'LUX'),
(125, 'Macau', 'MO', 'MAC'),
(126, 'Macedonia, The Former Yugoslav Republic of', 'MK', 'MKD'),
(127, 'Madagascar', 'MG', 'MDG'),
(128, 'Malawi', 'MW', 'MWI'),
(129, 'Malaysia', 'MY', 'MYS'),
(130, 'Maldives', 'MV', 'MDV'),
(131, 'Mali', 'ML', 'MLI'),
(132, 'Malta', 'MT', 'MLT'),
(133, 'Marshall Islands', 'MH', 'MHL'),
(134, 'Martinique', 'MQ', 'MTQ'),
(135, 'Mauritania', 'MR', 'MRT'),
(136, 'Mauritius', 'MU', 'MUS'),
(137, 'Mayotte', 'YT', 'MYT'),
(138, 'Mexico', 'MX', 'MEX'),
(139, 'Micronesia, Federated States of', 'FM', 'FSM'),
(140, 'Moldova, Republic of', 'MD', 'MDA'),
(141, 'Monaco', 'MC', 'MCO'),
(142, 'Mongolia', 'MN', 'MNG'),
(143, 'Montserrat', 'MS', 'MSR'),
(144, 'Morocco', 'MA', 'MAR'),
(145, 'Mozambique', 'MZ', 'MOZ'),
(146, 'Myanmar', 'MM', 'MMR'),
(147, 'Namibia', 'NA', 'NAM'),
(148, 'Nauru', 'NR', 'NRU'),
(149, 'Nepal', 'NP', 'NPL'),
(150, 'Netherlands', 'NL', 'NLD'),
(151, 'Netherlands Antilles', 'AN', 'ANT'),
(152, 'New Caledonia', 'NC', 'NCL'),
(153, 'New Zealand', 'NZ', 'NZL'),
(154, 'Nicaragua', 'NI', 'NIC'),
(155, 'Niger', 'NE', 'NER'),
(156, 'Nigeria', 'NG', 'NGA'),
(157, 'Niue', 'NU', 'NIU'),
(158, 'Norfolk Island', 'NF', 'NFK'),
(159, 'Northern Mariana Islands', 'MP', 'MNP'),
(160, 'Norway', 'NO', 'NOR'),
(161, 'Oman', 'OM', 'OMN'),
(162, 'Pakistan', 'PK', 'PAK'),
(163, 'Palau', 'PW', 'PLW'),
(164, 'Panama', 'PA', 'PAN'),
(165, 'Papua New Guinea', 'PG', 'PNG'),
(166, 'Paraguay', 'PY', 'PRY'),
(167, 'Peru', 'PE', 'PER'),
(168, 'Philippines', 'PH', 'PHL'),
(169, 'Pitcairn', 'PN', 'PCN'),
(170, 'Poland', 'PL', 'POL'),
(171, 'Portugal', 'PT', 'PRT'),
(172, 'Puerto Rico', 'PR', 'PRI'),
(173, 'Qatar', 'QA', 'QAT'),
(174, 'Reunion', 'RE', 'REU'),
(175, 'Romania', 'RO', 'ROM'),
(176, 'Russian Federation', 'RU', 'RUS'),
(177, 'Rwanda', 'RW', 'RWA'),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA'),
(179, 'Saint Lucia', 'LC', 'LCA'),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT'),
(181, 'Samoa', 'WS', 'WSM'),
(182, 'San Marino', 'SM', 'SMR'),
(183, 'Sao Tome and Principe', 'ST', 'STP'),
(184, 'Saudi Arabia', 'SA', 'SAU'),
(185, 'Senegal', 'SN', 'SEN'),
(186, 'Seychelles', 'SC', 'SYC'),
(187, 'Sierra Leone', 'SL', 'SLE'),
(188, 'Singapore', 'SG', 'SGP'),
(189, 'Slovakia (Slovak Republic)', 'SK', 'SVK'),
(190, 'Slovenia', 'SI', 'SVN'),
(191, 'Solomon Islands', 'SB', 'SLB'),
(192, 'Somalia', 'SO', 'SOM'),
(193, 'South Africa', 'ZA', 'ZAF'),
(194, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS'),
(195, 'Spain', 'ES', 'ESP'),
(196, 'Sri Lanka', 'LK', 'LKA'),
(197, 'St. Helena', 'SH', 'SHN'),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM'),
(199, 'Sudan', 'SD', 'SDN'),
(200, 'Suriname', 'SR', 'SUR'),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM'),
(202, 'Swaziland', 'SZ', 'SWZ'),
(203, 'Sweden', 'SE', 'SWE'),
(204, 'Switzerland', 'CH', 'CHE'),
(205, 'Syrian Arab Republic', 'SY', 'SYR'),
(206, 'Taiwan', 'TW', 'TWN'),
(207, 'Tajikistan', 'TJ', 'TJK'),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA'),
(209, 'Thailand', 'TH', 'THA'),
(210, 'Togo', 'TG', 'TGO'),
(211, 'Tokelau', 'TK', 'TKL'),
(212, 'Tonga', 'TO', 'TON'),
(213, 'Trinidad and Tobago', 'TT', 'TTO'),
(214, 'Tunisia', 'TN', 'TUN'),
(215, 'Turkey', 'TR', 'TUR'),
(216, 'Turkmenistan', 'TM', 'TKM'),
(217, 'Turks and Caicos Islands', 'TC', 'TCA'),
(218, 'Tuvalu', 'TV', 'TUV'),
(219, 'Uganda', 'UG', 'UGA'),
(220, 'Ukraine', 'UA', 'UKR'),
(221, 'United Arab Emirates', 'AE', 'ARE'),
(222, 'United Kingdom', 'GB', 'GBR'),
(223, 'United States', 'US', 'USA'),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI'),
(225, 'Uruguay', 'UY', 'URY'),
(226, 'Uzbekistan', 'UZ', 'UZB'),
(227, 'Vanuatu', 'VU', 'VUT'),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT'),
(229, 'Venezuela', 'VE', 'VEN'),
(230, 'Viet Nam', 'VN', 'VNM'),
(231, 'Virgin Islands (British)', 'VG', 'VGB'),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR'),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF'),
(234, 'Western Sahara', 'EH', 'ESH'),
(235, 'Yemen', 'YE', 'YEM'),
(236, 'Yugoslavia', 'YU', 'YUG'),
(237, 'Zaire', 'ZR', 'ZAR'),
(238, 'Zambia', 'ZM', 'ZMB'),
(239, 'Zimbabwe', 'ZW', 'ZWE');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card`
--

CREATE TABLE IF NOT EXISTS `credit_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL DEFAULT '0',
  `cc_no` bigint(16) unsigned NOT NULL DEFAULT '0',
  `tid` int(10) unsigned NOT NULL DEFAULT '0',
  `date_joined` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_expiry` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(16) NOT NULL DEFAULT '',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `credit_card`
--

INSERT INTO `credit_card` (`id`, `cid`, `cc_no`, `tid`, `date_joined`, `date_expiry`, `status`, `date_last_modified`) VALUES
(1, 1, 5140010120208888, 1, '2004-09-01 00:00:00', '2005-09-01 00:00:00', 'active', '2004-09-27 02:48:49'),
(2, 1, 4556010120208888, 2, '2004-09-01 00:00:00', '2005-09-01 00:00:00', 'active', '2004-09-27 02:58:39');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card_payment`
--

CREATE TABLE IF NOT EXISTS `credit_card_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cc_id` int(10) unsigned NOT NULL DEFAULT '0',
  `aid` int(10) unsigned NOT NULL DEFAULT '0',
  `amount` float(8,4) unsigned NOT NULL DEFAULT '0.0000',
  `date_pay` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `credit_card_payment`
--

INSERT INTO `credit_card_payment` (`id`, `cc_id`, `aid`, `amount`, `date_pay`) VALUES
(5, 2, 1, 194.3000, '2004-10-06 06:45:02');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card_type`
--

CREATE TABLE IF NOT EXISTS `credit_card_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `credit_card_type`
--

INSERT INTO `credit_card_type` (`id`, `type_name`) VALUES
(1, 'Master'),
(2, 'Visa');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL DEFAULT '',
  `fullname` varchar(64) NOT NULL DEFAULT '',
  `ic_passport` varchar(32) NOT NULL DEFAULT '',
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `username`, `fullname`, `ic_passport`, `dob`, `date_created`, `date_last_modified`) VALUES
(1, 'andynyau', 'Andy Nyau Wai Thong', '820308-08-5965', '1982-03-08', '2004-09-18 22:38:20', '2004-09-18 22:38:20');

-- --------------------------------------------------------

--
-- Table structure for table `customer_image`
--

CREATE TABLE IF NOT EXISTS `customer_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL DEFAULT '0',
  `image` longblob,
  `status` varchar(16) NOT NULL DEFAULT 'pending',
  `date_uploaded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `customer_image`
--

INSERT INTO `customer_image` (`id`, `cid`, `image`, `status`, `date_uploaded`) VALUES
(1, 1, 0xffd8ffe000104a46494600010101006000600000ffdb004300080606070605080707070909080a0c140d0c0b0b0c1912130f141d1a1f1e1d1a1c1c20242e2720222c231c1c2837292c30313434341f27393d38323c2e333432ffdb0043010909090c0b0c180d0d1832211c213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232ffc0001108009000b003012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f008562c76a76dc54fb3146dcd7258a2204839a951f9e69767147978aa8c9ad8564c955d4f434f033dea00869ca594f5ad6356fb92e1d89c023de91e458d4b3305c7ad57bbd4a0b0b679ee182c6a324d79778a3c7136a8cd6d625a2b7eedd0b56f1b35733b3d8eaf5ef1b41a731581d2470082a083cfe06b94ff85897a26dc211b7fbbbab8c24924939349473f62d411e80fe3e594a1d981fc4a7f9573da9789ae2eed5eda32551a42c48ea46781581451cfd03951d1d978aee6d76aed078c575fa278e209e458ee9ca9f41c28fc6bcb81e6a6f2b079900c0e6b55aab89c51f41db6a16f7080c72ab7d0d5c038cd7cf965ad5ee9f3ab413c8554fdd2c7047d2bd97c1dabff6dd8a3bc91ee1f7901e47eb50ec438b47451c2d2638357e28020e94f8d1546054e1695ee03020a705f535205a7aa54d8445b28d953eda368aa42d4e376fb518a948e3a52605709d561bb4d3b61a503d69e0534047b0d365648616924202a8c93e956715cf78d65921f0d5c796db598638eb4d46eec26cf33f1878846b1a8b240cdf668f85c9eb5ccd1457574b084a28a2a4028a28a0051d79a93786249193dbd2a2ab4967214de41db8cf15a42ef6132bb1c9e83f0ab3637f71a7dca4f6f2346e0f63d6a07d838553f52699c76a6db4c0fa17c11afa6b7a62b79a6495701f3d8d76416be5df0f6b773a1ea915cc1232aee01d41ea2be97d1afd353d3e2b88d89dca09a5b9935665f54a78518a705a76da42198a4dbed525253ba038ce28039a805d21ea3153248afd08ae1b1d23f14e5a41cf7a781d28402edac6f135b24fa3ca1c1200e82b6c64565788a4483459d9ba62b4a7f124296d73c32e2c25fb4b011845278f4a866b09a19150ae4b74c77aeba7b885d142a6e603737b554b9b696e3632e70c79c1e00af41c2265ccce69ece55dc40dcabd597903f1a8d20924385527f0aebadf46babe9c46176c0bc0c74c575da2f8124907cd11119efdcd613e55b1ac15f73c9e4b29a28d5dd0aeee80f7a85a364c6e5233d335eb973e1b4bbd62565889b6b55d880f42d58d7de1b47b679d9017dfb73ed9acf991afb3b9e76090723ad4f1dddc270b3363d09ad3bbd066898edc115993da4901f9d48ab8cbb10e9b431dfcd24b75f6a8e9f1e7703bb1f854b3c223f9958321ee2b492bea6656af6cf849e26f3b4f3a5dc025a26c2393d47a5789d76bf0c6f9ed7c571c3f298a752ae1bdbd3deb38ee292ba3e8ee9413ef5c8f8d7c650785f4b22dd925bf90010c24e4f3dc8f4af341f13bc5b691ac9750c0f1927931e3e838aa2541b3de38a4ae6fc21e209bc41a34579322a3b70c17a66ba20d9a1222e718d6ead50b5a63ee922afe07a5057358346d73376cf19e18d4c9732afde5cd5bd83d293cb07b66972a63b8c5bd8cf0dc53f50f0c5df89fc3d71f629910a72818f3211c9029a6dd09fbb5dbf87ed34ad2ecacefefe686293cb6f25646c6092727f2c0a9b59ab1a4126ddcf99858dce99773add46ea5485604ff0017a56cd859cb752ac71233b16ec3f415e97f14fc3b6296d06a50a1f3269c0454e84f5271f41fad6e7c36f085bc708d46e63defd5430e33eb5d6a7eedccdc55ec627873c15a8451a4b7368573c80c2bb99f4c5b7d3ca951f77a2f19f6fc4d763800600e2a9dfc0248c1c0e39acdb6ca8b4797de69c2d60504609c9603d4f5ae3b522a37478da09c015e87af46c777cebc7a3570777033b104eef66e87f1ae777b9d30b58e66e2d0924baf1eb8ac6d42ca3f29b700c08eb8e95d4de288895923788e3a8e45615de5c15475917bf1cd34ca68e1aeed4c0d9c71553240c02706babb9b4565c119f6ae7af2d8c4dd38ad94ae61521d5152b53c3a1bfb7ec8a4863612ae1c76e6b2eba3f0658bddf886df09b954e4e7a7a55477316751af6972b78fe517f33c8afb3cb73fc208ce07d3a7e35a77da6db4b6174ab0868208c9751d5b0320d1e3c4b88fc57691c6c4016f1c8a0f4c8dc0fe78159f16abf6e9d2d223bd6484995c76e3a7e7fcea25760b468ea7e13dcf99e1f688f58e522bd181e2bc8be11cc636bfb63905581c7e62bd69092b5b2b18cfe26739c5048a393d40a075f6ac99a20cd2ad271d29ebd3afeb5230c7233567c4da1dd5e58e8fa820f32db6470b267a1271508c77aecb44b55d73c252d84921531cbf291d571861fae6a251bad0d68cf92572978bf4c133e8563026f40e79ebc000576b636a967671c08000a2a03a7234f6aef826dd4e38f5abf5a2dac4c98572de2d8f526841b197000395dd8cfe95b3a95e3dac659064819ae1f51f17c98612e997c1012be62c64ae704ff2069a6097567976bb36b304ecf70929009e55b38acbb2f1288d8c774ad8271cd6ceb7e2ed3b5199a3b6b904ab1528c0a9c8ea39ae7da3b7ba721946e3de8e65f68d947aa3667d5ecae212237278e33585244252587cadea38cd523692c5725532466accb711dac61a4c126a5c55f42eed6e53b8dc3e571f461591a9c3fe8eed9e8335aada8dacad8de14fa1aa7aa4666b2db165839e4a8ce00e49a12699339ab687330c4d34c91a8cb31c0af61f00f84e7d3f5082edf26329ceef719fe758fe04f02cf36a705ddf45b618ca4b923af04edfcf1f957b4dbdb476f108d14003a62b557471ca5d8e07e205a20d774dbc322a7c9e5ed27961bc7f2c9ae6eca2b4b3b7b8d4e360b309f6b438c874e4120f6c1e6ba2f8a963aa5dae9d2e9f68f2a5b6f677519eb818fd2bcaeeaf7519e37b69207854e432a83927d2a5456b72e37691d7fc33d49ae3c557af230cdc02dd319e6bda931b6bca7e1c784278e74d56ec18d719893a57ac20dab8ad5194ed7d0e32ca59595c4a0e4355d1d3ad4408ed8a76ee3a8ac8a5a126680714c047b52ee1dcd1619306e39ab56facde6916b3bda3005c00548e09ed5477afa8a86e2f2dd7c8b679d1649e748d149e58961d295ae86bc8f6988b3471337de2833f954b51838503d0714c79b6a9cd245d9b32f5b9576e0571daf5dc175a05c594ca1a19548207507d6ba1d6d9d91b6f715e75a81b862f1eee33de936d1b46299e5e744b1d3dee55499ddf21371fbbff00d7f7a934db1ba95a08d54994fde039c727fa575c3c3df6db951f7989e82bbff0af83c595c89360217049228bb7b94d28ad0f3fd6b4c4d1f4549e54026907435e7375389988c9af5bf8bb36fd422b7418555e82bc5ef2c6496ec0136c5efd7f3f7a1242d6db11cba5b925e36e7af26bb6f875a3b5f4b702f612d00c00587bf4fc6b06c2091f538a1843bdb310bf3fdecd7b5e9767169f671411a80557923b9ef5a5dec73d597446adbc6b126d4000f402ad28c8aa919cd5943548e625d9b94e467eb59973a1e9f7320792d22661dca8ad55228600f4a632b4312c4a11142a8e8054d4808ce28cfbd2b01f2fc1e2ad6532bf6f9391d49a7ffc25bac9da8f7b2601f9b9ac0cf39a716cf38aa8cd5b535b1b9ff0966b0cd96be9481d066ad9f146b3394864be90c520cf5e4572e0e2acc521d9b78e3906b5a6d4909c51af06b5a8ddcc913de4ed1a7ab9e057bd7c25f0440f610789b52533ddce4b5b093911460f0467b9f5af9d34fc999c0ea46057db5a1d8ae97e1ad3ecd463c9b644e3d945655de8922e1a365d49010013cf4a5923dc3359de76c908f5357e397cc4f5e2b9d33468e775e611c4727b57996a3745a46c122bd535ab6fb4230f6af31d474a966ba2abc229e4fad4b66d4cddf05d8fdaa40c1379cf24d7a6154b3b3721470bcd73fe1682cecbc3b008e78d24704bf3ce68d57563169d22b480e7be7b5527a1135cd2d0f1bf88b73f68d6d896e82b84921491c1239adef16ddb5c6ab348a7386009f6ac359540c92066848d1b37fc2b67e66a909232b1fcd5e9313702b81f095d46974f1346caee06d63dc57731b71568e2aafde346363c55857ed9aa31bd5b8f9ad12312da1e2958d301c0a0f34c04ce0d26ee69bd3ad26690cf9268a28ac8dc2a4538a8e9c2b5a52b3133434c91629448dd0303f91afb78dd24ba7c52c4728f186523d08af8591b031eb5f49fc28f198d67c21158cd266eec0089813c951f74fe54ebec9954d6a7a22fdf627bf4abd6ae7671f95739f6f2660bbbbd68dadfaa5c842c30fc0fad7323568357be8a253bd828c739ac06305d5b650654f522b5b565b7ba2d14ea39ae626d256ce07974eb9921c8f9903657eb834d6a3454d42c74a9a266bbf39043f321472a73f8572bad78e6ce4d2521b7798c899493cc19e9d0e7bf1557c47e20bdb75fb35c989e3972a245f97f3ae027beb6406052cee49c05e771a0d395adc5bfd6158ba479692538048e9ef57ed74d8422ca5dcc9b7b9aeebc0bf0a92f74d7d535bdc92ca330a63ee8ac1d774f5d2b559ed501023381ef4e5a19a95ef61ba2b7fc4e2d87a37f4af4088fca2bcdf499766ab6c475df8fcebd1217e05540e6abf11a111c55d89ab351be5eb57216cd6a8c4ba0d3b39151a926a502985c6e293152e0d017da811f2251451581d2140a28a680901c1ad8f0cf88eebc33ac25e5bb12a46d9133c32d620a53c8ad65ef46c25a1f42e91e2b83538d6e62946586719e4543ad78d4e9e772302ea7239af08b3d4aeec1b36f294f6a96ef57bcbdff005d266b0b1aa9a3e9ff000f78b74df17e92b711b2a5d28db2c44f2a7fc0d727e2b6beb50e6def3675e335e1361aa5f69776b736572f0cabfc4a7afd7d6b6eebc6fa8dfa8176159bbb2f19a2c0a68b77f7b75313f6b94b8504f3cd741e09d22cd2f5759d663436fe5931a1fd2bcfaeb5792e2328a8141e09ea6bb18273a97c3fb7504f9906e8b03bfa7f4a6b62f9b9b447d2d6e4368cb7b1c89f671182bb7a74af09f1648d3eb33cec721db835dafc2282ef53f85d7367713487cabd78c6f6276a0553b47b649ae5fc71686d6f0ae31838144919ad1d8e56d1c477f037a383fad7a442c38c1fcabcaa639c8f518cd745f0ff005f9b51b492c6e4ee9ed80c39fe25aa811555cf408ce466af41dab3a2357a16e40c56a7397d6a74e6aba0e3a54ca0fa53b08980cd281ef42f4e94b401ffd9, 'pending', '2004-10-04 04:46:15');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(64) NOT NULL DEFAULT '',
  `primary` varchar(4) NOT NULL DEFAULT 'NO',
  `status` varchar(16) NOT NULL DEFAULT 'unverify',
  `verify_str` varchar(255) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `date_last_modified` (`date_last_modified`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL DEFAULT '0',
  `password` varchar(64) NOT NULL DEFAULT '',
  `session` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `cid`, `password`, `session`) VALUES
(1, 1, '14c4f0718a04671d26b7bc8f38958f06', '');

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE IF NOT EXISTS `participant` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(64) NOT NULL DEFAULT '',
  `contact_person` varchar(64) NOT NULL DEFAULT '',
  `company_registration_no` varchar(16) NOT NULL DEFAULT '',
  `phone` varchar(64) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id`, `company_name`, `contact_person`, `company_registration_no`, `phone`, `address`, `date_created`, `date_last_modified`) VALUES
(1, 'D Bank Credit Card Sdn. Bhd.', 'Andy Nyau', '888888-K', '03-99999999', '99, Jalan Bank, Menara D-Bank,58880', '2004-08-06 06:35:05', '2004-08-06 06:35:05'),
(2, 'Tenaga Nasional Berhad', 'Chong Li Vooi', '777777-X', '', '', '2004-08-06 06:35:05', '2004-08-06 06:35:05'),
(3, 'Telekom Malaysia Berhad', 'Yap Chao Yen', '666666-C', '', '', '2004-08-06 06:35:05', '2004-08-06 06:35:05'),
(4, 'Astro MEASAT Broadcast Network System Sdn. Bhd.', 'Then Kah Mun', '333333-T', '', '', '2004-08-06 06:35:05', '2004-08-06 06:35:05');

-- --------------------------------------------------------

--
-- Table structure for table `phone`
--

CREATE TABLE IF NOT EXISTS `phone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL DEFAULT '0',
  `phone_no` varchar(64) NOT NULL DEFAULT '',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `primary` varchar(4) NOT NULL DEFAULT 'NO',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `phone`
--

INSERT INTO `phone` (`id`, `cid`, `phone_no`, `type_id`, `primary`, `date_created`, `date_last_modified`) VALUES
(1, 1, '016-5660640', 3, 'YES', '2004-08-06 06:37:54', '2004-08-06 06:37:54');

-- --------------------------------------------------------

--
-- Table structure for table `phone_type`
--

CREATE TABLE IF NOT EXISTS `phone_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `phone_type`
--

INSERT INTO `phone_type` (`id`, `type_name`) VALUES
(1, 'Telephone'),
(2, 'Facsimile (Fax)'),
(3, 'Mobile Phone');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(64) NOT NULL DEFAULT '',
  `value` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `key`, `value`, `description`, `date_created`, `date_last_modified`) VALUES
(1, 'site_name', 'D-Bank', 'The name of the D-Bank Site, use for browser header', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(2, 'url', 'http://127.0.0.1/dbank/', 'url for the dbank site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(3, 'customer_url', 'http://127.0.0.1/dbank/', 'url for the dbank customer site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(4, 'admin_url', 'http://127.0.0.1/dbank-admin/', 'url for the dbank admin site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(5, 'lib_path', '../lib/', 'library path for both customer and admin site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(6, 'img_path', '../img/', 'image path for both admin and customer site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(7, 'inc_path', './inc/', 'include path for both admin and customer site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(8, 'meta_desc', 'd-bank', 'meta description for the site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(9, 'meta_keywds', 'dbank d-bank', 'meta keywords for the site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(10, 'meta_copy', 'Copyright (c) 2004, D-Bank All Rights Reserved.', 'meta copyright for the site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(11, 'cookies_name', 'dbank', 'cookies name for customer login session', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(12, 'cookies_name_admin', 'dbank_admin', 'cookies name for admin login session', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(13, 'cookies_timeout', '1', 'timeout for both cookies (unit in hours)', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(14, 'site_email', 'admin@dbank.com.my', 'email of the site', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(15, 'limit_per_transaction', '1000', 'limit of per transaction', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(16, 'limit_daily_transaction', '3000', 'limit of total daily transaction', '2004-08-06 06:38:52', '2004-08-06 06:42:41'),
(17, 'company_account', '3999999999', 'company account no, for credit card payment', '2004-08-06 06:38:52', '2004-08-06 06:42:41');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_in`
--

CREATE TABLE IF NOT EXISTS `transaction_in` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aid` int(10) unsigned NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `from` int(10) unsigned NOT NULL DEFAULT '0',
  `amount` float(8,4) unsigned NOT NULL DEFAULT '0.0000',
  `remark` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `transaction_in`
--

INSERT INTO `transaction_in` (`id`, `aid`, `datetime`, `from`, `amount`, `remark`) VALUES
(38, 3, '2004-10-06 06:44:30', 1000000001, 30.0000, 'transaction test'),
(39, 4, '2004-10-06 06:45:02', 1000000001, 194.3000, 'Credit Card Payment (4556010120208888 : 1000000001 : 2004-10-06 06:45:02)'),
(40, 7, '2004-10-06 06:45:49', 3000000001, 98.0000, 'Bill Payment (AS6-2863-3659 : 3000000001 : 2004-10-06 06:45:49)');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_out`
--

CREATE TABLE IF NOT EXISTS `transaction_out` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aid` int(10) unsigned NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to` int(10) unsigned NOT NULL DEFAULT '0',
  `amount` float(8,4) NOT NULL DEFAULT '0.0000',
  `remark` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `transaction_out`
--

INSERT INTO `transaction_out` (`id`, `aid`, `datetime`, `to`, `amount`, `remark`) VALUES
(45, 1, '2004-10-06 06:44:30', 3000000001, 30.0000, 'transaction test'),
(46, 1, '2004-10-06 06:45:02', 3999999999, 194.3000, 'Credit Card Payment (4556010120208888 : 1000000001 : 2004-10-06 06:45:02)'),
(47, 3, '2004-10-06 06:45:49', 3666666666, 98.0000, 'Bill Payment (AS6-2863-3659 : 3000000001 : 2004-10-06 06:45:49)');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_pending`
--

CREATE TABLE IF NOT EXISTS `transaction_pending` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL DEFAULT '0',
  `to` int(10) unsigned NOT NULL DEFAULT '0',
  `amount` float(8,4) unsigned NOT NULL DEFAULT '0.0000',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remark` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `transaction_pending`
--

INSERT INTO `transaction_pending` (`id`, `from`, `to`, `amount`, `datetime`, `remark`) VALUES
(111, 3000000001, 1000000001, 1100.0000, '2004-10-06 06:47:13', 'limit test');

-- --------------------------------------------------------

--
-- Table structure for table `web_log`
--

CREATE TABLE IF NOT EXISTS `web_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `query_string` varchar(255) NOT NULL DEFAULT '',
  `remote_addr` varchar(32) NOT NULL DEFAULT '',
  `customer_user` varchar(16) NOT NULL DEFAULT '',
  `admin_user` varchar(16) NOT NULL DEFAULT '',
  `user_agent` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `web_log`
--

INSERT INTO `web_log` (`id`, `datetime`, `query_string`, `remote_addr`, `customer_user`, `admin_user`, `user_agent`) VALUES
(1, '2004-10-06 06:43:21', '', '127.0.0.1', '', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(2, '2004-10-06 06:43:26', '', '127.0.0.1', '', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(3, '2004-10-06 06:43:30', 'opt=transfer', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(4, '2004-10-06 06:44:28', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(5, '2004-10-06 06:44:30', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(6, '2004-10-06 06:44:31', 'opt=transfer', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(7, '2004-10-06 06:44:32', 'opt=history', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(8, '2004-10-06 06:44:34', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(9, '2004-10-06 06:44:35', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(10, '2004-10-06 06:44:39', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(11, '2004-10-06 06:44:45', 'opt=creditcard', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(12, '2004-10-06 06:44:47', 'opt=creditcard&act=pay&id=2', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(13, '2004-10-06 06:45:02', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(14, '2004-10-06 06:45:08', 'opt=history', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(15, '2004-10-06 06:45:10', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(16, '2004-10-06 06:45:16', 'opt=bill', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(17, '2004-10-06 06:45:49', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(18, '2004-10-06 06:45:56', 'opt=history', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(19, '2004-10-06 06:46:01', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(20, '2004-10-06 06:46:14', 'opt=photo', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(21, '2004-10-06 06:46:15', 'opt=view_image&id=1', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(22, '2004-10-06 06:46:17', 'opt=change_pass', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(23, '2004-10-06 06:46:20', 'opt=transfer', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(24, '2004-10-06 06:46:35', 'opt=balance', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(25, '2004-10-06 06:46:36', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(26, '2004-10-06 06:46:37', 'opt=balance', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(27, '2004-10-06 06:46:39', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(28, '2004-10-06 06:46:45', 'opt=transfer', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(29, '2004-10-06 06:47:10', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(30, '2004-10-06 06:47:13', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(31, '2004-10-06 06:47:15', 'opt=balance', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(32, '2004-10-06 06:47:17', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(33, '2004-10-06 06:47:24', 'opt=help', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(34, '2004-10-06 06:47:25', 'opt=services', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(35, '2004-10-06 06:47:27', '', '127.0.0.1', 'andynyau', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(36, '2004-10-06 06:48:31', '', '127.0.0.1', '', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(37, '2004-10-06 06:48:43', '', '127.0.0.1', '', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'),
(38, '2004-10-06 06:48:57', '', '127.0.0.1', '', '', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
